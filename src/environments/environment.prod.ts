export const environment = {
  production: true,
  http:'http://',
  https:'https://',
  org:"POS20200001",
  branch:"1",
  api_url: "webdemos.itsabacus.net/backend_pos_53/public/api/",
//api_url: "localhost/back/backend/public/api/",
  //api_url:"appadmin.restrobeingz.com/api/",
//  api_url: "webdemos.itsabacus.net/pos_53_backend/public/api/",
  version: require('../../package.json').version,
  kds: false
};
