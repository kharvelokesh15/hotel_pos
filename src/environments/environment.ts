// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  http: 'http://',
  org: "POS20200001",
  branch: "1",
  https: 'http://',
  api_url: "webdemos.itsabacus.net/backend_pos_53/public/api/",
 //api_url: "localhost/back/backend/public/api/",
  //api_url: "appadmin.restrobeingz.com/api/",
//  api_url: "webdemos.itsabacus.net/pos_53_backend/public/api/",
  version: require('../../package.json').version,
  kds: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
