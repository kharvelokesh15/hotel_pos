import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions, IMyDateModel, IMyDate} from 'mydatepicker';

//services
import { HeaderService } from './../../header.service';
import { InvoieManageService } from './../../@service/kot';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-receiptlisting',
  templateUrl: './invoicelisting.component.html',
  styleUrls: ['./invoicelisting.component.css']
})
export class InvoicelistingComponent implements OnInit {
  public invoiceDetails: any =[];
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };
  public KOTDate; 
  public KotGroup:any=[];
  isTrade: boolean = false;
  checkAllTrades: boolean = false;
  checkAllinvoiceitemlist: boolean = false;
  isinvoiceitemlistCol: any = [];
  selectedIds : any =[];
  SelectedDate : any ='';
  currency;
  public selDate: IMyDate = {year: 0, month: 0, day: 0};
  public Date: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  invoice_status:any = 2;
  constructor(public _router: Router, private headerService: HeaderService,private kot
    : InvoieManageService,private utilityService: UtilityService) { 
      let d: Date = new Date();
      this.selDate = {year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate()};
      this.currency = this.utilityService.getCurrency();
    }
 

  
  ngOnInit() {
    this.headerService.setTitle('INVOICE LISTING');

    this.kot.invoiceList({"status":this.invoice_status,"kot_date":""}).subscribe(res => {
      this.invoiceDetails = res;
      if(this.invoiceDetails.length > 0){
        this.invoiceDetails = res;
      } else{
        this.invoiceDetails = [];
      }
        
    });

  }
  changeMergeByCategory(event) {
    if (event.target.name == 'mergereceiptdata') {
      this.isTrade = true
    }

    if (this.isTrade && this.checkAllTrades) {
      event.target.checked = true
    }
  }

  onDateChanged(event: IMyDateModel) {
    this.SelectedDate = event.formatted;
    this.kot.invoiceList({"status":this.invoice_status,"kot_date":this.SelectedDate}).subscribe(res => {
      this.invoiceDetails = res;
      if(this.invoiceDetails.length > 0){
        this.invoiceDetails = res;
      } else{
        this.invoiceDetails = [];
      }
      console.log(this.invoiceDetails.data)
    });
  }

  statusChange(status){
    this.invoice_status = status;
    this.kot.invoiceList({"status":this.invoice_status,"kot_date":this.SelectedDate}).subscribe(res => {
      this.invoiceDetails = res;
      if(this.invoiceDetails.length > 0){
        this.invoiceDetails = res;
      } else{
        this.invoiceDetails = [];
      }
    });
  }

  allholdItemList(event_select) {

    this.isinvoiceitemlistCol =[];

    if( event_select == true){
      for (let index = 0; index < this.invoiceDetails.length; index++) {
        this.selectedIds[index] = 1;
        this.isinvoiceitemlistCol.push(this.invoiceDetails[index].id);
        this.checkAllinvoiceitemlist = true
      }

    } else{
      for (let index = 0; index < this.invoiceDetails.length; index++) {
        this.selectedIds[index] = 0;
          this.isinvoiceitemlistCol =[];
          this.checkAllinvoiceitemlist = false;
      }
    }
  }

  // allmergeReceipt(event) {
  //   const checked = event.target.checked;
  //   this.mergereceiptdata.forEach(item => item.selected = checked);
  // }


}
