import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { InvoieManageService } from './../../@service/kot';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-kot-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.css']
})
export class InvoiceDetailsComponent implements OnInit {
  public _entityId;
  public invoiceDetailsData : any;
  public payent_type;
  public invoiceWaiter;
  public invoiceCustomer;
  public kotDetailMenu;
  public kotTableDetails;
  card_details:any =[];
  currency;
  constructor(
    // public _route: Route,
    public route: ActivatedRoute, private headerService: HeaderService,private invoice
    : InvoieManageService,private utilityService: UtilityService
  ) { this.currency = this.utilityService.getCurrency();}

  ngOnInit() {
    this.headerService.setTitle("Invoice Details Screen");
    this._entityId = this.route.snapshot.paramMap.get('id');
    this.invoice.invoiceDetailsList(this._entityId).subscribe(res => {
      console.log(res.payent_type);
      this.invoiceDetailsData = res;
      this.payent_type = res.payent_type;
      this.kotDetailMenu = this.invoiceDetailsData.kot_data;
      this.invoiceWaiter = this.invoiceDetailsData.waiter_id;
      this.invoiceCustomer = this.invoiceDetailsData.customer_id;
      console.log(this.invoiceDetailsData);
    });
  }

  onprint(){
    window.print();
  }

  GetStatusid(){
    var payment_array = (this.payent_type)?this.payent_type.split(','):'';
  

    if (payment_array.indexOf('card') != -1){
      var card_name_array = this.invoiceDetailsData.card_name.split(',');
      var card_number_array = this.invoiceDetailsData.card_number.split(',');
      var card_type_array = this.invoiceDetailsData.card_type.split(',');
      this.card_details = new Array(payment_array.length);

      for (let index = 0; index < payment_array.length; index++) {
        this.card_details[index] = {
          "card_name":card_name_array[index],
          "card_number":card_number_array[index],
          "card_type":card_type_array[index]
        }
      }
      return true;
    } else{
      return false;
    }

  } 


}
