import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicelistingComponent } from './invoicelisting/invoicelisting.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';

const routes: Routes = [
  {path:'invoicelisting',component:InvoicelistingComponent,pathMatch:'full'},
  {path: 'invoiceDetails/:id', component: InvoiceDetailsComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicelistingRoutingModule { }
