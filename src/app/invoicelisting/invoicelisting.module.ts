import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';

import { InvoicelistingRoutingModule } from './invoicelisting-routing.module';
import { InvoicelistingComponent } from './invoicelisting/invoicelisting.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';


@NgModule({
  declarations: [InvoicelistingComponent,InvoiceDetailsComponent],
  imports: [
    CommonModule,
    InvoicelistingRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule
  ]
})
export class InvoicelistingModule { }