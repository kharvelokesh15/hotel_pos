import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service'

@Component({
  selector: 'app-clockout',
  templateUrl: './clockout.component.html',
  styleUrls: ['./clockout.component.css']
})
export class ClockoutComponent implements OnInit {

  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Clock Out');
  }

}
