import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service'
@Component({
  selector: 'app-clockin',
  templateUrl: './clockin.component.html',
  styleUrls: ['./clockin.component.css']
})
export class ClockinComponent implements OnInit {

  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Clock In')
  }

}
