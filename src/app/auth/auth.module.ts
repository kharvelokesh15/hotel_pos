//modules
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { HeaderModule } from './../header/header.module';
import { FormsModule } from '@angular/forms'

//components

import { LoginComponent } from './login/login.component';
import { ClockinComponent } from './clockin/clockin.component';
import { ClockoutComponent } from './clockout/clockout.component';
import { OpenbalanceComponent } from './openbalance/openbalance.component';
import { OnlyNumbersDirective } from './only-numbers.directive';


@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    FormsModule,
    AuthRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [LoginComponent, ClockinComponent, ClockoutComponent, OpenbalanceComponent, OnlyNumbersDirective],
})
export class AuthModule { }
