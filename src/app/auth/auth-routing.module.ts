//modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// components
import { LoginComponent } from './login/login.component';
import { ClockinComponent } from './clockin/clockin.component';
import { ClockoutComponent } from './clockout/clockout.component';
import { OpenbalanceComponent } from './openbalance/openbalance.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'clockin', component: ClockinComponent },
  { path: 'clockout', component: ClockoutComponent },
  { path: 'openbalance', component: OpenbalanceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
