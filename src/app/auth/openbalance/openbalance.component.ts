import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
@Component({
  selector: 'app-openbalance',
  templateUrl: './openbalance.component.html',
  styleUrls: ['./openbalance.component.css']
})
export class OpenbalanceComponent implements OnInit {
  public openbalanceData;
  keyPadNo: string = "";
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Opening Balance Screen');
    this.openbalanceData = [
      { Type: '$500', Quantity: '2', Amount: '$1000' },
      { Type: '$500', Quantity: '2', Amount: '$1000' },
      { Type: '$500', Quantity: '2', Amount: '$1000' },
      { Type: '$500', Quantity: '2', Amount: '$1000' },
      { Type: '$500', Quantity: '2', Amount: '$1000' },
      { Type: '$500', Quantity: '2', Amount: '$1000' },
    ]

  }
  keyPressed(key: any): void {
    if (key === "clear") {
      this.keyPadNo = "";
    } else {
      this.keyPadNo += key;
    }
  }
  numberEnters(){
    
  }

}
