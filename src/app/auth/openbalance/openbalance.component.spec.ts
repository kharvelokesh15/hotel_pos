import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenbalanceComponent } from './openbalance.component';

describe('OpenbalanceComponent', () => {
  let component: OpenbalanceComponent;
  let fixture: ComponentFixture<OpenbalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenbalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenbalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
