import { Directive,ElementRef,Input,HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyNumbers]'
})
export class OnlyNumbersDirective {

  @Input('appOnlyNumbers') inputLength: string;
  constructor(private element: ElementRef) { }

  @HostListener('keydown', ['$event'])
  allowOnlyNumbers(event: KeyboardEvent) {
    if ((event.key == "+" || event.key == "-" || event.key == "e" || event.key == "E" || this.element.nativeElement.value.length >= this.inputLength) &&
      (event.key != "Backspace")) {
      event.preventDefault();
    }
  }


}
