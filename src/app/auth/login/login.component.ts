import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { AuthenticationService } from './../../@service/authentication.service';
import { StaffManageService } from './../../@service/staff/';
import { UtilityService } from '../../@service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChildren('passwordRef') passwordRef: any;
  passwordOne:  string = '';
  employeelist: any = [];
  employeeID:  string;
  currentlyFocused: number = 1;
  constructor(public _router: Router, private headerService: HeaderService,
     public Auth:AuthenticationService,
     public employee:StaffManageService,
     private utilityService: UtilityService,
     ) { }

  ngOnInit() {
    localStorage.removeItem('currentUser')
    localStorage.removeItem('currentUsertype')
    this.utilityService.clearDinine();
    this.headerService.setTitle('Login');
    this.employee.staffList().subscribe(res=>{
      this.employeelist = res;
      this.employeeID = this.employeelist[0].id_number
      console.log(this.employeelist)
    });
  }
  clockin(){
    this._router.navigateByUrl('/auth/clockin');
  }
  login(){
    console.log({"id_number":this.employeeID,"hash":this.passwordOne});
    // this.Auth.waiterlogin({"id_number":this.employeeID,"hash":this.passwordOne});
    this.Auth.employeelogin({"id_number":this.employeeID,"hash":this.passwordOne});
    //this._router.navigateByUrl('/homescreen/home');
  }
  clockout(){
    this._router.navigateByUrl('/auth/clockout');
  }
  changeFocusToNext(key: any, num: number) {
    if (key != "deleteContentBackward") {
      this.passwordRef.first.nativeElement.children[num].focus();
    }

  }

  pressedKey(input: any) {
    if (input == "clear") {
      this.passwordOne = "";
      this.currentlyFocused = 1;
    } else {
      this.passwordOne = this.passwordOne +''+ input;
      console.log(this.passwordOne)
    }

  }

}
