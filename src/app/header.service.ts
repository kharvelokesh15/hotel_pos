import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public title = new BehaviorSubject('Title');
  public restarunt = new BehaviorSubject('restarunt');
  public branch = new BehaviorSubject('branch');
  public callcenterbranch = new BehaviorSubject('callcenterbranch');

  constructor() { }

  setTitle(title) {
    this.title.next(title);
  }
  setRestarunt(restarunt) {
    this.restarunt.next(restarunt);
  }
  setBranch(branch) {
    this.branch.next(branch);
  }
  setCallcenterbranch(callcenterbranch) {
    this.callcenterbranch.next(callcenterbranch);
  }

}
