import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineWaitinglistComponent } from './dine-waitinglist.component';

describe('DineWaitinglistComponent', () => {
  let component: DineWaitinglistComponent;
  let fixture: ComponentFixture<DineWaitinglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineWaitinglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineWaitinglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
