import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { WaitinglistManageService } from './../../@service/waitinglist';
import { TableManageService } from './../../@service/table';

@Component({
  selector: 'app-dine-waitinglist',
  templateUrl: './dine-waitinglist.component.html',
  styleUrls: ['./dine-waitinglist.component.css']
})
export class DineWaitinglistComponent implements OnInit {
  public waitinglistData=[];
  public floorList;
  public SelectedFloor;
  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private waitinglistManageService: WaitinglistManageService,
    private tableManageService: TableManageService,
    private utilityService: UtilityService,
    ) { }

  ngOnInit() {
    this.tableManageService.floorList().subscribe(res => {
      this.floorList = res
      //console.log(res);
      this.SelectedFloor = this.floorList[0].id
      this.Getwaitinglist(this.SelectedFloor);
      console.log(this.SelectedFloor);
    })


    
   
    // this.waitinglistData = [
    //   {
    //     GuestName: 'Guest 1', NoofGuest: '04', ArrivedTime: '1h 30m'
    //   },
    //   {
    //     GuestName: 'Guest 2', NoofGuest: '02', ArrivedTime: '1h 30m'
    //   },
    //   {
    //     GuestName: 'Guest 3', NoofGuest: '03', ArrivedTime: '1h'
    //   },
    //   {
    //     GuestName: 'Guest 4', NoofGuest: '05', ArrivedTime: '50m'
    //   },
    //   {
    //     GuestName: 'Guest 5', NoofGuest: '02', ArrivedTime: '40m'
    //   },
    //   {
    //     GuestName: 'Guest 6', NoofGuest: '01', ArrivedTime: '30m'
    //   },
    //   {
    //     GuestName: 'Guest 7', NoofGuest: '02', ArrivedTime: '20m'
    //   },
    //   {
    //     GuestName: 'Guest 8', NoofGuest: '03', ArrivedTime: '10m'
    //   }
    // ]
  }
  Getwaitinglist(SelectedFloor){
    this.waitinglistManageService.tableWaitingDetailsList(SelectedFloor).subscribe(res => {
      this.waitinglistData = res
      
      console.log('info',this.waitinglistData);
    })
  }

  onFloorSelected(SelectedFloor){
    this.Getwaitinglist(SelectedFloor);
  }

}
