import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dine-reservation',
  templateUrl: './dine-reservation.component.html',
  styleUrls: ['./dine-reservation.component.css']
})
export class DineReservationComponent implements OnInit {
  public reservationData;
  constructor() { }

  ngOnInit() {
    this.reservationData = [
      {
        GuestName: 'Guest 1', Table: 'T2', NoofGuest: '04', PhoneNo: '9652147412', FromTime: '8:10 pm', ToTime: '9:10 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 2', Table: 'T3', NoofGuest: '02', PhoneNo: '9652147412', FromTime: '8:20 pm', ToTime: '9:20 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 3', Table: 'T4', NoofGuest: '03', PhoneNo: '9652147412', FromTime: '8:30 pm', ToTime: '9:30 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 4', Table: 'T5', NoofGuest: '05', PhoneNo: '9652147412', FromTime: '8:40 pm', ToTime: '9:40 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 5', Table: 'T6', NoofGuest: '02', PhoneNo: '9652147412', FromTime: '8:50 pm', ToTime: '9:50 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 6', Table: 'T7', NoofGuest: '01', PhoneNo: '9652147412', FromTime: '8:55 pm', ToTime: '9:55 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 7', Table: 'T8', NoofGuest: '02', PhoneNo: '9652147412', FromTime: '9:00 pm', ToTime: '10:00 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
      {
        GuestName: 'Guest 8', Table: 'T1', NoofGuest: '03', PhoneNo: '9652147412', FromTime: '9:10 pm', ToTime: '10:10 pm', Date: '25-Jul-2019', Time: '8:55 PM'
      },
    ]
  }

}
