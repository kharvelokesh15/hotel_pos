import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineReservationComponent } from './dine-reservation.component';

describe('DineReservationComponent', () => {
  let component: DineReservationComponent;
  let fixture: ComponentFixture<DineReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
