import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';

import { DineinRoutingModule } from './dinein-routing.module';
import { NgxElectronModule } from 'ngx-electron';

import { DineinSelectionComponent } from './dinein-selection/dinein-selection.component';
import { DineinCustomersComponent } from './dinein-customers/dinein-customers.component';
import { DineintabComponent } from './dineintab/dineintab.component';
import { DineinOrderComponent } from './dinein-order/dinein-order.component';
import { DineReservationComponent } from './dine-reservation/dine-reservation.component';
import { DineWaitinglistComponent } from './dine-waitinglist/dine-waitinglist.component';
import { DineinServersComponent } from './dinein-servers/dinein-servers.component';
import { DineCustomerinforamtionComponent } from './dine-customerinforamtion/dine-customerinforamtion.component';
import { MenucategoryComponent } from './menucategory/menucategory.component';
import { BillscreenComponent } from './billscreen/billscreen.component';
import { MenucategoryListComponent } from './menucategory-list/menucategory-list.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KotDineinComponent } from './kot-dinein/kot-dinein.component';
import { DineinTableDetailsComponent } from './dinein-table-details/dinein-table-details.component';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { OrderlistSearchComponent } from './orderlist-search/orderlist-search.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [DineinSelectionComponent, DineinCustomersComponent, DineintabComponent, DineinOrderComponent, DineReservationComponent, DineWaitinglistComponent, DineinServersComponent, DineCustomerinforamtionComponent, MenucategoryComponent, BillscreenComponent, MenucategoryListComponent, OrderlistComponent, KotDineinComponent, DineinTableDetailsComponent, OrderlistSearchComponent],
  imports: [
    CommonModule,
    HeaderModule,
    DineinRoutingModule,
    MyDatePickerModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    NgxElectronModule

  ],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [
    DineinSelectionComponent
  ]
})
export class DineinModule { }
