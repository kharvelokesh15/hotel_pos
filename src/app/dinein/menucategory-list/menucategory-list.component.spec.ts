import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenucategoryListComponent } from './menucategory-list.component';

describe('MenucategoryListComponent', () => {
  let component: MenucategoryListComponent;
  let fixture: ComponentFixture<MenucategoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenucategoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenucategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
