import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
//services
import { HeaderService } from './../../header.service';
import { HttpService, UtilityService } from '../../@service';
import { MenuManageService } from './../../@service/menu';
import { CustomerManageService } from './../../@service/customer';
import { TableManageService } from './../../@service/table';
import { CartManageService, KOTManageService } from './../../@service/kot';
import { BillingService } from './../../@service/billing/billing.service';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-menucategory-list',
  templateUrl: './menucategory-list.component.html',
  styleUrls: ['./menucategory-list.component.css']
})
export class MenucategoryListComponent implements OnInit {
  @ViewChild('templatetablefull', { static: true }) templatetablefull: TemplateRef<any>;
  @ViewChild('templatetableAddress', { static: true }) templatetableAddress: TemplateRef<any>;

  modalRef: BsModalRef;
  public foodList;
  public foodList_popup;
  public counter: number = 2;

  public menuCategory;
  public menuGroup;
  public menuList = [];
  public selectedGroup;
  public Query = "";
  public selectedCategory;
  public selectedModule;

  public SelectedCustomer;
  public selectedTable = [];
  public selectedtablename = [];
  public selectedtablenamecon;
  public Numseats;
  public popupmenu;
  public cartitems;
  public CartDetails;
  public CurentStaff;
  orgInformation: any;
  public item_notes;
  currency;
  public customerData;
  public customerQuery;
  public orgLogoPath;
  customerAddressList;
  selectedAddressId = '';
  customerAddressstore;
  showAddAddresSection = 0;
  public customer = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "gender": "",
    "email": ""
  };
  public customerAddress = {
    "no": "",
    "street_1": "",
    "street_2": "",
    "region": "",
    "country": "",
    "zipcode": "",
    "landmark": "",
    "phone": ""
  };
  public exCustomer: false | boolean;
  public search: any;

  ordermenus: string[] = ['Customer', 'Order', 'Table Selection'];
  selectedorder = this.ordermenus[1];

  fastfoodmenus: string[] = ['Fast Food', 'Drinks', 'Pizza', 'Dessert', 'Lunch Special', 'Dinner Special'];
  selectedfastfood = this.fastfoodmenus[0];

  kitchenPrinters: any = {}
  kotDetails = {}

  filteredData: any = [];
  usersForm: FormGroup;
  isLoading = false;

  coupon: any = '';
  coupon_status: any = "";
  discount_value: any = 0.00;
  coupon_value: any = 0.00;
  coupon_flag = 0;
  sub_total = 0;
  couponError = '';

  constructor(public _router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private billing: BillingService,
    private httpService: HttpService,
    private utilityService: UtilityService,
    private headerService: HeaderService,
    private modalService: BsModalService,
    private menuManageService: MenuManageService,
    private cartManageService: CartManageService,
    private kOTManageService: KOTManageService,
    private tableManageService: TableManageService,
    private customerManageService: CustomerManageService,
    private _electronService: ElectronService
  ) {

    this.currency = this.utilityService.getCurrency();
    this.orgInformation = this.utilityService.getOrgInformation();
    console.log('country    ' + this.orgInformation);
    this.orgLogoPath = this.utilityService.getLogoPath();

    this.filteredData = [];

  }
  openModal(template: TemplateRef<any>, menu) {
    console.log(menu);
    this.item_notes = '';
    this.menuManageService.menuDetails(menu.menu_id).subscribe(res => {
      this.popupmenu = res;
      this.popupmenu.qty = 1;
      this.popupmenu.menu_preparing_printer = menu.menu_preparing_printer;
      this.popupmenu.menu_modifier_id.forEach(element => { element.selected = false; element.qty = 0 });
      console.log(this.popupmenu);
      this.modalRef = this.modalService.show(template, {
        animated: true,
        // backdrop: 'static'
      });
    })

  }


  openModals(templates: TemplateRef<any>) {
    this.modalRef = this.modalService.show(templates, {
      class: 'list_receipt',
      animated: true,
      // backdrop: 'static'
    });

  }
  updatetoCart(item) {
    console.log(item.menu_modifier.map(v => v.qty).join(','))
    // console.log({	
    //     "cart_id":this.CartDetails.id,
    //     "cart_item_id":item.cart_item_id,
    //     "qty":item.menu_qty,
    //     "menu_modifier_ids":[
    //       {
    //         "menu_id":item.menu_id,
    //         "modifier_id":item.menu_modifier.map(v=>v.modifier_id).join(','),
    //         "modifier_qty":item.menu_modifier.map(v=>v.qty).join(',')
    //       }
    //     ]
    //   });
    this.cartManageService.CartItemUpdate({
      "cart_id": this.CartDetails.id,
      "cart_item_id": item.cart_item_id,
      "qty": item.menu_qty,
      "menu_modifier_ids": [
        {
          "menu_id": item.menu_id,
          "modifier_id": item.menu_modifier.map(v => v.modifier_id).join(','),
          "modifier_qty": item.menu_modifier.map(v => v.qty).join(',')
        }
      ]
    }).subscribe(res => {
      this.cartitems = res;
      this.segregateMenus(this.cartitems.menus);
      this.utilityService.setcartitems(this.cartitems);
    })
  }
  removecart(itemid) {
    this.cartManageService.CartItemRemove({
      "cart_id": this.CartDetails.id,
      "cart_item_id": itemid
    }).subscribe(res => {
      this.cartitems = res;
      this.segregateMenus(this.cartitems.menus);
      console.log(res);
      this.utilityService.setcartitems(this.cartitems);
    }, error => {
      this.cartitems = [];
      this.kitchenPrinters = {};
      console.log(error);
      this.utilityService.setcartitems(this.cartitems);
    })
  }


  Menusearch() {
    // if(this.selectedCategory == "All"){
    //   this.menuManageService.menuSearch("","","").subscribe(res => {
    //     this.menuList = res 
    //   });
    // }else{
    this.Query = this.usersForm.value['userInput'];
    this.menuManageService.menuSearch(this.Query, this.selectedCategory, this.selectedGroup.id).subscribe(res => {
      if (res.length > 0) {
        this.menuList = res
      } else {
        alert("No menu avilable");
      }
    });
    // }
  }

  initCart() {
    let data = {
      "floor_id": (this.selectedTable[0]) ? this.selectedTable[0].split('-')[1] : 0,
      "table_id": (this.selectedTable[0]) ? this.selectedTable[0].split('-')[0] : 0,
      "waiter_id": this.CurentStaff.id,
      "customer_id": this.SelectedCustomer.id,
      "occupied_chairs": (this.Numseats) ? this.Numseats : 0,
      "customer_guest_no": (this.Numseats) ? this.Numseats : 1
    }
    this.cartManageService.CatCreate(data).subscribe(res => {
      this.CartDetails = res;
      this.utilityService.setCartDetails(this.CartDetails);
      this.GetcartDetails()
    })

  }
  GetcartDetails() {
    this.cartManageService.CartItemList(this.CartDetails.id).subscribe(res => {
      this.cartitems = res;
      this.segregateMenus(this.cartitems.menus);
      console.log('cart details', res);
      this.utilityService.setcartitems(this.cartitems);
    })
  }
  AddToCart() {
    console.log(this.popupmenu);
    let data = {
      "cart_id": this.CartDetails.id,
      "customer_id": this.CartDetails.customer_id,
      "menu_ids": this.popupmenu.menu_id,
      "menu_qty": this.popupmenu.qty,
      "item_notes": this.item_notes,
      "menu_modifier_ids": []
    };
    if (this.popupmenu.menu_modifier_id && this.popupmenu.menu_modifier_id.length > 0) {
      data.menu_modifier_ids.push({
        "menu_id": this.popupmenu.menu_id,
        "modifier_id": this.popupmenu.menu_modifier_id.map(e => (e.selected) ? e.id : false).join(','),
        "modifier_qty": this.popupmenu.menu_modifier_id.map(e => (e.selected) ? e.qty : false).join(','),
        "menu_preparing_printer": this.popupmenu.menu_preparing_printer
      });
    }
    if (this.extraModifier && this.extraModifier.length > 0) {

      this.extraModifier.forEach(element => {
        data.menu_modifier_ids.push({
          "menu_id": this.popupmenu.menu_id,
          "modifier_id": element.id,
          "modifier_qty": 1,
          "menu_preparing_printer": this.popupmenu.menu_preparing_printer
        });
      });
     
    }
    this.extraModifier = [];
    console.log(data)

    this.cartManageService.CartAddItem(data).subscribe(res => {
      this.cartitems = res;
      this.segregateMenus(this.cartitems.menus);
      console.log('cartitems', res);
      // console.log(res);
      this.utilityService.setcartitems(this.cartitems);
    })
    console.log(this.cartitems)

  }

  sendToKitchen(type = '') {
    let data = {
      "floor_id": this.cartitems.floor_id,
      "table_id": this.cartitems.table_id,
      "cart_id": this.CartDetails.id,
      "occupied_chairs": this.cartitems.occupied_chairs,
      "waiter_id": this.CurentStaff.id,
      "customer_id": this.cartitems.customer_id,
      "customer_guest_no": this.cartitems.customer_id,
      "sub_total": this.cartitems.grand_total,
      "tax_total": this.cartitems.total_tax,
      "grand_total": this.cartitems.grand_total,
      "discount_total": this.cartitems.grand_discount,
      "over_all_qty": this.cartitems.grand_qty,
      "notes": "",
      "status": "2",
      "kot_type": this.selectedModule,
      "menu_ids": this.cartitems.menus.map(e => e.menu_id).join(','),
      "menu_qty": this.cartitems.menus.map(e => e.menu_qty).join(','),
      // "menu_notes":this.cartitems.menus.map(e=>e.menu_notes).join(','),
      "menu_notes": [],
      "menu_item_status": "1",
      "menu_modifier_ids": [
      ]
    };
    this.cartitems.menus.forEach(element => {
      data.menu_modifier_ids.push({
        "menu_id": element.menu_id,
        "modifier_id": element.menu_modifier.map(v => v.modifier_id).join(','),
        "modifier_qty": element.menu_modifier.map(v => v.qty).join(',')
      });
      data.menu_notes.push(element.menu_notes);
    });
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var time2 = (today.getHours() + 1) + ":" + today.getMinutes() + ":" + today.getSeconds();
    let tableselection = {
      "floor_id": this.cartitems.floor_id,
      "table_ids": this.cartitems.table_id,
      "occupied_chairs": this.cartitems.occupied_chairs,
      "waiter_id": this.CurentStaff.id,
      "customer_id": this.cartitems.customer_id,
      "is_booked": "1",
      "kot_type": this.selectedModule,
      "booked_from": time,
      "booked_to": time2,
      "type": 2,
      "waiting_id": 1
    };
    this.tableManageService.tableSelectionSave(tableselection).subscribe(res => {
      console.log('table selection save', res);
    })
    console.log('data', data);
    this.kOTManageService.kotSave(data).subscribe(
      res => {
        console.log('kot manage', res);
        this.kotDetails = res;
        this.printPageArea('full-menu');
        this.utilityService.clearDinine();
        if (type == 'takeaway') {
          this._router.navigate(['/receipt/DineinNormalBill/' + this.CurentStaff.id + '/' + this.cartitems.customer_id]);
        } else {
          this._router.navigate(['/kot/KotListing']);
        }

      }
    )
    //routerLink="/kot/KotListing"

  }


  openModalTableFullTemplate(templatetablefull: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      templatetablefull, { animated: true, class: "popup-min", backdrop: 'static', keyboard: false }

    );
  }
  openModalAddressTemplate(templatetablefull: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      templatetablefull, { animated: true, class: "popup-min", backdrop: 'static', keyboard: false }

    );
  }

  ngOnInit() {

    this.filteredData = [];

    this.usersForm = this.formBuilder.group({
      userInput: null
    });

    this.headerService.setTitle('Order Menu');
    this.selectedModule = this.utilityService.GetModule();
    this.headerService.setTitle(this.selectedModule.toUpperCase() + '-' + 'Order Menu');

    if (this.selectedModule == 'delivery' || this.selectedModule == 'callcenter') {
      this.openModalTableFullTemplate(this.templatetablefull)
      // this.SelectedCustomer= this.utilityService.getSelectedCustomer();
    }

    this.SelectedCustomer = this.utilityService.getSelectedCustomer();
    if (!this.SelectedCustomer || !this.SelectedCustomer.id) {
      this.customerManageService.guestSave({
        "type": "2",
        "mobile_number": ""
      }).subscribe(res => {
        this.SelectedCustomer = res
        this.utilityService.setSelectedCustomer(this.SelectedCustomer);
        if (!this.CartDetails || !this.CartDetails.id && (this.selectedModule != 'delivery' || this.selectedModule != 'callcenter')) {
          this.initCart();
        }
      })
    }

    this.CartDetails = this.utilityService.getCartDetails();
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.selectedTable = this.utilityService.getTableSelection();
    this.selectedtablename = this.utilityService.getTablenames();
    this.selectedtablenamecon = (this.selectedtablename) ? this.selectedtablename.join(', ') : '';
    this.Numseats = this.utilityService.getNumSeat();
    this.cartitems = this.utilityService.getcartitems();
    this.segregateMenus(this.cartitems.menus);
    console.log('this.cartitems', this.cartitems)
    this.menuManageService.menuCategory().subscribe(res => {
      this.menuCategory = res
    });
    this.menuManageService.menuGroup().subscribe(res => {
      this.menuGroup = res
      this.selectedGroup = '';
      // this.selectedGroup = this.menuGroup[0];
    });

    // this.menuManageService.menuList().subscribe(res=>{
    //   this.menuList = res
    // });

    this.menuManageService.menuSearch("", "", "").subscribe(res => {

      console.log(res)
      // this.menuList = res;
      if (res.data && res.data.length == 0) {
        this.menuManageService.menuList().subscribe(res => {
          this.menuList = res
        })
      } else {
        this.menuList = res;
      }

    });

    if (!this.CartDetails || !this.CartDetails.id && (this.selectedModule != 'delivery' || this.selectedModule != 'callcenter')) {
      this.initCart();
    }
    let httpHeaders = this.httpService.getJsonHeaders();

    this.usersForm.get('userInput').valueChanges
      .pipe(tap(() => {
        this.isLoading = true;
      }),
        switchMap(value =>

          this.http.post(environment.https + environment.api_url + 'menu-group-category', {
            "search_data": value,
            "category_id": this.selectedCategory,
            "group_id": this.selectedGroup.id
          }, {
            headers: httpHeaders
          }).pipe(
            finalize(() => {
              this.isLoading = false
            }),
          ))
      )
      .subscribe(res => {
        this.filteredData = res['data'];
        console.log(this.filteredData);
      })

  }

  customerSerch() {
    this.customerManageService.customerSearch(this.customerQuery).subscribe(res => {
      this.customerData = res.slice(0, 7)
      console.log(res);
    })
  }

  searchCustomers(data) {
    this.customerManageService.customerSearch(data.target.value).subscribe(res => {
      if (res) {
        this.customer = res[0];
        this.exCustomer = true;
        console.log('search customer =>', this.customer);
      } else {
        this.exCustomer = false;
        this.search = data.target.value;
        this.resetCustomer();
        this.resetLocalStorageCustomer();
      }
    })
  }

  resetCustomer() {
    this.customer = {
      "first_name": "",
      "last_name": "",
      "phone": this.search,
      "gender": "",
      "email": ""
    };
  }
  resetLocalStorageCustomer() {
    this.utilityService.setSelectedCustomer({});
  }

  saveCustomer() {
    if (this.exCustomer == true) {
      // Save Customer in local storage
      this.utilityService.setSelectedCustomer(this.customer);
      this.SelectedCustomer = this.utilityService.getSelectedCustomer();
      console.log(this.SelectedCustomer)
      this.modalRef.hide();
      this.openModalAddressTemplate(this.templatetableAddress)
      this.customerManageService.customerAddressList({ customer_id: this.SelectedCustomer.id }).subscribe(res => {
        this.customerAddressList = res;
      });
      this.initCart();

      console.log('add ex customer in local storage');
    } else {
      // add new customer
      this.customerManageService.customerSave({
        "first_name": this.customer.first_name,
        "last_name": this.customer.last_name,
        "gender": this.customer.gender,
        "email": this.customer.email,
        "mobile_number": this.customer.phone,
        "type": 1,
        "phone": this.customer.phone,
        "mobile_key": this.customer.phone,
      }).subscribe(res => {
        this.customerManageService.customerSearch(this.search).subscribe(res => {
          this.customer = res[0];
          this.utilityService.setSelectedCustomer(this.customer);
          this.SelectedCustomer = this.utilityService.getSelectedCustomer();
          this.modalRef.hide();
          this.openModalAddressTemplate(this.templatetableAddress)
          this.customerManageService.customerAddressList({ customer_id: this.SelectedCustomer.id }).subscribe(res => {
            this.customerAddressList = res;
          });
          this.initCart();
        });


      });
    }
  }

  storeAddress() {
    this.customerManageService.customerAddressDetails({ id: this.selectedAddressId }).subscribe(res => {
      this.customerAddressstore = res;
      this.utilityService.setSelectedAddress(this.customerAddressstore);
      this.modalRef.hide();
    });
  }

  segregateMenus(menus) {
    this.kitchenPrinters = {};
    if (menus) {
      menus.forEach(menu => {
        const menuPrinterName = menu.menu_preparing_printer || 'common';
        this.kitchenPrinters[menuPrinterName] = [...(this.kitchenPrinters[menuPrinterName] || []), menu];
      });
      console.log(this.kitchenPrinters);
    }
  }

  printPageArea(areaId, forceBrowserPrint = false) {
    const styleTags = `<style>
        *.print-ticket {
          font-size: 8px;
          font-family: 'Times New Roman';
        text-transform: uppercase;
      }
      .print_header{
        display:block;
      }

      td,
      th,
      tr,
      table {
        /*border-bottom: 2px dotted black;*/
          border-collapse: collapse;
      }
      table {
        border-bottom: 2px dotted black;
        width:100%;
          border-collapse: collapse;
      }

      td.description,
      th.description {
          width: 40%;
          max-width: 40%;
      }

      td.quantity,
      th.quantity {
          width: 15%;
          max-width: 15%;
          word-break: break-all;
          text-align: right;
      }

      .centered {
          text-align: center;
          align-content: center;
      }

      .ticket {
          width: 100%;
          max-width: 100%;
      }

      img.logo-print {
          background-size: cover;
          width: 120px;
          height: 120px;
          border: 5px solid white;
          padding: 0.25rem;
          border-radius: 50%;
      }

      .dotted-line{
        border-bottom: 2px dotted black;
        width: 100%;
      }

      tr td,
      tr th{
          text-align: left;
          font-size: 12px;
      }
  
      .text-right{
          text-align: right;
          padding-right: 10px;
      }
  
      .extra-details{
          height:15px;
          font-size: 12;
        }
  
        .left-details{
          float: left;
        }
        
        .right-details{
          float: right;
        }
  
      @page { margin: 0; }
        </style>`;

    if (this._electronService.isElectronApp && !forceBrowserPrint) {
      for (const [index, [kitchen,]] of Object.entries(Object.entries(this.kitchenPrinters))) {
        console.log(this._electronService);
        const printContent = document.getElementById(`kitchen-${index}`);
        const htmlContent = styleTags + printContent.innerHTML
        try {
          this._electronService.ipcRenderer.send('printToKitchen', {
            id: index,
            htmlContent, printerOptions: {
              silent: true,
              deviceName: kitchen === 'common' ? '' : kitchen
            }
          })
        } catch (ex) {
          this.printPageArea(areaId);
          // Electron.ipcRenderer.send('', { htmlContent: printContent[0].innerHTML, printerOptions: {} })
        }
      }
    } else {
      const printContent = document.getElementById(areaId);
      const htmlContent = styleTags + printContent.innerHTML
      var WinPrint = window.open('', '', 'width=900,height=650');
      WinPrint.document.write(htmlContent);
      // WinPrint.document.write(printContent[0].innerHTML);
      WinPrint.document.close();
      WinPrint.focus();
      WinPrint.print();
      WinPrint.close();
    }

  }

  addNewAddress(type) {
    if (type == 'hide') {
      this.showAddAddresSection = 0;
    } else {
      this.showAddAddresSection = 1;
    }
  }
  removeAddress() {
    if (this.selectedAddressId != '' && this.selectedAddressId != null) {
      this.customerManageService.customerAddressRemove({
        "customer_id": this.SelectedCustomer.id,
        "address_id": this.selectedAddressId,

      }).subscribe(res => {
        this.customerManageService.customerAddressList({ customer_id: this.SelectedCustomer.id }).subscribe(res => {
          this.customerAddressList = res;
          this.showAddAddresSection = 0;
        });
      })
    } else {
      return false;
    }

  }
  storeNewAddress() {
    this.customerManageService.customerAddressSave({
      "customer_id": this.SelectedCustomer.id,
      "no": this.customerAddress['no'],
      "street_1": this.customerAddress['street_1'],
      "street_2": this.customerAddress['street_2'],
      "region": this.customerAddress['region'],
      "country": this.customerAddress['country'],
      "zipcode": this.customerAddress['zipcode'],
      "landmark": this.customerAddress['landmark'],
      "phone": this.customerAddress['phone'],
      "status": 1,

    }).subscribe(res => {
      this.customerManageService.customerAddressList({ customer_id: this.SelectedCustomer.id }).subscribe(res => {
        this.customerAddressList = res;
        this.showAddAddresSection = 0;
      });
    });
  }


  homeRedirect() {
    this.modalRef.hide();
    this._router.navigate(['/']);
  }
  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;
  }

  discountOpenModal(template) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm modal-dialog-centered', backdrop: true, ignoreBackdropClick: true });
  }

  validateCoupon() {
    this.billing.couponValidate({ "coupon": this.coupon, "customer_id": this.cartitems.customer_id }).subscribe(res => {
      this.coupon_status = res;
      if (this.coupon_status != null) {
        this.modalRef.hide();
        if (this.coupon_status.discount_type == "percent") {
          this.cartitems.grand_discount = (this.cartitems.grand_discount + ((this.coupon_status.discount_value * 0.01) * this.cartitems.grand_total)).toFixed(2);
          this.cartitems.grand_total = (this.cartitems.grand_total - ((this.coupon_status.discount_value * 0.01) * this.cartitems.grand_total)).toFixed(2);
          this.coupon_value = ((this.coupon_status.discount_value * 0.01) * this.cartitems.grand_total).toFixed(2);
        } else {
          this.cartitems.grand_discount = (this.cartitems.grand_discount + this.coupon_status.discount_value).toFixed(2);
          this.cartitems.grand_total = (this.cartitems.grand_total - this.coupon_status.discount_value).toFixed(2);
          this.coupon_value = this.coupon_status.discount_value.toFixed(2);
        }
        this.coupon_flag = 1;
      } else {
        this.coupon_flag = 0;
        this.cartitems.grand_discount = this.cartitems.grand_discount;
        this.cartitems.grand_total = this.cartitems.grand_total;
        this.coupon_value = this.coupon_value;
        this.couponError = 'Coupon is Invaild / Expired';
      }
    });
  }

  extraModifier: any = [];

  addExtraModifier(modifiers){
    this.extraModifier.push(modifiers);
  }

  cancelItem(){
    this.extraModifier = [];
    this.modalRef.hide();
  }
  del(i,modifie){
   console.log(i);
   this.extraModifier.splice(i,1);
   console.log(this.extraModifier);
  }
}
