import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DineinSelectionComponent } from './dinein-selection/dinein-selection.component'
import { DineinCustomersComponent } from './dinein-customers/dinein-customers.component'
import { DineinOrderComponent } from './dinein-order/dinein-order.component'
import { DineReservationComponent } from './dine-reservation/dine-reservation.component'
import { DineWaitinglistComponent } from './dine-waitinglist/dine-waitinglist.component'
import { DineinServersComponent } from './dinein-servers/dinein-servers.component'
import { DineCustomerinforamtionComponent } from './dine-customerinforamtion/dine-customerinforamtion.component';
import { MenucategoryComponent } from './menucategory/menucategory.component';
import { MenucategoryListComponent } from './menucategory-list/menucategory-list.component';

import { OrderlistComponent } from './orderlist/orderlist.component';
import { KotDineinComponent } from './kot-dinein/kot-dinein.component';
import { DineinTableDetailsComponent } from './dinein-table-details/dinein-table-details.component';
import { OrderlistSearchComponent } from './orderlist-search/orderlist-search.component';
import { BillscreenComponent } from './billscreen/billscreen.component';



const routes: Routes = [
  { path: 'DineinSelection', component: DineinSelectionComponent, pathMatch: 'full' },
  { path: 'DineinCustomers', component: DineinCustomersComponent, pathMatch: 'full' },
  { path: 'DineinOrder', component: DineinOrderComponent },
  { path: 'DineinReservation', component: DineReservationComponent },
  { path: 'DineinWaitingList', component: DineWaitinglistComponent },
  { path: 'DineinServers', component: DineinServersComponent },
  { path: 'DineinCustomerinforamtion', component: DineCustomerinforamtionComponent },
  { path: 'DineinMenuCategory', component: MenucategoryComponent },
  { path: 'DineinMenuListCategory', component: MenucategoryListComponent },
  { path: 'DineinKot', component: KotDineinComponent },
  { path: 'DineinTableDetails', component: DineinTableDetailsComponent },
  { path: 'DineinMenuOrderList', component: OrderlistComponent },
  { path: 'DineinMenuListCategorySearch', component: OrderlistSearchComponent },  
  { path: 'DineinMenuOrder', component: BillscreenComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DineinRoutingModule { }
