import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineCustomerinforamtionComponent } from './dine-customerinforamtion.component';

describe('DineCustomerinforamtionComponent', () => {
  let component: DineCustomerinforamtionComponent;
  let fixture: ComponentFixture<DineCustomerinforamtionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineCustomerinforamtionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineCustomerinforamtionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
