import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import Keyboard from "simple-keyboard";
import {IMyOptions} from 'mydatepicker';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { TableManageService } from './../../@service/table';
import { CustomerManageService } from './../../@service/customer';

@Component({
  selector: 'app-dine-customerinforamtion',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dine-customerinforamtion.component.html',
  styleUrls: ['./dine-customerinforamtion.component.css',
    '../../../../node_modules/simple-keyboard/build/css/index.css']
})
export class DineCustomerinforamtionComponent implements OnInit {

  public customer = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "gender": "",
    "email": ""
  };
  public exCustomer: false | boolean;
  public search: any;


  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public Date; 

  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private customerManageService: CustomerManageService,
    private tableManageService: TableManageService,
    private utilityService: UtilityService,
    ) { }

  ngOnInit() {
  }

  searchCustomers(data){
    this.customerManageService.customerSearch(data.target.value).subscribe(res => {
      if(res){  
        this.customer = res[0]; 
        this.exCustomer = true;
        console.log('search customer =>', this.customer);
      }else{
        this.exCustomer = false;
        this.search = data.target.value;
        this.resetCustomer();
        this.resetLocalStorageCustomer();
      }
    })
  }
  resetCustomer(){
    this.customer = {
      "first_name": "",
      "last_name": "",
      "phone": this.search,
      "gender": "",
      "email": ""
    };
  }
  resetLocalStorageCustomer(){        
    this.utilityService.setSelectedCustomer({});
  }
  saveCustomer(){
    if(this.exCustomer == true){
      // Save Customer in local storage
      this.utilityService.setSelectedCustomer(this.customer);
      console.log('add ex customer in local storage');
      this._router.navigate(['dinein/DineinMenuListCategory']);
    }else{
      // add new customer
      this.customerManageService.customerSave({
        "first_name": this.customer.first_name,
        "last_name": this.customer.last_name,
        "gender": this.customer.gender,
        "email": this.customer.email,
        "mobile_number": this.customer.phone,
        "type":1,
        "phone": this.customer.phone,
        "mobile_key": this.customer.phone,
      }).subscribe(res => {
          this.customerManageService.customerSearch(this.search).subscribe(res => {
            this.customer = res[0];
            this.utilityService.setSelectedCustomer(this.customer);
            this._router.navigate(['dinein/DineinMenuListCategory']);
          });
          console.log(res);
      });
    }
  }


}
