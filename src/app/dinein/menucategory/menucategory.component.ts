import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { MenuManageService } from './../../@service/menu';

@Component({
  selector: 'app-menucategory',
  templateUrl: './menucategory.component.html',
  styleUrls: ['./menucategory.component.css']
})
export class MenucategoryComponent implements OnInit {


  public menuCategory;
  public menuGroup;
  public selectedTable=[];
  public selectedtablename = [];
  public Numseats;

  constructor(public _router: Router, 
    
    private utilityService: UtilityService, private headerService: HeaderService,private menuManageService: MenuManageService) { }

  ngOnInit() {
    this.headerService.setTitle('Menu Category');
    this.selectedTable= this.utilityService.getTableSelection();
    this.selectedtablename= this.utilityService.getTablenames();
    this.Numseats= this.utilityService.getNumSeat();
    this.menuManageService.menuCategory().subscribe(res => {
      this.menuCategory = res
    });
    this.menuManageService.menuGroup().subscribe(res => {
      this.menuGroup = res
    });
  }

}
