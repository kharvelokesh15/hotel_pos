import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-dinein-table-details',
  templateUrl: './dinein-table-details.component.html',
  styleUrls: ['./dinein-table-details.component.css']
})
export class DineinTableDetailsComponent implements OnInit {
  modalRef: BsModalRef;
  public selectedModule;
  constructor(private modalService: BsModalService) { }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'customClass_receipt',
      animated: true,
      // backdrop: 'static'
    });
  }

  ngOnInit() {
  }
  
  

}
