import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineinTableDetailsComponent } from './dinein-table-details.component';

describe('DineinTableDetailsComponent', () => {
  let component: DineinTableDetailsComponent;
  let fixture: ComponentFixture<DineinTableDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineinTableDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineinTableDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
