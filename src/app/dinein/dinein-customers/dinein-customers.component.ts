import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { TableManageService } from './../../@service/table';
import { CustomerManageService } from './../../@service/customer';

@Component({
  selector: 'app-dinein-customers',
  templateUrl: './dinein-customers.component.html',
  styleUrls: ['./dinein-customers.component.css']
})
export class DineinCustomersComponent implements OnInit {
  public customerData;
  public SelectedCustomer;
  public selectedModule;
  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private customerManageService: CustomerManageService,
    private tableManageService: TableManageService,
    private utilityService: UtilityService) { }

  ngOnInit() {
    this.selectedModule = this.utilityService.GetModuleInfo();
    this.headerService.setTitle(this.selectedModule.Module+'-DINE IN');
    this.SelectedCustomer= this.utilityService.getSelectedCustomer();
    this.customerManageService.customerList().subscribe(res => {
      this.customerData = res
      console.log(res);
    })
    
  }
  custometSelect(customer){
    this.SelectedCustomer = customer;
    this.utilityService.setSelectedCustomer(this.SelectedCustomer);
  }
  RemoveCustomer(){
    this.SelectedCustomer = {};
    this.utilityService.setSelectedCustomer(this.SelectedCustomer);
  }

}
