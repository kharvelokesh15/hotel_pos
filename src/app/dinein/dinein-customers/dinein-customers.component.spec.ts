import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineinCustomersComponent } from './dinein-customers.component';

describe('DineinCustomersComponent', () => {
  let component: DineinCustomersComponent;
  let fixture: ComponentFixture<DineinCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineinCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineinCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
