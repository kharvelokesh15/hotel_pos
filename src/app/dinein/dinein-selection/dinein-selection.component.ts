import { Component, OnInit , TemplateRef ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { TableManageService } from './../../@service/table';
import { KOTManageService } from './../../@service/kot';
import { CustomerManageService } from './../../@service/customer';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-dinein-selection',
  templateUrl: './dinein-selection.component.html',
  styleUrls: ['./dinein-selection.component.css']
})
export class DineinSelectionComponent implements OnInit {
  @ViewChild('template', {static: true}) template: TemplateRef<any>;
  @ViewChild('templatetablefull', {static: true}) templatetablefull: TemplateRef<any>;
  
  public customer = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "gender": "",
    "email": ""
  };
  public exCustomer: false | boolean;
  public search: any;


  public floorList;
  public SelectedFloor;
  public tableList;
  public selectedTable=[];
  public selectedtablename = [];
  public Numseats;
  public customerData;
  public customerQuery;
  public SelectedCustomer;
  public table_seats_avail = [];
  public kotList;
  public CurentStaff;
  modalRef: BsModalRef; 
 
  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private tableManageService: TableManageService,
    private KOTManageService: KOTManageService,
    private utilityService: UtilityService,
    private modalService: BsModalService, 
    private customerManageService: CustomerManageService,
    ) {  }

  ngOnInit() {
    
    this.headerService.setTitle('DINE IN');
    this.selectedTable= this.utilityService.getTableSelection();
    this.selectedtablename= this.utilityService.getTablenames();
    this.Numseats= this.utilityService.getNumSeat();
    this.customerSerch();
    this.tableManageService.floorList().subscribe(res => {
      this.floorList = res
      //console.log(res);
      this.SelectedFloor = this.floorList[0].id
      this.GetFloor(this.SelectedFloor);
      this.tableAvailDetails(this.SelectedFloor);
      console.log(this.SelectedFloor);
    })
  }

  redirectPay(){
    this.CurentStaff = this.utilityService.getCurrentUser();
    console.log(this.kotList[0].customer_id);
    this.modalRef.hide();
    this._router.navigate(['/receipt/DineinNormalBill/'+this.CurentStaff.id+'/'+this.kotList[0].customer_id]);
  }


  openModalWithClass(template: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(  
      template,{animated: true, backdrop: 'static',class: 'modal-sm popup-min' }  
      
    );  
  } 
  openModalTableFullTemplate(templatetablefull: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(  
      templatetablefull,{animated: true, class:"popup-min" }  
      
    );  
  } 
  customerSerch(){
    this.customerManageService.customerSearch(this.customerQuery).subscribe(res => {
      this.customerData = res.slice(0, 7)
      console.log(res);
    })
  }
  pressedKey(input: any) {
  if(input == "clear"){
    this.Numseats="";
    this.utilityService.setNumSeat(this.Numseats);
  }else{
    this.Numseats = this.Numseats+""+input;
    this.utilityService.setNumSeat(this.Numseats);
  }
  }
  custometSelect(customer){
    this.SelectedCustomer = customer;
    this.utilityService.setSelectedCustomer(this.SelectedCustomer);
  }
  enterSeat(){
    this.utilityService.setNumSeat(this.Numseats);
  }

  GetFloor(SelectedFloor){
    this.tableManageService.tableList(SelectedFloor).subscribe(res => {
      this.tableList = res
      console.log(this.tableList);
    })
  }

  onFloorSelected(SelectedFloor){
    this.GetFloor(SelectedFloor);
    this.tableAvailDetails(SelectedFloor);
  }

  tableAvailDetails(floor_id){
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    this.tableManageService.tableAvailDetails({"floor_id":floor_id,"current_time":time}).subscribe(res=>{
      console.log(res);
      this.table_seats_avail= res.table_seats_avail;
    })
  }
  addNewKot(kotlist){
    console.log(kotlist);
    this.SelectedCustomer = {"id":kotlist[0].customer_id,"first_name":kotlist[0].customer_name}
    this.utilityService.setSelectedCustomer(this.SelectedCustomer);
    this._router.navigate(['/dinein/DineinMenuListCategory']);
    this.modalRef.hide();
  }
  CheckAvilable(table_id){
   let status = true;
  //  console.log(table_id);
  //  console.log(this.table_seats_avail);
    this.table_seats_avail.forEach(element => {
      if(element.table_id == table_id){
        status =  (element.table_seat_used > 0)
      }
    });
    return status;
  }

  GetAvilable(table_id): any{
    let tableinfo = [];
   //  console.log(table_id);
    // console.log(this.table_seats_avail);
     this.table_seats_avail.forEach(element => {
       if(element.table_id == table_id){
        tableinfo =  element
       }
     });
     return tableinfo;
   }

  selectTable(table){
    console.log(table);
    this.selectedTable = [];
    this.selectedtablename = [];
    if(this.selectedTable.indexOf(table.table_id+'-'+table.floor_id) !== -1){
      this.selectedTable.splice(this.selectedTable.indexOf(table.table_id+'-'+table.floor_id),1);
      this.selectedtablename.splice(this.selectedtablename.indexOf(table.table_code),1);     
    }else{
      this.selectedTable.push(table.table_id+'-'+table.floor_id);
      this.selectedtablename.push(table.table_code);
    }
    if(this.GetAvilable(table.table_id) && !(this.GetAvilable(table.table_id).table_seat_avail) && !(this.GetAvilable(table.table_id).table_seat_avail >= 0)){
      if(table.status == 1){        
        this.openModalWithClass(this.template);
      }
    }
    else{
    // alert("Table is Full");
    // console.log('table', table);
    this.KOTManageService.tableFullKotDetails({
              "waiter_id": "",
              "customer_id": "",
              "floor_id": table.floor_id,
              "table_id": table.table_id
            }).subscribe(res=>{
      console.log('table full =>', res)
      this.kotList = res;
    })
    this.openModalTableFullTemplate(this.templatetablefull);
  }
  this.utilityService.setTableSelection(this.selectedTable,this.selectedtablename);
  }

  searchCustomers(data){
    this.customerManageService.customerSearch(data.target.value).subscribe(res => {
      if(res){  
        this.customer = res[0]; 
        this.exCustomer = true;
        console.log('search customer =>', this.customer);
      }else{
        this.exCustomer = false;
        this.search = data.target.value;
        this.resetCustomer();
        this.resetLocalStorageCustomer();
      }
    })
  }
  resetCustomer(){
    this.customer = {
      "first_name": "",
      "last_name": "",
      "phone": this.search,
      "gender": "",
      "email": ""
    };
  }
  resetLocalStorageCustomer(){        
    this.utilityService.setSelectedCustomer({});
  }
  saveCustomer(){
    if(this.exCustomer == true){
      // Save Customer in local storage
      this.utilityService.setSelectedCustomer(this.customer);
      this.modalRef.hide();
      console.log('add ex customer in local storage');
    }else{
      // add new customer
      this.customerManageService.customerSave({
        "first_name": this.customer.first_name,
        "last_name": this.customer.last_name,
        "gender": this.customer.gender,
        "email": this.customer.email,
        "mobile_number": this.customer.phone,
        "type":1,
        "phone": this.customer.phone,
        "mobile_key": this.customer.phone,
      }).subscribe(res => {
          this.customerManageService.customerSearch(this.search).subscribe(res => {
            this.customer = res[0];
            this.utilityService.setSelectedCustomer(this.customer);
          });
          this.modalRef.hide();
      });
    }
  }

}
