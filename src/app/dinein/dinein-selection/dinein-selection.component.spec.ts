import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineinSelectionComponent } from './dinein-selection.component';

describe('DineinSelectionComponent', () => {
  let component: DineinSelectionComponent;
  let fixture: ComponentFixture<DineinSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineinSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineinSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
