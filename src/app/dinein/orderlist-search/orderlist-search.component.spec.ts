import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderlistSearchComponent } from './orderlist-search.component';

describe('OrderlistSearchComponent', () => {
  let component: OrderlistSearchComponent;
  let fixture: ComponentFixture<OrderlistSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderlistSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderlistSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
