import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineinOrderComponent } from './dinein-order.component';

describe('DineinOrderComponent', () => {
  let component: DineinOrderComponent;
  let fixture: ComponentFixture<DineinOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineinOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineinOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
