import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dinein-order',
  templateUrl: './dinein-order.component.html',
  styleUrls: ['./dinein-order.component.css']
})
export class DineinOrderComponent implements OnInit {
  public orderData;
  constructor() { }

  ngOnInit() {
    this.orderData = [
      {
        Floor: '1 Floor', Table: 'T1', OrderNo: '84', TypeServer: 'Dine IN / Davis'
      },
      {
        Floor: '2 Floor', Table: 'T2', OrderNo: '85', TypeServer: 'Take Away / John'
      },
      {
        Floor: '3 Floor', Table: 'T3', OrderNo: '86', TypeServer: 'Dine IN / Davis'
      },
      {
        Floor: '4 Floor', Table: 'T4', OrderNo: '87', TypeServer: 'Dine IN / Davis'
      },
      {
        Floor: '5 Floor', Table: 'T5', OrderNo: '88', TypeServer: 'Take Away / Jerry'
      },
      {
        Floor: '6 Floor', Table: 'T6', OrderNo: '89', TypeServer: 'Take Away / Steve'
      },
      {
        Floor: '7 Floor', Table: 'T7', OrderNo: '90', TypeServer: 'Dine IN / Davis'
      },
      {
        Floor: '8 Floor', Table: 'T8', OrderNo: '91', TypeServer: 'Dine IN / Davis'
      },
    ]
  }

}
