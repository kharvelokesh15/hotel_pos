import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillscreenComponent } from './billscreen.component';

describe('BillscreenComponent', () => {
  let component: BillscreenComponent;
  let fixture: ComponentFixture<BillscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
