import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { HeaderService } from './../../header.service';
import { KOTManageService, InvoieManageService } from './../../@service/kot';

import { BillingService } from './../../@service/billing/billing.service';
import { TableManageService } from './../../@service/table/';
import { UtilityService } from '../../@service';
import { CustomerManageService } from './../../@service/customer';

@Component({
	selector: 'app-billscreen',
	templateUrl: './billscreen.component.html',
	styleUrls: ['./billscreen.component.css']
})
export class BillscreenComponent implements OnInit {
	public split_bill_save;
	public CurentStaff;
	modalRef: BsModalRef;
	split_by_amount: any;
	splitError: any;
	splitpaid: any;
	splitbalance: any;
	split_type: any;
	split_index: any;
	split_bill_save_array: any;
	card_name: any;
	card_number: any;
	card_type: any;
	total_data_entered: any = 0;
	show_completed: any = 0;
	counter: any = 0;
	new_split_by_amount: any;
	invoice_id: any;
	invoice_details: any;
	showSection: any;
	public address_no = '';
	public address_street_1 = '';
	public address_street_2 = '';
	public address_region = '';
	public address_country = '';
	public address_landmark = '';
	public address_zipcode = '';
	public delivery_address = '';
	public invoiceDetailsData;
	public invoiceWaiter;
	public invoiceCustomer;
	public kotDetailMenu;
	public invoiceOrganisation;
	rewardData: any = [];
	currency;
	enableFlag: boolean = false;
	ok_button: boolean = false;
	sales_type;

	constructor(public route: ActivatedRoute, public _router: Router, private headerService: HeaderService, private kot
		: KOTManageService, private customer: CustomerManageService, private billing: BillingService, private table: TableManageService,
		private utilityService: UtilityService, private invoice
			: InvoieManageService, private modalService: BsModalService) { }

	openModal(template: TemplateRef<any>, type) {
		this.split_type = type;
		this.modalRef = this.modalService.show(template, {
			class: 'customClass_500',
			animated: true,
			// backdrop: 'static'
		});
	}
	typeAssign(type, index) {
		this.split_type[index] = type;
	}

	ngOnInit() {
		this.headerService.setTitle('Bill Payment');
		this.split_bill_save = this.utilityService.getsplitbillvalues();
		this.CurentStaff = this.utilityService.getCurrentUser();
		// this.CurentStaff = 
		this.currency = this.utilityService.getCurrency();
		this.showSection = this.split_bill_save.payent_type;
		this.split_by_amount = this.split_bill_save.split_by_amount;
		this.counter = this.split_bill_save.total_no_of_split_bill;

		this.splitError = new Array();
		this.splitpaid = new Array(this.counter);
		this.splitbalance = new Array(this.counter);
		this.split_type = new Array(this.counter);
		this.split_bill_save_array = new Array(this.counter);
		this.card_name = new Array(this.counter);
		this.card_number = new Array(this.counter);
		this.card_type = new Array(this.counter);

		for (let index = 0; index < this.splitpaid.length; index++) {
			this.splitpaid[index] = 0;
		}
		for (let index = 0; index < this.splitpaid.length; index++) {
			this.splitbalance[index] = 0;
		}

		if (this.showSection == 'cod') {
			let selectAddress = this.utilityService.getSelectedAddress();
			this.address_no = selectAddress[0].no;
			this.address_street_1 = selectAddress[0].street_1;
			this.address_street_2 = selectAddress[0].street_2;
			this.address_region = selectAddress[0].region;
			this.address_country = selectAddress[0].country;
			this.address_landmark = selectAddress[0].landmark;
			this.address_zipcode = selectAddress[0].zipcode;
		}

		this.customer.customerRewards({ "customer_id": this.split_bill_save.customer_id[0] }).subscribe(res => {
			this.rewardData = res;
			// console.log(Object.keys(this.rewardData).length);
			if (Object.keys(this.rewardData).length > 3) {
				// if(this.rewardData > 0){
				this.rewardData = res;
				this.enableFlag = true;
			} else {
				this.rewardData = res.data;
				this.enableFlag = false;
			}
		});

	}

	checkingvalidtion(new_val, i, amt, type = "") {
		if (type == 'points') {
			if ((this.rewardData.balance_amt - this.rewardData.mini_usage) >= new_val) {
				this.splitpaid[i] = new_val;
				this.splitbalance[i] = new_val - amt;
				this.ok_button = true;
			} else {
				this.ok_button = false;
			}

		} else {
			this.splitpaid[i] = new_val;
			this.splitbalance[i] = new_val - amt;
			this.ok_button = true;
		}

		if (parseFloat(this.splitpaid[i]) >= parseFloat(this.split_by_amount[i])) {
			this.ok_button = false;
			this.splited_data_save(i);
			// this.show_completed = 1;
			// this.total_data_entered++;
		} else {
			this.show_completed = 0;
			this.ok_button = true;
		}
	}

	splited_data_save(index) {

		if (this.splitpaid[index] != '' && this.splitpaid[index] > 0) {
			// if(this.splitpaid[index] >= this.split_by_amount[index]){
			let check_bal = 0;
			// for cash and card		
			if (parseFloat(this.splitpaid[index]) < parseFloat(this.split_by_amount[index])) {
				this.counter = (this.split_by_amount.length + 1);
				this.split_by_amount[this.split_by_amount.length] = this.split_by_amount[index] - this.splitpaid[index];
				check_bal = 1;
			}

			this.total_data_entered++;

			// console.log(this.total_data_entered+'======'+this.split_by_amount.length);
			if (this.total_data_entered == this.split_by_amount.length) {
				this.show_completed = 1;
			}

			let grand;
			if (this.split_bill_save.is_split_bill == 0) {

				if ((this.splitpaid.length > 1)) {
					console.log(this.splitpaid.length - 1 == index);
					grand = (this.splitpaid.length - 1 == index) ? this.split_by_amount[index - 1] - this.splitpaid[index - 1] : this.splitpaid[index];
				} else {
					grand = check_bal == 1 ? this.splitpaid[index] : this.split_bill_save.grand_total;
				}
			} else {
				grand = this.split_by_amount[index]
			}

			this.split_bill_save_array[index] = {
				"kot_id": this.split_bill_save.kot_id,
				"customer_id": this.split_bill_save.customer_id[0],
				"kot_type": this.split_bill_save.kot_type[0],
				"app_user_id": this.split_bill_save.app_user_id,
				"over_all_qty": this.split_bill_save.over_all_qty,
				"payent_type": this.split_type[index],
				"used_rewards": (this.split_type[index] == 'points') ? this.splitpaid[index] : 0,
				"waiter_id": this.split_bill_save.waiter_id,
				"sub_total": this.split_bill_save.sub_total,
				"tax_total": "0.00",
				"discount_amount": this.split_bill_save.discount_amount,
				// "grand_total": (this.split_bill_save.is_split_bill == 0) ? ((this.splitpaid.length > 1) ? this.splitpaid[index] : this.split_bill_save.grand_total) :this.split_by_amount[index],
				"grand_total": grand,
				"paid_amount": this.splitpaid[index],
				"balance_to_customer": (this.splitbalance[index] >= 0) ? this.splitbalance[index] : 0,
				"balance_to_res": (this.splitbalance[index] < 0) ? this.splitbalance[index] : 0,
				"is_split_bill": this.split_bill_save.is_split_bill[0],
				"split_type": this.split_type[index],
				"total_no_of_split_bill": this.split_bill_save.total_no_of_split_bill,
				"notes": "",
				"status": "2",
				"split_by_menu": '0',
				"split_by_amount": (this.split_bill_save.is_split_bill == 0) ? this.splitpaid[index] : this.split_by_amount[index],
				"card_name": this.card_name[index] ? this.card_name[index] : '',
				"card_number": this.card_number[index] ? this.card_number[index] : '',
				"card_type": this.card_type[index] ? this.card_type[index] : '',
				"address_no": '',
				"address_street_1": '',
				"address_street_2": '',
				"address_region": '',
				"address_landmark": '',
				"address_country": '',
				"address_zipcode": '',
				"coupon": this.split_bill_save.coupon,
				"coupon_amount": this.split_bill_save.coupon_amount,
				"floor_id": this.split_bill_save.kot_floor_id[index] ? this.split_bill_save.kot_floor_id[index] : 0,
			}

		} else {
			alert('Enter the Bill Amount');
		}

	}

	splited_save() {
		if (this.total_data_entered == this.split_by_amount.length) {

			if (this.split_bill_save.is_split_bill == 1) {
				for (let index = 0; index < this.split_bill_save_array.length; index++) {
					this.billing.splitBillReceiptSave(this.split_bill_save_array[index]).subscribe(res => {
						this.invoice_details = res;
						this.getinvoice(res.id);
						this.invoice_id = this.invoice_details.id;
						this.sales_type = this.invoice_details.sales_type;
						if (this.split_bill_save_array.length == (index + 1)) {
							if (this.sales_type == 'dinein') {
								let table_data = { "floor_id": this.split_bill_save.kot_floor_id[0], "customer_id": this.split_bill_save.customer_id[0], "table_id": this.split_bill_save.kot_table_id[0], "occupied_chairs": this.split_bill_save.kot_occupied[0], "is_booked": "2", "is_cancel": "" };
								this.table.tableSelectionClose(table_data).subscribe(res => {
									// if(this.split_bill_save_array.length == (index+1)){
									setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
									// }
								});
							} else {
								setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
							}

						}
					});

				}
			} else {
				console.log(this.split_bill_save_array);
				this.billing.splitBillReceiptSave(this.split_bill_save_array).subscribe(res => {
					this.invoice_details = res;
					this.getinvoice(res.id);
					this.invoice_id = this.invoice_details.id;
					let table_data = { "floor_id": this.split_bill_save.kot_floor_id[0], "customer_id": this.split_bill_save.customer_id[0], "table_id": this.split_bill_save.kot_table_id[0], "occupied_chairs": this.split_bill_save.kot_occupied[0], "is_booked": "2", "is_cancel": "" };
					this.sales_type = this.invoice_details.sales_type;
					if (this.sales_type == 'dinein') {
						this.table.tableSelectionClose(table_data).subscribe(res => {
							setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);

						});
					} else {
						setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
					}

				});
			}


		}
		// this._router.navigate(['/invoicelisting/invoiceDetails/'+this.invoice_id]);

		// this._router.navigate(['/kot/KotListing']);
	}

	card_type_assign(card, index) {
		this.card_type[index] = card;

	}

	cash_on_delivery_save() {
		if (this.address_no == '') {
			alert('Address Number Required');
			return;
		}
		if (this.address_street_1 == '') {
			alert('Street Address Required');
			return;
		}
		if (this.address_region == '') {
			alert('Region Required');
			return;
		}
		if (this.address_country == '') {
			alert('Country Required');
			return;
		}

		if (this.address_zipcode == '') {
			alert('Zipcode Required');
			return;
		}
		let data = [];
		data[0] = {
			"kot_id": this.split_bill_save.kot_id,
			"customer_id": this.split_bill_save.customer_id[0],
			"kot_type": 'delivery',
			"app_user_id": this.CurentStaff.id,
			"over_all_qty": this.split_bill_save.over_all_qty,
			"payent_type": 'cod',
			"waiter_id": this.split_bill_save.waiter_id,
			"sub_total": this.split_bill_save.sub_total,
			"used_rewards": '0.00',
			"tax_total": "0.00",
			"discount_amount": this.split_bill_save.discount_amount,
			"grand_total": this.split_bill_save.grand_total,
			"paid_amount": '0.00',
			"balance_to_customer": '0.00',
			"balance_to_res": this.split_bill_save.grand_total,
			"is_split_bill": '0',
			"split_type": 'cod',
			"total_no_of_split_bill": '0',
			"notes": "",
			"status": "1",
			"split_by_menu": '0',
			"split_by_amount": '0',
			"card_name": '',
			"card_number": '',
			"card_type": '',
			"address_no": this.address_no,
			"address_street_1": this.address_street_1,
			"address_street_2": this.address_street_2 ? this.address_street_2 : '',
			"address_region": this.address_region,
			"address_country": this.address_country,
			"address_landmark": this.address_landmark ? this.address_landmark : '',
			"address_zipcode": this.address_zipcode,
			"coupon": '',
			"coupon_amount": "",
			"floor_id": this.split_bill_save.kot_floor_id[0]
		};
		console.log('data', data[0]);
		this.billing.splitBillReceiptSave(data).subscribe(res => {
			this._router.navigate(['/newtrip/newtriplist/']);
		});
	}

	getinvoice(id) {

		this.invoice.invoiceDetailsList(id).subscribe(res => {
			this.invoiceDetailsData = res;
			this.kotDetailMenu = this.invoiceDetailsData.kot_data;
			this.invoiceWaiter = this.invoiceDetailsData.waiter_id;
			this.invoiceCustomer = this.invoiceDetailsData.customer_id;
			this.invoiceOrganisation = this.invoiceDetailsData.organisation;

			setTimeout(function () {
				var printContent = document.getElementsByClassName('print-ticket');
				var WinPrint = window.open('', '', 'width=900,height=650');

				WinPrint.document.write(`<style>
				*.print-ticket {
					font-size: 8px;
					font-family: 'Times New Roman';
					text-transform: uppercase;
				}
				
				td,
				th,
				tr,
				table {
				   /*border-bottom: 2px dotted black;*/
					border-collapse: collapse;
				}
				table {
				   border-bottom: 2px dotted black;
				   width:100%;
					border-collapse: collapse;
				}
				
				td.description,
				th.description {
					width: 40%;
					max-width: 40%;
					font-size: 12px;
				}							
				td.quantity,
				th.quantity {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
					text-align: right;
					font-size: 12px;
				}

				td.price,
				th.price {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
					text-align: right;
				}
				td.total,
				th.total {
					width: 25%;
					max-width: 25%;
					word-break: break-all;
					text-align: right;
					padding-right: 10px;
					font-size: 12px;
				}
				th.item {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
				}
				th.item-value {
					width: 5%;
					max-width: 5%;
					word-break: break-all;
				}
				th.paid-total {
					width: 25%;
					max-width: 25%;
					word-break: break-all;
				}
				th.paid-total-value {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
				}
				th.balance {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
				}
				th.balance-value {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
				}
				
				.centered {
					text-align: center;
					align-content: center;
				}
				
				.ticket {
					width: 100%;
					max-width: 100%;
				}
				
				img.logo-print {
					background-size: cover;
					width: 120px;
					height: 120px;
					border: 5px solid white;
					padding: 0.25rem;
					border-radius: 50%;
				}
				table.inv_head {
					text-align: left;
				}
				.inv_head th.quantity {
					width: 60%;
				}
				.inv_head th.description {
					width: 40%;
				}
				@page { margin: 0; }
				</style>`+ printContent[0].innerHTML);
				WinPrint.document.close();
				WinPrint.focus();
				WinPrint.print();
				WinPrint.close();

			}, 1000);

		});
	}


}
