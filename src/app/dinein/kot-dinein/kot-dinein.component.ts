import { Component, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-kot-dinein',
  templateUrl: './kot-dinein.component.html',
  styleUrls: ['./kot-dinein.component.css']
})
export class KotDineinComponent implements OnInit {

  constructor() { }

  public floors : any;
  public areas : any;

  ngOnInit() {
    this.floors=[
      {optionname:"3 Floor"},
      {optionname:"4 Floor"},
      {optionname:"5 Floor"}
    ];
    this.areas=[
      {optionname:"Main Area"},
      {optionname:"Sub Area"}

    ];
  }

}
