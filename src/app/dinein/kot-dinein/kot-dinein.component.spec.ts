import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KotDineinComponent } from './kot-dinein.component';

describe('KotDineinComponent', () => {
  let component: KotDineinComponent;
  let fixture: ComponentFixture<KotDineinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KotDineinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KotDineinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
