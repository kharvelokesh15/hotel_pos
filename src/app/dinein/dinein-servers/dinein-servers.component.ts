import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dinein-servers',
  templateUrl: './dinein-servers.component.html',
  styleUrls: ['./dinein-servers.component.css']
})
export class DineinServersComponent implements OnInit {
  public serversData
  constructor() { }

  ngOnInit() {
    this.serversData = [
      { Servers: 'Davis', Table: 'T2', Type: 'Dine IN', GuestName: 'Guest 1' },
      { Servers: 'John', Table: 'T3', Type: 'Take Away', GuestName: 'Guest 2' },
      { Servers: 'Davis', Table: 'T4', Type: 'Dine IN', GuestName: 'Guest 3' },
      { Servers: 'Davis', Table: 'T5', Type: 'Dine IN', GuestName: 'Guest 4' },
      { Servers: 'Jerry', Table: 'T6', Type: 'Take Away', GuestName: 'Guest 5' },
      { Servers: 'Steve', Table: 'T7', Type: 'Dine IN', GuestName: 'Guest 6' },
      { Servers: 'Davis', Table: 'T8', Type: 'Dine IN', GuestName: 'Guest 7' },
      { Servers: 'Davis', Table: 'T1', Type: 'Dine IN', GuestName: 'Guest 8' },
    ]
  }

}
