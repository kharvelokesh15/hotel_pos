import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineinServersComponent } from './dinein-servers.component';

describe('DineinServersComponent', () => {
  let component: DineinServersComponent;
  let fixture: ComponentFixture<DineinServersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineinServersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineinServersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
