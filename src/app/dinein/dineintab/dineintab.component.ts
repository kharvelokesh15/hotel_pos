import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
@Component({
  selector: 'app-dineintab',
  templateUrl: './dineintab.component.html',
  styleUrls: ['./dineintab.component.css']
})
export class DineintabComponent implements OnInit {
  public selectedModule;
  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private utilityService: UtilityService) { }
  @Input() activeTab;
  ngOnInit() {
    this.selectedModule = this.utilityService.GetModuleInfo();
    console.log(this.selectedModule);
  }

}
