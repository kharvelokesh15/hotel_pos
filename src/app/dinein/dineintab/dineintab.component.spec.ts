import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DineintabComponent } from './dineintab.component';

describe('DineintabComponent', () => {
  let component: DineintabComponent;
  let fixture: ComponentFixture<DineintabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DineintabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DineintabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
