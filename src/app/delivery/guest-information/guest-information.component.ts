import { Component, OnInit } from '@angular/core';
import { HeaderService } from './../../header.service';
import {IMyOptions} from 'mydatepicker' 

@Component({
  selector: 'app-guest-information',
  templateUrl: './guest-information.component.html',
  styleUrls: ['./guest-information.component.css']
})
export class GuestInformationComponent implements OnInit {
  ordermenus: string[] = ['Address ', 'Other Details'];
  selectedorder = this.ordermenus[0];
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public DOB; 
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Delivery');
  }

}
