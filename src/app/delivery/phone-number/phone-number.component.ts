import { Component, OnInit,ViewChildren  } from '@angular/core';
import { Router } from '@angular/router'
//services
import { HeaderService } from './../../header.service'

@Component({
  selector: 'app-phone-number',
  templateUrl: './phone-number.component.html',
  styleUrls: ['./phone-number.component.css']
})
export class PhoneNumberComponent implements OnInit {
  @ViewChildren('passwordRef') passwordRef: any;
  passwordOne: number | string;
  passwordTwo: number | string;
  passwordThree: number | string;
  passwordFour: number | string;
  currentlyFocused: number = 1;
  constructor(public _router: Router , private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Delivery');
  }

  changeFocusToNext(key: any, num: number) {
    if (key != "deleteContentBackward") {
      this.passwordRef.first.nativeElement.children[num].focus();
    }
  }


  pressedKey(input: any) {
    if (input == "clear") {
      this.passwordOne = "";
      this.passwordTwo = "";
      this.passwordThree = "";
      this.passwordFour = "";
      this.currentlyFocused = 1;
    } else {
      switch (this.currentlyFocused) {
        case 1: this.passwordOne = input;
          this.changeFocusToNext("1", 1);
          break;
        case 2: this.passwordTwo = input;
          this.changeFocusToNext("2", 2);
          break;
        case 3: this.passwordThree = input;
          this.changeFocusToNext("3", 3);
          break;
        case 4: this.passwordFour = input;
          this.changeFocusToNext("3", 3);
          break;
      }
    }

  }

}
