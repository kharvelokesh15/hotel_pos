import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DeliveryRoutingModule } from './delivery-routing.module';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { GuestInformationComponent } from './guest-information/guest-information.component';
import { MenuScreenComponent } from './menu-screen/menu-screen.component';


@NgModule({
  declarations: [PhoneNumberComponent, GuestInformationComponent, MenuScreenComponent],
  imports: [
    CommonModule,
    DeliveryRoutingModule,
    FormsModule,
    HeaderModule,
    MyDatePickerModule,
    ModalModule.forRoot()
  ]
})
export class DeliveryModule { }
