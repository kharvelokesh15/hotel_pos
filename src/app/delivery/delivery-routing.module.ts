import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { MenuScreenComponent } from './menu-screen/menu-screen.component';
import { GuestInformationComponent } from './guest-information/guest-information.component';





const routes: Routes = [
  { path: 'PhoneNumber', component: PhoneNumberComponent, pathMatch: 'full' },
  { path: 'MenuScreen', component: MenuScreenComponent},
  { path: 'GuestInformation', component: GuestInformationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule { }
