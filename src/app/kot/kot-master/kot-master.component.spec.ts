import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KotMasterComponent } from './kot-master.component';

describe('KotMasterComponent', () => {
  let component: KotMasterComponent;
  let fixture: ComponentFixture<KotMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KotMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KotMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
