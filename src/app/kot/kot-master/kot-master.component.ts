import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {IMyOptions, IMyDate} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { KOTManageService } from './../../@service/kot';

@Component({
  selector: 'app-kot-master',
  templateUrl: './kot-master.component.html',
  styleUrls: ['./kot-master.component.css']
})
export class KotMasterComponent implements OnInit {
  public KotReceiptListData;
  public CurentStaff;
  public KotGroup;

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public KOTDate: IMyDate = {year: 0, month: 0, day: 0};
  iskotReceiptlist: boolean = false;
  checkAllkotreceipt: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService,private kot
    : KOTManageService,private utilityService: UtilityService) { 
      
    }

  ngOnInit() {
    let d: Date = new Date();
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.KOTDate = {year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate()};
    this.headerService.setTitle('KOT');
    this.kot.kotList({"kot_date":"","kot_type":""}).subscribe(res => {
      this.KotReceiptListData = res
      this.KotGroup = this.KotReceiptListData.reduce((r,{customer_id})=>{
        if(!r.some(o=>o.customer_id==customer_id)){
          r.push({waiter_id:this.CurentStaff.id,customer_id,groupItem:this.KotReceiptListData.filter(v=>v.customer_id==customer_id)});
    }
    console.log(this.KotGroup)
    return r;
    },[]);
    });

  }

  changekotReceiptList(event) {
    if (event.target.name == 'KotReceiptListData') {
      this.iskotReceiptlist = true
    }

    if (this.iskotReceiptlist && this.checkAllkotreceipt) {
      event.target.checked = true
    }
  }
  GetTableid(tables){
    return tables.map(v=>v.table_name).join(', ');
  }
  allkotReceiptList(event) {
    const checked = event.target.checked;
    this.KotReceiptListData.forEach(item => item.selected = checked);
  }
  


}


