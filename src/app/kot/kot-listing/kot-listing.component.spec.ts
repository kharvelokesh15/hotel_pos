import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KotListingComponent } from './kot-listing.component';

describe('KotListingComponent', () => {
  let component: KotListingComponent;
  let fixture: ComponentFixture<KotListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KotListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KotListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
