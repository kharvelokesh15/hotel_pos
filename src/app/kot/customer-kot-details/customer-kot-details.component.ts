import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { KOTManageService } from './../../@service/kot';


@Component({
  selector: 'app-kot-details',
  templateUrl: './customer-kot-details.component.html',
  styleUrls: ['./customer-kot-details.component.css']
})
export class CustomerKotDetailsComponent implements OnInit {
  public customer_id;
  public waiter_id;
  public kotDetailsData;
  public kotDetailCustomer;
  constructor(
    // public _route: Route,
    public route: ActivatedRoute, private headerService: HeaderService,private kot
    : KOTManageService
  ) { }

  ngOnInit() {
    this.waiter_id = this.route.snapshot.paramMap.get('waiter_id');
    this.customer_id = this.route.snapshot.paramMap.get('customer_id');
    
    this.kot.kotIdList({"waiter_id":this.waiter_id,"customer_id":this.customer_id}).subscribe(res => {
      this.kotDetailsData = res;
      this.kotDetailCustomer = [
        { customer_name: this.kotDetailsData[0].customer_name ,customer_mobile: (this.kotDetailsData[0].customer_mobile?this.kotDetailsData[0].customer_mobile:"-")},
      ]
      // this.kotDetailCustomer['customer_name'] = this.kotDetailsData[0].customer_name;
      // this.kotDetailCustomer['customer_mobile'] = this.kotDetailsData[0].customer_mobile ? this.kotDetailsData[0].customer_mobile : "-";
      console.log(this.kotDetailCustomer);
    });

  }

}
