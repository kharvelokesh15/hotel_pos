import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerKotDetailsComponent } from './customer-kot-details.component';

describe('CustomerKotDetailsComponent', () => {
  let component: CustomerKotDetailsComponent;
  let fixture: ComponentFixture<CustomerKotDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerKotDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerKotDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
