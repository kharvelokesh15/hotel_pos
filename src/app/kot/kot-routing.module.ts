import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KotListingComponent } from './kot-listing/kot-listing.component';
import { KotCallListingComponent } from './kot-call-listing/kot-call-listing.component';
import { KotMasterComponent } from './kot-master/kot-master.component';
import { KotDetailsComponent } from './kot-details/kot-details.component';
import { CustomerKotDetailsComponent } from './customer-kot-details/customer-kot-details.component';

const routes: Routes = [
  { path: 'KotListing', component: KotListingComponent, pathMatch: 'full' },
  { path: 'CallCenterListing', component: KotCallListingComponent, pathMatch: 'full' },
  { path: 'KotMasterListing', component: KotMasterComponent, pathMatch: 'full' },
  { path: 'KotDetails/:id', component: KotDetailsComponent, pathMatch: 'full' },
  { path: 'CustomerKotDetails/:waiter_id/:customer_id', component: CustomerKotDetailsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KotRoutingModule { }
