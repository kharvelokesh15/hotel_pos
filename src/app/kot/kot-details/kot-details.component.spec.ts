import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KotDetailsComponent } from './kot-details.component';

describe('KotDetailsComponent', () => {
  let component: KotDetailsComponent;
  let fixture: ComponentFixture<KotDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KotDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KotDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
