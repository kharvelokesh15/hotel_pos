import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,Router } from '@angular/router';

//services
import { HeaderService } from './../../header.service';
import { KOTManageService } from './../../@service/kot';
import { UtilityService } from '../../@service';


@Component({
  selector: 'app-kot-details',
  templateUrl: './kot-details.component.html',
  styleUrls: ['./kot-details.component.css']
})
export class KotDetailsComponent implements OnInit {
  public _entityId;
  public kotDetailsData;
  public kotDetailMenu;
  public kotTableDetails;
  public SelectedCustomer;
  currency;
  constructor(
    // public _route: Route,
    public route: ActivatedRoute,public _router: Router, private headerService: HeaderService,private kot
    : KOTManageService,private utilityService: UtilityService
  ) {  this.currency = this.utilityService.getCurrency(); }

  ngOnInit() {
    this._entityId = this.route.snapshot.paramMap.get('id');
    this.kot.kotDetailsList(this._entityId).subscribe(res => {
      this.kotDetailsData = res;
      this.kotDetailMenu = this.kotDetailsData.menus;
      this.kotTableDetails = this.kotDetailsData.kot_table;
      console.log(this.kotDetailsData);
    });
  }

  addmoremenu(){
    // "/dinein/DineinMenuListCategory"
     this.SelectedCustomer = {"id":this.kotDetailsData.customer_id,"first_name":this.kotDetailsData.customer_name}
     this.utilityService.setSelectedCustomer(this.SelectedCustomer);
     this.utilityService.setTableSelection([this.kotDetailsData.kot_table[0].id+'-'+this.kotDetailsData.kot_floor],[this.kotDetailsData.kot_table[0].table_name]);
     this._router.navigate(['/dinein/DineinMenuListCategory']);
   }


  holdKot(kot_menu_id,kot_id,kot_type,customer_cart){
    
    this.kot.KotPosHold({"kot_menu_id":kot_menu_id,"kot_id":kot_id,"kot_type":kot_type,"customer_cart":customer_cart}).subscribe(res => {
  
      this.kot.kotDetailsList(this._entityId).subscribe(res => {
        this.kotDetailsData = res;
        this.kotDetailMenu = this.kotDetailsData.menus;
        this.kotTableDetails = this.kotDetailsData.kot_table;
      });

    });
  
  }
  
  cancelKot(kot_menu_id,kot_id,kot_type,customer_cart){
    if(confirm("Are you sure to Cancel This Item ?")) {
      this.kot.KotPosCancel({"kot_menu_id":kot_menu_id,"kot_id":kot_id,"kot_type":kot_type,"customer_cart":customer_cart}).subscribe(res => {
    
        this.kot.kotDetailsList(this._entityId).subscribe(res => {
          this.kotDetailsData = res;
          this.kotDetailMenu = this.kotDetailsData.menus;
          this.kotTableDetails = this.kotDetailsData.kot_table;
        });

      });
    } else{
      return false;
    }
  }

}
