import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';

import { KotRoutingModule } from './kot-routing.module';
import { KotListingComponent } from './kot-listing/kot-listing.component';
import { KotCallListingComponent } from './kot-call-listing/kot-call-listing.component';
import { KotDetailsComponent } from './kot-details/kot-details.component';
import { CustomerKotDetailsComponent } from './customer-kot-details/customer-kot-details.component';
import { KotMasterComponent } from './kot-master/kot-master.component';

@NgModule({
  declarations: [KotListingComponent,KotCallListingComponent,KotDetailsComponent,CustomerKotDetailsComponent, KotMasterComponent],
  imports: [
    MyDatePickerModule,
    CommonModule,
    HeaderModule,
    KotRoutingModule
  ],schemas: [NO_ERRORS_SCHEMA],
  exports: [
    KotListingComponent,
    KotCallListingComponent
  ]
})
export class KotModule { }
