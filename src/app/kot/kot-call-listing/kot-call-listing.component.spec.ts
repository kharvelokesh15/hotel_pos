import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KotCallListingComponent } from './kot-call-listing.component';

describe('KotCallListingComponent', () => {
  let component: KotCallListingComponent;
  let fixture: ComponentFixture<KotCallListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KotCallListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KotCallListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
