import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// import {IMyOptions, IMyDate} from 'mydatepicker';
import {IMyOptions, IMyDateModel, IMyDate} from 'mydatepicker';
//services
import { HeaderService } from '../../header.service';
import { UtilityService } from '../../@service';
import { KOTManageService } from '../../@service/kot';

@Component({
  selector: 'app-kot-call-listing',
  templateUrl: './kot-call-listing.component.html',
  styleUrls: ['./kot-call-listing.component.css']
})
export class KotCallListingComponent implements OnInit {
  public KotReceiptListData;
  public CurentStaff;
  KotGroup:any = [];
  isholditemlistCol: any = [];
  public selDate: IMyDate = {year: 0, month: 0, day: 0};
  public KOTDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  KOTSelectedData:any = "";
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };
  kot_type:any="callcenter";
  flag:any =1;
  rowIndex:any =[];
  iskotReceiptlist: boolean = false;
  checkAllkotreceipt: boolean = false;
  currency;

  constructor(public _router: Router, private headerService: HeaderService,private kot
    : KOTManageService,private utilityService: UtilityService) { 
      let d: Date = new Date();
      this.selDate = {year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate()};
      this.currency = this.utilityService.getCurrency();
    }

  ngOnInit() {
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.headerService.setTitle('Call Center List');
   
      this.kot.kotList({"kot_date":this.KOTSelectedData,"kot_type":this.kot_type}).subscribe(res => {
        this.KotReceiptListData = res
          this.KotGroup = this.KotReceiptListData.reduce((r,{customer_id})=>{
            if(!r.some(o=>o.customer_id==customer_id)){
              let test = this.KotReceiptListData.filter(v=>v.customer_id==customer_id);
              r.push({waiter_id:test[0].waiter_id,customer_id,groupItem:this.KotReceiptListData.filter(v=>v.customer_id==customer_id)});
            }
            return r;
          },[]);
          console.log(this.KotGroup.length)
       
      });

  }

  changekotReceiptList(event) {
    if (event.target.name == 'KotReceiptListData') {
      this.iskotReceiptlist = true
    }

    if (this.iskotReceiptlist && this.checkAllkotreceipt) {
      event.target.checked = true
    }
  }
  GetTableid(tables){
    return tables.map(v=>v.table_name).join(', ');
  }

  GetStatusid(tables){
    if(tables.some(person => person.status == "Cancelled") || tables.some(person => person.status == "Hold") ){
     return true;
    } else{
      return false;
    }
  } 
  GetKotType(tables, i){
    if(tables.some(person => person.kot_type == "delivery")){
      console.log('delivery true' + i, tables);
     return true;
    } else{
      console.log('delivery false' + i);
      return false;
    }
  } 

  GetTotal(collection){
    let sum: number = 0;
    collection.forEach(a => sum += parseFloat(a.grand_total));
    return sum.toFixed(2);

  } 
  GetOverQty(collection){
    let sum: number = 0;
    collection.forEach(a => sum += parseFloat(a.over_all_qty));
    return sum;

  } 
  
  showRow(index){
    if(this.rowIndex[index] == 1){
      this.rowIndex[index] = 0;
    } else{
      this.rowIndex[index] = 1;
    }
    
  }
  

  onDateChanged(event: IMyDateModel) {
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.KOTSelectedData = event.formatted;

    this.kot.kotList({"kot_date":event.formatted,"kot_type":this.kot_type}).subscribe(res => {
        this.KotReceiptListData = res
        if(this.KotReceiptListData != undefined){
          this.KotGroup = this.KotReceiptListData.reduce((r,{customer_id})=>{
            if(!r.some(o=>o.customer_id==customer_id)){
              r.push({waiter_id:this.CurentStaff.id,customer_id,groupItem:this.KotReceiptListData.filter(v=>v.customer_id==customer_id)});
            }
            return r;
          },[]);
        } else{
          alert('No KOT Available')
          return this.KotGroup = [];
        }
      });
    
  }

  statusChange(kot_type){
  
   this.kot_type = kot_type;
    this.kot.kotList({"kot_date":this.KOTSelectedData,"kot_type":kot_type}).subscribe(res => {
      this.KotReceiptListData = res
      if(this.KotReceiptListData != undefined){
        this.KotGroup = this.KotReceiptListData.reduce((r,{customer_id})=>{
          if(!r.some(o=>o.customer_id==customer_id)){
            r.push({waiter_id:this.CurentStaff.id,customer_id,groupItem:this.KotReceiptListData.filter(v=>v.customer_id==customer_id)});
          }
          return r;
        },[]);
      } else{
        alert('No KOT Available')
        return this.KotGroup = [];
      }
    });
    
  }

  allkotReceiptList(event) {
    const checked = event.target.checked;
    this.KotReceiptListData.forEach(item => item.selected = checked);
  }

  cancelKot(kot_id){
    this.isholditemlistCol.push(kot_id);
    console.log(this.isholditemlistCol);
    this.kot.KotPosMultiUpdate({"kot_id":this.isholditemlistCol,"type":'cancel'}).subscribe(res => {
  
      // this.kot.kotList().subscribe(res => {
      //   this.KotReceiptListData = res
      //   this.KotGroup = this.KotReceiptListData.reduce((r,{customer_id})=>{
      //     if(!r.some(o=>o.customer_id==customer_id)){
      //       r.push({waiter_id:this.CurentStaff.id,customer_id,groupItem:this.KotReceiptListData.filter(v=>v.customer_id==customer_id)});
      //     }
      //     return r;
      //   },[]);
      // });

      this._router.navigate(['/kot/KotListing']);

    });
  }


}
