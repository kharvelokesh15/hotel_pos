import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageScreenComponent } from './message-screen/message-screen.component';


const routes: Routes = [
  {path:'messageScreen',component:MessageScreenComponent,pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRoutingModule { }
