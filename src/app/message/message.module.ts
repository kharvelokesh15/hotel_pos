import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MessageRoutingModule } from './message-routing.module';
import { MessageScreenComponent } from './message-screen/message-screen.component';


@NgModule({
  declarations: [MessageScreenComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MessageRoutingModule
  ]
})
export class MessageModule { }
