import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-message-screen',
  templateUrl: './message-screen.component.html',
  styleUrls: ['./message-screen.component.css']
})
export class MessageScreenComponent implements OnInit {
  public MessageReceiptListData;
  ismessageReceiptlist: boolean = false;
  checkAllmessagereceipt: boolean = false;
  ordermenus: string[] = ['Write Message', 'Inbox', 'Sent', 'Trash'];
  selectedorder = this.ordermenus[0];

  constructor(public _router: Router, private headerService: HeaderService) { }




  ngOnInit() {
    this.headerService.setTitle('Message Screen ');

    this.MessageReceiptListData = [
      {
        sender: 'Sender 1', id: 1, selected: false, subject: 'Subject 1 ', date: '29 Jul 2019',
      },
      {
        sender: 'Sender 1', id: 2, selected: false, subject: 'Subject 1 ', date: '29 Jul 2019',
      },
      {
        sender: 'Sender 1', id: 3, selected: false, subject: 'Subject 1 ', date: '29 Jul 2019',
      },
      {
        sender: 'Sender 1', id: 4, selected: false, subject: 'Subject 1 ', date: '29 Jul 2019',
      },
      {
        sender: 'Sender 1', id: 3, selected: false, subject: 'Subject 1 ', date: '29 Jul 2019',
      },



    ]
  }

  changemessageReceiptList(event) {
    if (event.target.name == 'MessageReceiptListData') {
      this.ismessageReceiptlist = true
    }

    if (this.ismessageReceiptlist && this.checkAllmessagereceipt) {
      event.target.checked = true
    }
  }

  allmessageReceiptList(event) {
    const checked = event.target.checked;
    this.MessageReceiptListData.forEach(item => item.selected = checked);
  }





}


