import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
//components
import { HeaderComponent } from './header.component';
//services
import { HeaderService } from '../header.service';

@NgModule({
  declarations: [HeaderComponent],
  providers: [HeaderService],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
