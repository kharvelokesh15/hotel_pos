import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { HeaderService } from './../header.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']

})


export class HeaderComponent implements OnInit {
  title = '';
  restarunt = '';
  branch = '';
  callcenterbranch = '';
  public show: boolean = false;
  previousUrl: string;

  emailShowOrHide: boolean;
  public now: Date = new Date();
  constructor(public _router: Router, private headerService: HeaderService, private location: Location) {
    console.log(_router);
    // _router.events
    // .pipe(filter(event => event instanceof NavigationEnd))
    // .subscribe((event: NavigationEnd) => {
    //   console.log('prev:', event);
    //   this.previousUrl = event.url;
    // });
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }

  public localStorageItem(id: string): string {
    return JSON.parse(localStorage.getItem(id));
  }

  ngOnInit() {
    this.headerService.title.subscribe(title => {
      this.title = title;
    });
    this.headerService.restarunt.subscribe(restarunt => {
      this.restarunt = restarunt;
    });
    this.headerService.branch.subscribe(branch => {
      this.branch = branch;
    });
    this.headerService.callcenterbranch.subscribe(callcenterbranch => {
      this.callcenterbranch = callcenterbranch;
    });

    //login
    this.emailShowOrHide = (this._router.url == "/auth/clockin" || this._router.url == "/auth/clockout" || this._router.url == "/auth/login")
      ? false : true;
  }
  userButton() {
    this.show = !this.show;
  }

  goBack() {

    // this.location.back();
    this._router.navigate(['/']);
  }

}
