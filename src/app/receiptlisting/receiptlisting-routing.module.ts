import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptlistingComponent } from './receiptlisting/receiptlisting.component';


const routes: Routes = [
  {path:'receiptlisting',component:ReceiptlistingComponent,pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptlistingRoutingModule { }
