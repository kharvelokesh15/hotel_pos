import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';

import { ReceiptlistingRoutingModule } from './receiptlisting-routing.module';
import { ReceiptlistingComponent } from './receiptlisting/receiptlisting.component';


@NgModule({
  declarations: [ReceiptlistingComponent],
  imports: [
    CommonModule,
    ReceiptlistingRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule
  ]
})
export class ReceiptlistingModule { }
