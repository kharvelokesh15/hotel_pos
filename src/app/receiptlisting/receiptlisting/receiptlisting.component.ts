import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-receiptlisting',
  templateUrl: './receiptlisting.component.html',
  styleUrls: ['./receiptlisting.component.css']
})
export class ReceiptlistingComponent implements OnInit {
  public mergereceiptdata;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public KOTDate; 
  isTrade: boolean = false;
  checkAllTrades: boolean = false;
 

  constructor(public _router: Router, private headerService: HeaderService) { }
 

  
  ngOnInit() {
    this.headerService.setTitle('RECEIPT LISTING');

    this.mergereceiptdata = [
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      },
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1',mergeShift:'Morning',mergeStatus:'Paid', mergeAmount: '$1300' 
      }
      
    ]
  }
  changeMergeByCategory(event) {
    if (event.target.name == 'mergereceiptdata') {
      this.isTrade = true
    }

    if (this.isTrade && this.checkAllTrades) {
      event.target.checked = true
    }
  }

  allmergeReceipt(event) {
    const checked = event.target.checked;
    this.mergereceiptdata.forEach(item => item.selected = checked);
  }


}
