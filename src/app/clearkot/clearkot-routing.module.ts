import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClearkotComponent } from './clearkot/clearkot.component';


const routes: Routes = [
  { path: 'clearkots', component: ClearkotComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClearkotRoutingModule { }
