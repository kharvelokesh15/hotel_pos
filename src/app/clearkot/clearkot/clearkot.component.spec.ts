import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearkotComponent } from './clearkot.component';

describe('ClearkotComponent', () => {
  let component: ClearkotComponent;
  let fixture: ComponentFixture<ClearkotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearkotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearkotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
