import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-clearkot',
  templateUrl: './clearkot.component.html',
  styleUrls: ['./clearkot.component.css']
})
export class ClearkotComponent implements OnInit {

  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Clear KOT ');
  }

}
