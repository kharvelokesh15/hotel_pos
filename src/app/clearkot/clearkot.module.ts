import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'

import { ClearkotRoutingModule } from './clearkot-routing.module';
import { ClearkotComponent } from './clearkot/clearkot.component';


@NgModule({
  declarations: [ClearkotComponent],
  imports: [
    CommonModule,
    ClearkotRoutingModule,
    HeaderModule
  ]
})
export class ClearkotModule { }
