import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker'; 
import { ModalModule } from 'ngx-bootstrap/modal';

import { ReportRoutingModule } from './report-routing.module';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { SalesReportComponent as oldSalesReportComponent } from './sales-report-old/sales-report.component';
import {MatPaginatorModule, MatTableModule} from '@angular/material';

@NgModule({
  declarations: [SalesReportComponent, oldSalesReportComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    ModalModule.forRoot(),
    AmazingTimePickerModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class ReportModule { }
