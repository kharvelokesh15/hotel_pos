import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material';
import { IMyDate, IMyDateModel, IMyOptions } from 'mydatepicker';
import { Subject, Subscription } from 'rxjs';
import { MenuManageService } from 'src/app/@service/menu';
import { ReportManageService } from 'src/app/@service/report';
import { HeaderService } from 'src/app/header.service';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.css']
})
export class SalesReportComponent implements OnInit {
  ordermenus: any[] = [
    {
      name: 'Daily Statement Report',
      url: `daily-statement-report?`,
      columns: [{
        header: 'Ref No',
        accessor: 'RefNo'
      }, {
        header: 'Date',
        accessor: 'invoice_date'
      }, {
        header: 'Type',
        accessor: 'sales_type',
      }, {
        header: 'Tr. Type',
        accessor: 'payent_type'
      }, {
        header: 'Description',
        accessor: 'first_name'
      }, {
        header: 'Gross Amt',
        accessor: 'sub_total',
        showTotal: true
      }, {
        header: 'Discount',
        accessor: 'discount_amount',
        showTotal: true
      }, {
        header: 'Amount',
        accessor: 'grand_total',
        showTotal: true
      }],
      sortBy: 'RefNo',
      filters: [{
        label: 'Type',
        accessor: 'sales_type',
        type: 'options',
        values: [{
          label: 'All',
          value: ''
        }, {
          label: 'Take away',
          value: 'takeaway'
        }, {
          label: 'Dine In',
          value: 'dinein'
        }]
      }, {
        label: 'Tr. Type',
        accessor: 'payent_type',
        type: 'options',
        values: [{
          label: 'All',
          value: ''
        }, {
          label: 'Cash',
          value: 'cash'
        }, {
          label: 'Card',
          value: 'card'
        }]
      }]
    }, {
      name: 'Category Wise Sales Report',
      url: `categeory-wise-sales-report?`,
      columns: [{
        header: 'Category Name',
        accessor: 'Categeory_Name'
      }, {
        header: 'Quantity',
        accessor: 'Quantity'
      }, {
        header: 'Amount',
        accessor: 'Grand_total',
        showTotal: true
      }],
      sortBy: 'Categeory_Name',
      filters: [{
        label: 'Category Name',
        accessor: 'Categeory_Name',
        type: 'text',
        values: []
      }]
    }, {
      name: 'Item Wise Sales Report',
      url: `item-wise-sales-report?`,
      columns: [{
        header: 'Item Name',
        accessor: 'Item_Name'
      }, {
        header: 'Quantity',
        accessor: 'Quantity',
        showTotal: true
      }, {
        header: 'Uom\'s',
        accessor: 'Uom',
      }, {
        header: 'Sales Price',
        accessor: 'Sales_Price',
        showTotal: true
      }, {
        header: 'Amount',
        accessor: 'amount',
        showTotal: true
      }],
      sortBy: 'Item_Name',
      filters: [{
        label: 'Item Name',
        accessor: 'Item_Name',
        type: 'text',
        values: []
      }]
    }, {
      name: 'Sales Summarized Report',
      url: `sales-summarized-report?`,
      columns: [{
        header: 'Ref No',
        accessor: 'RefNo'
      }, {
        header: 'Inv Date',
        accessor: 'invoice_date'
      }, {
        header: 'Inv No',
        accessor: 'Inv_No'
      }, {
        header: 'Customer Name',
        accessor: 'Customer_name'
      }, {
        header: 'Order Type',
        accessor: 'sales_type'
      }, {
        header: 'Payment Type',
        accessor: 'payent_type'
      }, {
        header: 'User',
        accessor: 'user_name'
      }, {
        header: 'Gross Amt',
        accessor: 'sub_total',
        showTotal: true
      }, {
        header: 'Discount Amt',
        accessor: 'discount_amount',
        showTotal: true
      }, {
        header: 'Del Charges',
        accessor: 'shipping_total',
        showTotal: true
      }, {
        header: 'Tax Amt',
        accessor: 'tax_total',
        showTotal: true
      }, {
        header: 'Invoice Paid Amt',
        accessor: 'invoice_paid_amount',
        showTotal: true
      }, {
        header: 'Balance to Cust',
        accessor: 'balance_to_customer',
        showTotal: true
      }, {
        header: 'Balance to Res',
        accessor: 'balance_to_res',
        showTotal: true
      }, {
        header: 'Amount',
        accessor: 'grand_total',
        showTotal: true
      }],
      sortBy: 'RefNo',
      filters: [{
        label: 'Type',
        accessor: 'sales_type',
        type: 'options',
        values: [{
          label: 'All',
          value: ''
        }, {
          label: 'Take away',
          value: 'takeaway'
        }, {
          label: 'Dine In',
          value: 'dinein'
        }]
      }, {
        label: 'Tr. Type',
        accessor: 'payent_type',
        type: 'options',
        values: [{
          label: 'All',
          value: ''
        }, {
          label: 'Cash',
          value: 'cash'
        }, {
          label: 'Card',
          value: 'card'
        }]
      }, {
        label: 'User',
        accessor: 'user_name',
        type: 'text',
        values: []
      }]
    }, {
      name: 'Org Wise Sales Report',
      url: `org-wise-sales-report?`,
      columns: [{
        header: 'Branch',
        accessor: 'branch_name_ar'
      }, {
        header: 'Branch-en',
        accessor: 'branch_name_en'
      }, {
        header: 'Date',
        accessor: 'invoice_date'
      }, {
        header: 'Invoice',
        accessor: 'invoices'
      }, {
        header: 'Type',
        accessor: 'sales_type',
      }, {
        header: 'Gross Amt',
        accessor: 'sub_total',
        showTotal: true
      }, {
        header: 'Discount',
        accessor: 'discount_amount',
        showTotal: true
      }, {
        header: 'Del Charges',
        accessor: 'shipping_total',
        showTotal: true
      }, {
        header: 'Tax Amt',
        accessor: 'tax_total',
        showTotal: true
      }, {
        header: 'Amount',
        accessor: 'grand_total',
        showTotal: true
      }],
      sortBy: 'branch_name_ar',
      filters: [{
        label: 'Branch Name (ar)',
        accessor: 'branch_name_ar',
        type: 'text',
        values: []
      },{
        label: 'Branch Name (en)',
        accessor: 'branch_name_en',
        type: 'text',
        values: []
      }, {
        label: 'Type',
        accessor: 'sales_type',
        type: 'options',
        values: [{
          label: 'All',
          value: ''
        }, {
          label: 'Take away',
          value: 'takeaway'
        }, {
          label: 'Dine In',
          value: 'dinein'
        }]
      }]
    }
  ];
  // ordermenus: string[] = ['SALES REPORT','Daily Statement Details' ,'Category Sales','Group Sales','Cashier Sales Report', 'Serving Period Summary Report', 'Menu Item Sales Report', 'Void Transaction Listing', 'Time Wise Item Sales Report', 'Daily Sales Summary', 'Sales Report with A/R_A/P', 'Flash Report Terminal Wise', 'XZ Report (Innscor)', 'Payment Type Listing Report by Receipt Wise'];
  selectedorder = this.ordermenus[0];
  comlumnsToDisplay = this.selectedorder.columns.map(col => col.header);
  selDate: IMyDate = { year: 0, month: 0, day: 0 };
  myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };
  fromDate: any = { date: { year: (new Date()).getMonth() ? (new Date()).getFullYear() : (new Date()).getFullYear() - 1, month: (new Date()).getMonth() || 12, day: (new Date()).getDate() } };
  toDate: any = { date: { year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate() } };

  reportData;
  pageEvent: PageEvent = {
    length: 0,
    pageSize: 50,
    pageIndex: 0
  };
  textFilterSubject: Subject<string> = new Subject<string>();
  textFilterSubscription: Subscription;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private headerService: HeaderService,
    private reportService: ReportManageService,
    private menuManageService: MenuManageService) { }

  ngOnInit() {
    this.headerService.setTitle('Report');
    /* this.textFilterSubject.debounceTime(1000).distinctUntilChanged((valA, valB) => {
      console.log(valA, valB);
      return true;
    }).subscribe(res => console.log(res)) */
    this.fetchReport();
    /* this.menuManageService.menuCategory().subscribe(res => {
      // this.menuCategoryList = res;
      console.log('category list', res);
    })
    this.menuManageService.menuGroup().subscribe(res => {
      // this.menuCategoryList = res;
      console.log('group list', res);
    })
    this.menuManageService.menuList().subscribe(res => {
      // this.menuCategoryList = res;
      console.log('menu list', res);
    }) */

  }

  fetchReport(additionalFilters = '') {
    console.log(additionalFilters)
    if (this.fromDate && this.toDate) {
      this.reportService.getReport(`${this.selectedorder.url}&fromdate=${this.fromDate.date.year}-${this.fromDate.date.month}-${this.fromDate.date.day}&todate=${this.toDate.date.year}-${this.toDate.date.month}-${this.toDate.date.day}&lang=1&&sort=${this.selectedorder.sortBy}&order=ASC&perPage=${this.pageEvent.pageSize}&page=${this.pageEvent.pageIndex + 1}&${additionalFilters}`)
        .subscribe(res => {
          this.reportData = res;
        }, err => console.log(err))
    }
  }

  filterChange(filters = {}) {
    console.log(filters)
    const additionalFilter = Object.keys(filters).map(key => `${key}=${filters[key]}`).join("&")
    this.pageEvent = {
      length: 0,
      pageSize: 50,
      pageIndex: 0
    };
    setTimeout(() => { this.fetchReport(additionalFilter) }, 0)
    // this.sales_report_data.fromdate = event.formatted;
    // this.load_reports();
  }

  changePage() {
    setTimeout(() => { this.fetchReport() }, 0)
  }

  changeReport() {
    this.fromDate = { date: { year: (new Date()).getFullYear(), month: (new Date()).getMonth(), day: (new Date()).getDate() } };
    this.toDate = { date: { year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate() } };

    this.pageEvent = {
      length: 0,
      pageSize: 50,
      pageIndex: 0
    };
    this.comlumnsToDisplay = this.selectedorder.columns.map(col => col.header);
    this.fetchReport();
  }

  filterTextChange(value) {
    let newValue = value;
    setTimeout(function (val) {
      let oldValue = val;
      console.log(newValue, oldValue)
    }, 1000, newValue);
  }

  printPageArea(areaID) {
    var printContent = document.getElementsByClassName(areaID);
    var WinPrint = window.open('', '', 'width=900,height=650');
    WinPrint.document.write(printContent[0].innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
  }
}
