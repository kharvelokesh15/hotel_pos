import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions, IMyDateModel, IMyDate} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';
import { ReportManageService } from './../../@service/report';
import { StaffManageService } from './../../@service/staff';
import { MenuManageService } from './../../@service/menu';
import { TableManageService } from './../../@service/table';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.css']
})
export class SalesReportComponent implements OnInit {

  ordermenus: string[] = ['SALES REPORT','Daily Statement Details' ,'Category Sales Report', 'Menu Item Sales Report', 'Section Wise Sales Report','Group Sales Report','Void Transaction Listing', 'Time Wise Item Sales Report', 'Daily Sales Summary', 'Sales Report with A/R_A/P', 'Flash Report Terminal Wise', 'XZ Report (Innscor)', 'Payment Type Listing Report by Receipt Wise'];
  // ordermenus: string[] = ['SALES REPORT','Daily Statement Details' ,'Category Sales','Group Sales','Cashier Sales Report', 'Serving Period Summary Report', 'Menu Item Sales Report', 'Void Transaction Listing', 'Time Wise Item Sales Report', 'Daily Sales Summary', 'Sales Report with A/R_A/P', 'Flash Report Terminal Wise', 'XZ Report (Innscor)', 'Payment Type Listing Report by Receipt Wise'];
  selectedorder = this.ordermenus[0];

  // Common
  public selDate: IMyDate = {year: 0, month: 0, day: 0};
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };
  public staffList = [];
  public menuCategoryList =[];
  public menuGroupList =[];
  public menuList = [];
  public floorList=[];
  tableList:any=[];
  
  public CSR_FromDate; 
  public CSR_ToDate; 
  public SPSR_FromData;
  public SPSR_ToData;
  public MIST_FromDate;
  public MIST_ToDate;
  public VTL_FromDate;
  public VTL_ToDate;
  public VTL_VoidDate;
  public VTL_VoidToDate;
  public TWSI_FromDate;
  public TWSI_ToDate;
  public DSS_FromDate;
  public DSS_ToDate;
  public SRW_ToDate;
  public SRW_FromDate;
  public FRTW_FromDate;
  public FRTW_ToDate;
  public XZR_FromDate;
  public XZR_ToDate;
  public Pay_FromDate;
  public Pay_ToDate;

  
  // Sales Report
  public sales_report_data = {
    orgid: '1',
    branchid: '1',
    waiterid: '',
    type:"SALES REPORT",
    fromdate:"",
    todate:"",
    salestype:"",
    username:"",
    menugroup:"",
    submenugroup:"" 
  };
  public SR_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public SR_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  SR_SalesType = '';
  SR_WaiterId = '';
  public reportSalesList = [];

  // Daily Statement Detail Report
  public daily_statement_detail_data = {
    orgid: '1',
    branchid: '1',
    lang:'1',
    type:"Daily Statement Details",
    fromdate:"",
    todate:"",
  
  };
  public DSD_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public DSD_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };

  public reportDailyStatementDetail = [];


  // Category Sales Report
  public category_sales_data = {
    orgid: '1',
    branchid: '1',
    lang:'1',
    type:"Category Sales Report",
    fromdate:"",
    todate:"",
   
  };
  public CS_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public CS_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };

  public reportCategorySales = [];

   // Group Sales Report
   public group_sales_data = {
    orgid: '1',
    branchid: '1',
    waiterid: '',
    lang:'1',
    type:"Group Sales Report",
    fromdate:"",
    todate:"",
    salestype:"",
    menu_cat:"",
    menu_group:"",
    menu:"",
    table:"",
    floor:""
  };
  public GS_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public GS_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  GS_SalesType = '';
  GS_WaiterId = '-1';
  GS_CategoryId = '-1';
  GS_GroupId = '-1';
  GS_MenuId = '0';
  GS_FloorId = '-1';
  GS_TableId = '-1';
  public reportGroupSales = [];

  // Menu Item Sales Report
  public menu_item_sales_data = {
    orgid: '1',
    branchid: '1',
    lang:'1',
    type:"Menu Item Sales Report",
    fromdate:"",
    todate:""
    
  };
  public MIS_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public MIS_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };

  public reportMenuItemSales = [];

  // Section wise  Sales Report
  public floor_table_sales_data = {
    orgid: '1',
    branchid: '1',
    lang:'1',
    type:"Section Wise Sales Report",
    fromdate:"",
    todate:"",
    floor:"",
    report_type:""
  };
  public FTS_FromDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  public FTS_ToDate: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };
  
  FTS_FloorId = '0';
  FTS_ReportType = '0'; 

  public reportFloorTableSales = [];

  constructor(public _router: Router, 
    private headerService: HeaderService,
    private staff: StaffManageService, 
    public report: ReportManageService,
    public menu:MenuManageService,
    public table:TableManageService
    ) { }
  
  
  public counter: number = 0;

  ngOnInit() {
    this.headerService.setTitle('Report');

    // Common
    let d: Date = new Date(); 
    this.selDate = {year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate()};
    this.staff.staffList().subscribe(res => {
      this.staffList = res;
      console.log('staff list', this.staffList);
    })

    this.menu.menuCategory().subscribe(res => {
      this.menuCategoryList = res;
      console.log('category list', this.menuCategoryList);
    })

    this.menu.menuGroup().subscribe(res => {
      this.menuGroupList = res;
      console.log('group list', this.menuGroupList);
    })

    this.menu.menuList().subscribe(res => {
      this.menuList = res;
      console.log('menu list', this.menuList);
    })

    this.table.floorList().subscribe(res => {
      this.floorList = res;
      console.log('floor list', this.floorList);
    })

    // this.table.tableList({floor_id:this.DSD_FloorId}).subscribe(res => {
    //   this.tableList = res;
    //   console.log('table list', this.tableList);
    // })
    
    // Sales Report
    this.sales_report_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.sales_report_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    // Daily Statement Report
    this.daily_statement_detail_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.daily_statement_detail_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    // Category Sales Report
    this.category_sales_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.category_sales_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    // Group Sales Report
    this.group_sales_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.group_sales_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    // Menu Item Sales Report
    this.menu_item_sales_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.menu_item_sales_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    //Section wise Sales Report
    this.floor_table_sales_data.fromdate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;
    this.floor_table_sales_data.todate = this.selDate.year+'-'+this.selDate.month+'-'+this.selDate.day;

    this.load_reports();
    this.DSD_load_reports();
    this.CS_load_reports();
    this.GS_load_reports();
    this.MIS_load_reports();
    this.FTS_load_reports();

  }

  changeReport(order){
    
      switch (order) {
          case 'Daily Statement Details':
            this.DSD_load_reports();
          break;
          case 'Category Sales Report':
            this.CS_load_reports();
          break;
          case 'Group Sales Reports':
            this.GS_load_reports();
          break;
          case 'Menu Item Sales Report':
            this.MIS_load_reports();
          break;
          case 'Section Wise Sales Report':
            this.FTS_load_reports();
          break;
        default:
          break;
      }
  }
  sales = [
    { label: ' Show Receipt Summary', selected: true },
    { label: ' Show Payment With Other Currency', selected: false },
    { label: 'Show Discount Summary', selected: false },
    { label: 'Show Category Summary', selected: true },
    { label: ' Show Printer by Information', selected: true },
    { label: ' Amount Before Discount', selected: false },
    { label: 'Show Order Type Summary', selected: true },
    { label: 'Show Gross Amount', selected: false },
    { label: 'Include Advance Order Payment', selected: false }
  ];



  salesitems = [
    { label: ' Show  item description', selected: true },
    { label: ' Ignore Modifier Without Cost', selected: false },
    { label: 'Sort By Category ', selected: true },
 
  ];


  voids = [
    { label: 'Show Item Modified Name', selected: true }
  ];
 
  salesFlash = [
    { label: 'Show Receipt Summary', selected: true },
    { label: 'Show Discount Summary', selected: false },
  ];
  salesFlashes = [
    { label: 'Show Printed By Information', selected: false },
    { label: 'Show Category Summary', selected: true },
    { label: 'Amount Before Discount', selected: false },
  ];

  timeWise = [
    { label: 'Show Time in Range of {n} Minute', selected: true },
    { label: 'Consolidate Item on Date Range', selected: false },
    { label: 'Ignore Modifier Without Cost', selected: true },
    { label: 'Consolidate Item on Date Range', selected: false },
    { label: 'Ignore Modifier Without Cost', selected: true }
  ];


  dailySales = [
    { label: 'Rounding Amount', selected: true },
    { label: 'Ignore Zero Amount', selected: false },
    { label: 'Show Gross Amount', selected: true },
    { label: 'Show Item Modified Name', selected: false },
    { label: 'Ignore Modifier Without Cost', selected: true }
  ];

   
  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;
  }

  // sales report on change functions start
  onSRFromDateDateSelected(event: IMyDateModel){
    this.sales_report_data.fromdate = event.formatted;
    this.load_reports();
  }
  onSRToDateDateSelected(event: IMyDateModel){
    this.sales_report_data.todate = event.formatted;
    this.load_reports();
  }
  // sales report on change functions start

  load_reports(){
    console.log('selected order', this.selectedorder);
    // slaes report
    if(this.selectedorder == 'SALES REPORT'){
      console.log('sales_report_data', this.sales_report_data);
      this.sales_report_data.waiterid = this.SR_WaiterId;
      this.sales_report_data.salestype = this.SR_SalesType;
      this.report.getReports(this.sales_report_data).subscribe(res => {   
        this.reportSalesList = res;     
        console.log('sales reports', res);
      });
    }
  }



  //***Daily Statement Details */
  onDSDFromDateDateSelected(event: IMyDateModel){
    this.daily_statement_detail_data.fromdate = event.formatted;
    this.DSD_load_reports();
  }
  onDSDToDateDateSelected(event: IMyDateModel){
    this.daily_statement_detail_data.todate = event.formatted;
    this.DSD_load_reports();
  }

  DSD_load_reports(){
    
    // this.table.tableList({floor_id:this.DSD_FloorId}).subscribe(res => {
    //   this.tableList = res;
    //   console.log('table list', this.tableList);
    // });
    
    if(this.selectedorder == 'Daily Statement Details'){

      this.report.getReports(this.daily_statement_detail_data).subscribe(res => {   
        this.reportDailyStatementDetail = res;     
        console.log('daily sales reports', res);
      });
    }
  }

  //***Category Sales Report*/
  onCSFromDateDateSelected(event: IMyDateModel){
    this.category_sales_data.fromdate = event.formatted;
    this.CS_load_reports();
  }
  onCSToDateDateSelected(event: IMyDateModel){
    this.category_sales_data.todate = event.formatted;
    this.CS_load_reports();
  }

  CS_load_reports(){

    if(this.selectedorder == 'Category Sales Report'){
      // this.category_sales_data.waiterid = this.CS_WaiterId;
      // this.category_sales_data.salestype = this.CS_SalesType;
      // this.category_sales_data.menu_cat = this.CS_CategoryId;
      // this.category_sales_data.menu_group = this.CS_GroupId;
      // this.category_sales_data.menu = this.CS_MenuId;

      this.report.getReports(this.category_sales_data).subscribe(res => {   
        this.reportCategorySales = res;     
        console.log('Category Sales reports', res);
      });
    }
  }

  //***Group Sales Report*/
  onGSFromDateDateSelected(event: IMyDateModel){
    this.group_sales_data.fromdate = event.formatted;
    this.GS_load_reports();
  }
  onGSToDateDateSelected(event: IMyDateModel){
    this.group_sales_data.todate = event.formatted;
    this.GS_load_reports();
  }

  GS_load_reports(){

    if(this.selectedorder == 'Group Sales Report'){
      this.group_sales_data.waiterid = this.GS_WaiterId;
      this.group_sales_data.salestype = this.GS_SalesType;
      this.group_sales_data.menu_cat = this.GS_CategoryId;
      this.group_sales_data.menu_group = this.GS_GroupId;
      this.group_sales_data.menu = this.GS_MenuId;

      this.report.getReports(this.group_sales_data).subscribe(res => {   
        this.reportGroupSales = res;     
        console.log('Gategory Sales reports', res);
      });
    }
  }

  //***Menu Item Sales Report*/
  onMISFromDateDateSelected(event: IMyDateModel){
    this.menu_item_sales_data.fromdate = event.formatted;
    this.MIS_load_reports();
  }
  onMISToDateDateSelected(event: IMyDateModel){
    this.menu_item_sales_data.todate = event.formatted;
    this.MIS_load_reports();
  }

  MIS_load_reports(){

    if(this.selectedorder == 'Menu Item Sales Report'){

      this.report.getReports(this.menu_item_sales_data).subscribe(res => {   
        this.reportMenuItemSales = res;     
        console.log('Menu Item Sales reports', res);
      });
    }
  }

  
  //***Section Wise Sales Report*/
  onFTSFromDateDateSelected(event: IMyDateModel){
    this.floor_table_sales_data.fromdate = event.formatted;
    this.FTS_load_reports();
  }
  onFTSToDateDateSelected(event: IMyDateModel){
    this.floor_table_sales_data.todate = event.formatted;
    this.FTS_load_reports();
  }

  FTS_load_reports(){

    // this.table.tableList({floor_id:this.FTS_FloorId}).subscribe(res => {
    //   if(res.message == "No data found"){
    //     // this.tableList = res;
    //   } else{
    //     this.tableList = res;
    //   }
      
      
    //   console.log('table list', this.tableList);
    // });

    if(this.selectedorder == 'Section Wise Sales Report'){
     
      this.floor_table_sales_data.floor = this.FTS_FloorId;
      this.floor_table_sales_data.report_type = this.FTS_ReportType;
      
      // alert(this.FTS_ReportType);
      this.report.getReports(this.floor_table_sales_data).subscribe(res => {   
        this.reportFloorTableSales = res;     
        console.log('Floor Table Sales reports', res);
      });
    }
  }

  printPageArea(areaID){
    var printContent = document.getElementsByClassName(areaID);
    var WinPrint = window.open('', '', 'width=900,height=650');
    WinPrint.document.write(printContent[0].innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}
  
}


