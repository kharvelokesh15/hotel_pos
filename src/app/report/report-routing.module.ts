import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { SalesReportComponent as oldSalesReportComponent } from './sales-report-old/sales-report.component';



const routes: Routes = [
  { path: 'SalesReports', component: SalesReportComponent, pathMatch: 'full' },
  { path: 'old-report', component: oldSalesReportComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
