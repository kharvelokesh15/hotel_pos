import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { CheckinRoutingModule } from './checkin-routing.module';
import { CheckinlistComponent } from './checkinlist/checkinlist.component';


@NgModule({
  declarations: [CheckinlistComponent],
  imports: [
    CommonModule,
    HeaderModule,
    CheckinRoutingModule
  ]
})
export class CheckinModule { }
