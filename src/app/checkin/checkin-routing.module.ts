import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckinlistComponent } from './checkinlist/checkinlist.component'


const routes: Routes = [
  {
    path: 'checklist', component: CheckinlistComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckinRoutingModule { }
