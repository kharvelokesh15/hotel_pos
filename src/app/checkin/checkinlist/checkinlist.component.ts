import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service'
@Component({
  selector: 'app-checkinlist',
  templateUrl: './checkinlist.component.html',
  styleUrls: ['./checkinlist.component.css']
})
export class CheckinlistComponent implements OnInit {

  public newtripData;
  public tripdata;
  public tripvaluedata;
  isnewtriplist: boolean = false;
  checkAllnewtriplist: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService) { }
  ngOnInit() {
    this.headerService.setTitle('CHECK IN');
    this.tripdata = [
      {
        triptitle: 'Selected'
      },
      {
        triptitle: 'Assigned'
      },
      {
        triptitle: 'Todays'
      }
    ]

    this.tripvaluedata = [
      {
        tripvalue: '1'
      },
      {
        tripvalue: '1'
      }
    ]
    this.newtripData = [
      {
        newtripreceipt: '122', id: 1, selected: true, newtripdriver: 'David', newtripguestname: 'Steve Smith', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: '25 mins', newtripordervalue: '$1250'
      },
      {
        newtripreceipt: '123', id: 1, selected: true, newtripdriver: 'Smith', newtripguestname: 'Albert Jeo', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: '35 mins', newtripordervalue: '$1250'
      },

      {
        newtripreceipt: '124', id: 1, selected: true, newtripdriver: 'Richards', newtripguestname: 'Johny Sin', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: '45 mins', newtripordervalue: '$1250'
      },
      {
        newtripreceipt: '125', id: 1, selected: true, newtripdriver: 'Bradman', newtripguestname: 'Daniel Rediff', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: '55 mins', newtripordervalue: '$1250'
      },
      {
        newtripreceipt: '126', id: 1, selected: true, newtripdriver: 'Midwinter', newtripguestname: 'Robert Dany', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: ' 1h 10 mins', newtripordervalue: '$1250'
      },
      {
        newtripreceipt: '127', id: 1, selected: true, newtripdriver: 'James', newtripguestname: 'Warner Sha', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: ' 1h 20 mins', newtripordervalue: '$1250'
      },

      {
        newtripreceipt: '128', id: 1, selected: true, newtripdriver: 'Nannes', newtripguestname: 'Delrio white', newtripaddress: '25 North wale st, Hollow, US', newtriptime: '8.30 pm', newtripzone: '', newtripdriverwentby: '8.00 pm', newtriptotalordertime: ' 1h 30 mins', newtripordervalue: '$1250'
      }


    ]
  }

  changenewtriplist(event) {
    if (event.target.name == 'newtripData') {
      this.isnewtriplist = true
    }

    if (this.isnewtriplist && this.checkAllnewtriplist) {
      event.target.checked = true
    }
  }

  allnewtriplist(event) {
    const checked = event.target.checked;
    this.newtripData.forEach(item => item.selected = checked);
  }

}
