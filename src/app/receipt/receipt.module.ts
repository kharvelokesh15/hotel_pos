import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { MyDatePickerModule } from 'mydatepicker';
import { ReceiptRoutingModule } from './receipt-routing.module';
import { MergeReceiptComponent } from './merge-receipt/merge-receipt.component';
import { ReceiptListingComponent } from './receipt-listing/receipt-listing.component';
import { SplitBillComponent } from './split-bill/split-bill.component';
import { NormalBillComponent } from './normal-bill/normal-bill.component';
import { CodBillComponent } from './cod-bill/cod-bill.component';
import { SplitReceiptComponent } from './split-receipt/split-receipt.component';
import { OrderTransferComponent } from './order-transfer/order-transfer.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MergeReceiptComponent,
    ReceiptListingComponent, SplitBillComponent,NormalBillComponent,CodBillComponent, SplitReceiptComponent, OrderTransferComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MyDatePickerModule,
    ReceiptRoutingModule,
    FormsModule,
    ModalModule.forRoot()

  ],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [
    MergeReceiptComponent
  ]
})
export class ReceiptModule { }
