import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-receipt-listing',
  templateUrl: './receipt-listing.component.html',
  styleUrls: ['./receipt-listing.component.css']
})
export class ReceiptListingComponent implements OnInit {
  public mergereceiptdata;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public KOTDate; 
  isTrade: boolean = false;
  checkAllTrades: boolean = false;
  modalRef: BsModalRef;

  constructor(public _router: Router, private headerService: HeaderService,private modalService: BsModalService) { }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {  class: 'list_receipt', 
      animated: true,
      // backdrop: 'static'
    });
  }

  
  ngOnInit() {
    this.headerService.setTitle('RECEIPT LISTING');

    this.mergereceiptdata = [
      {
        mergekot: '120/2', id: 1, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T2', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 1', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '121/25', id: 2, selected: false, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '1 Floor', mergetable: 'T3', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '128/23', id: 3, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '2 Floor', mergetable: 'T4', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '121/24', id: 4, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '3 Floor', mergetable: 'T5', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '122/27', id: 5, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '4 Floor', mergetable: 'T6', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '147/26', id: 6, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '4 Floor', mergetable: 'T7', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      },
      {
        mergekot: '152/44', id: 7, mergedate: '25-Jul-2019', mergetime: '8:55 PM', mergefloor: '4 Floor', mergetable: 'T8', mergetableserver: 'Dine IN / David', mergeGuestname: 'Guest 2', mergeAmount: '$1300', mergeDue: '$800'
      }

    ]
  }
  changeMergeByCategory(event) {
    if (event.target.name == 'mergereceiptdata') {
      this.isTrade = true
    }

    if (this.isTrade && this.checkAllTrades) {
      event.target.checked = true
    }
  }

  allmergeReceipt(event) {
    const checked = event.target.checked;
    this.mergereceiptdata.forEach(item => item.selected = checked);
  }


}
