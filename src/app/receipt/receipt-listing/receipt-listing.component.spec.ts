import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptListingComponent } from './receipt-listing.component';

describe('ReceiptListingComponent', () => {
  let component: ReceiptListingComponent;
  let fixture: ComponentFixture<ReceiptListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
