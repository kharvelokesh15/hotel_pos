import { Component, OnInit, TemplateRef } from '@angular/core';
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-split-receipt',
  templateUrl: './split-receipt.component.html',
  styleUrls: ['./split-receipt.component.css']
})
export class SplitReceiptComponent implements OnInit {
  public amounts;
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
 
      this.headerService.setTitle('Split Receipt');
      this.amounts = [
        { amoutValues: '1',amoutValues1: 'Fruit Salad',amoutValues2: '$100' },
        { amoutValues: '1',amoutValues1: 'Ice Cream',amoutValues2: '$500' },
        { amoutValues: '1',amoutValues1: 'Grill Chicken',amoutValues2: '$450' },
        { amoutValues: '1',amoutValues1: 'Mutton Biryani',amoutValues2: '$450' },
      ]
        
  
    
  }

}
