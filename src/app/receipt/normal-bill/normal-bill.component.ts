import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

//services
import { HeaderService } from './../../header.service';
import { KOTManageService, InvoieManageService } from './../../@service/kot';
import { BillingService } from './../../@service/billing/billing.service';
import { UtilityService } from '../../@service';
import { CustomerManageService } from '../../@service/customer';
import { TableManageService } from '../../@service/table/';


@Component({
  selector: 'app-normal-bill',
  templateUrl: './normal-bill.component.html',
  styleUrls: ['./normal-bill.component.css']
})
export class NormalBillComponent implements OnInit {
  // modalRef: BsModalRef;
  public amounts;
  public counter: number = 1;
  public customer_id;
  public waiter_id;
  public kotDetailsData;
  public kotDetailMenu;
  public kotDetailKot;
  grand_total: any;
  equally_splied_amt: any;
  fakeArray: any;
  split_type: any;
  public splited: number = 0;
  public spllied_data: any[];
  element_value = [];
  kot_id_col: any;
  split_by_menu: any;
  split_by_amount: any = [];
  split_by_amount_denominations: any = [];
  kot_id: any;
  split_bill_save: any;
  angular: any;
  public onClose: false;
  oldValue: any = 0;
  splitError: any;
  public CurentStaff;
  coupon: any = '';
  coupon_status: any = "";
  discount_value: any = 0.00;
  coupon_value: any = 0.00;
  coupon_flag = 0;
  sub_total = 0;
  couponError = '';
  currency;
  card_name: any;
  card_number: any;
  card_type: any;
  amountSubmit: any = [];

  splitpaid: any;
  splitbalance: any;
  total_data_entered: any = 0;
  show_completed: any = 0;
  split_bill_save_array: any;

  rewardData: any = [];
  enableFlag: boolean = false;
  ok_button: boolean = false;

  invoice_details: any;
  invoice_id: any;
  sales_type;

  public invoiceDetailsData;
  public invoiceWaiter;
  public invoiceCustomer;
  public invoiceOrganisation;

  constructor(public route: ActivatedRoute, public _router: Router, private headerService: HeaderService, private kot
    : KOTManageService, private billing: BillingService, private table: TableManageService, private customer: CustomerManageService, private invoice
      : InvoieManageService, private utilityService: UtilityService) { this.currency = this.utilityService.getCurrency(); }


  ngOnInit() {
    this.headerService.setTitle('Normal Bill');
    this.CurentStaff = this.utilityService.getCurrentUser();

    this.waiter_id = this.route.snapshot.paramMap.get('waiter_id');
    this.customer_id = this.route.snapshot.paramMap.get('customer_id');
    this.kot.kotMenuList({ "waiter_id": this.waiter_id, "customer_id": this.customer_id }).subscribe(res => {

      this.kotDetailsData = res;
      console.log(this.kotDetailsData.menu);
      this.kotDetailMenu = this.kotDetailsData.menu;
      this.kotDetailKot = this.kotDetailsData.kot;

      this.getTotalDiscountAmount();
      this.split_type = new Array(this.counter);
      this.card_name = new Array(this.counter);
      this.card_number = new Array(this.counter);
      this.card_type = new Array(this.counter);
      this.split_by_amount[0] = parseFloat(this.getTotalAmount()).toFixed(2);
      this.amountSubmit[0] = "";
      this.calulate_denominations(0);
      // this.split_by_amount_denominations[0] = {};
      // if (((Math.ceil(this.split_by_amount[0] / 5) * 5).toFixed(2)) == this.split_by_amount[0]) {
      //   this.split_by_amount_denominations[0].int = ((Math.ceil(this.split_by_amount[0])) + 5).toFixed(2);
      // } else if ((Math.ceil(this.split_by_amount[0])).toFixed(2) == this.split_by_amount[0]) {
      //   this.split_by_amount_denominations[0].int = (Math.ceil(this.split_by_amount[0] / 5) * 5).toFixed(2);
      // } else {
      //   this.split_by_amount_denominations[0].int = (Math.ceil(this.split_by_amount[0])).toFixed(2);
      // }

      // if (this.split_by_amount_denominations[0].int == (Math.ceil(this.split_by_amount[0] / 5) * 5).toFixed(2) || this.split_by_amount_denominations[0].int == ((Math.ceil(this.split_by_amount[0] / 5) * 5) + 5).toFixed(2)) {
      //   this.split_by_amount_denominations[0].int_five = (parseInt(this.split_by_amount_denominations[0].int) + 5).toFixed(2);
      //   this.split_by_amount_denominations[0].int_ten = (parseInt(this.split_by_amount_denominations[0].int) + 10).toFixed(2);
      // } else {
      //   this.split_by_amount_denominations[0].int_five = (Math.ceil(this.split_by_amount[0] / 5) * 5).toFixed(2);
      //   this.split_by_amount_denominations[0].int_ten = (Math.ceil(this.split_by_amount[0] / 10) * 10).toFixed(2);
      // }

      this.splitError = new Array(this.counter);


      this.splitpaid = new Array(this.counter);
      this.splitbalance = new Array(this.counter);
      this.split_bill_save_array = new Array(this.counter);

      for (let index = 0; index < this.splitpaid.length; index++) {
        this.splitpaid[index] = 0;
      }
      for (let index = 0; index < this.splitpaid.length; index++) {
        this.splitbalance[index] = 0;
      }

      this.customer.customerRewards({ "customer_id": this.customer_id }).subscribe(res => {
        this.rewardData = res;
        // console.log(Object.keys(this.rewardData).length);
        if (Object.keys(this.rewardData).length > 3) {
          // if(this.rewardData > 0){
          this.rewardData = res;
          this.enableFlag = true;
        } else {
          this.rewardData = res.data;
          this.enableFlag = false;
        }
      });
    });

  }

  calulate_denominations(i) {
    this.split_by_amount_denominations[i] = {};
    if (((Math.ceil(this.split_by_amount[i] / 5) * 5).toFixed(2)) == this.split_by_amount[i]) {
      this.split_by_amount_denominations[i].int = ((Math.ceil(this.split_by_amount[i])) + 5).toFixed(2);
    } else if ((Math.ceil(this.split_by_amount[i])).toFixed(2) == this.split_by_amount[i]) {
      this.split_by_amount_denominations[i].int = (Math.ceil(this.split_by_amount[i] / 5) * 5).toFixed(2);
    } else {
      this.split_by_amount_denominations[i].int = (Math.ceil(this.split_by_amount[i])).toFixed(2);
    }

    if (this.split_by_amount_denominations[i].int == (Math.ceil(this.split_by_amount[i] / 5) * 5).toFixed(2) || this.split_by_amount_denominations[i].int == ((Math.ceil(this.split_by_amount[i] / 5) * 5) + 5).toFixed(2)) {
      this.split_by_amount_denominations[i].int_five = (parseInt(this.split_by_amount_denominations[i].int) + 5).toFixed(2);
      this.split_by_amount_denominations[i].int_ten = (parseInt(this.split_by_amount_denominations[i].int) + 10).toFixed(2);
    } else {
      this.split_by_amount_denominations[i].int_five = (Math.ceil(this.split_by_amount[i] / 5) * 5).toFixed(2);
      this.split_by_amount_denominations[i].int_ten = (Math.ceil(this.split_by_amount[i] / 10) * 10).toFixed(2);

      if (this.split_by_amount_denominations[i].int_five == this.split_by_amount_denominations[i].int_ten) {
        this.split_by_amount_denominations[i].int_ten = (parseInt(this.split_by_amount_denominations[i].int_ten) + 5).toFixed(2);
      }
    }
  }

  getTotalAmount() {
    if (this.kotDetailKot) {
      let grand_total = 0;
      this.kotDetailKot.forEach(function (value) {
        if (value.status != 'cancelled') {
          grand_total += parseFloat(value.grand_total);
        }
      });
      let total = grand_total;
      this.sub_total = grand_total;
      return total.toFixed(2);
    }
  }
  getTotalDiscountAmount() {
    if (this.kotDetailKot) {
      let grand_discount_total = 0;
      this.kotDetailKot.forEach(function (value) {
        grand_discount_total += parseFloat(value.discount_total);
      });
      this.discount_value = grand_discount_total;
      this.coupon_value = grand_discount_total;

    }
  }

  getTotalQty() {
    if (this.kotDetailKot) {
      let total_qty = 0;
      this.kotDetailKot.forEach(function (value) {
        total_qty += parseInt(value.over_all_qty);
      });

      return total_qty;
    }
  }

  // checkingvalidtion(new_val, i) {
  //   let temp = parseFloat(this.oldValue) + parseFloat(new_val);
  //   let grand_total = parseFloat(this.getTotalAmount());

  //   if (grand_total < temp) {
  //     this.splitError[i] = 'Enter the correct amount'
  //   } else {
  //     this.oldValue = temp;
  //   }
  // }

  validateCoupon() {
    this.billing.couponValidate({ "coupon": this.coupon, "customer_id": this.kotDetailKot.map(v => v.customer_id) }).subscribe(res => {
      this.coupon_status = res;
      if (this.coupon_status != null) {
        console.log(this.coupon_status.discount_value);
        console.log(this.discount_value);
        if (this.coupon_status.discount_type == "percent") {
          this.discount_value = (this.discount_value + ((this.coupon_status.discount_value * 0.01) * this.sub_total)).toFixed(2);
          this.coupon_value = ((this.coupon_status.discount_value * 0.01) * this.sub_total).toFixed(2);
        } else {
          this.discount_value = (this.discount_value + this.coupon_status.discount_value).toFixed(2);
          this.coupon_value = this.coupon_status.discount_value.toFixed(2);
        }
        this.coupon_flag = 1;
      } else {
        this.coupon_flag = 0;
        this.discount_value = this.discount_value
        this.coupon_value = this.coupon_value
        this.couponError = 'Coupon is Invaild / Expired'
      }
    });
  }

  pressedKey(input: any, i, amt) {
    if (input == "clear") {
      this.amountSubmit[i] = "";
    } else {
      if (this.amountSubmit[i] == undefined) {
        this.amountSubmit[i] = input;
      } else {
        this.amountSubmit[i] = this.amountSubmit[i] + '' + input;
      }
      this.checkingvalidtion(this.amountSubmit[i], i, amt);
    }

  }

  pressedKeyDenominations(input: any, i, amt) {
    this.amountSubmit[i] = input;
    this.checkingvalidtion(input, i, amt);
  }

  // splited_data_save() {
  //   let values_getted;
  //   let temp;
  //   let grand_total = parseFloat(this.getTotalAmount());
  //   let localValue: any = 0;
  //   let i = 0;

  //   this.split_by_amount[i] = grand_total;

  //   delete this.split_by_amount['undefined'];

  //   this.split_bill_save = {
  //     "kot_id": this.kotDetailKot.map(v => v.id).join(','),
  //     "customer_name": this.kotDetailKot.map(v => v.customer_name),
  //     "customer_mobile": this.kotDetailKot.map(v => v.customer_mobile),
  //     "customer_id": this.kotDetailKot.map(v => v.customer_id),
  //     "kot_floor_id": this.kotDetailKot.map(v => v.kot_floor),
  //     "kot_table_id": this.kotDetailKot.map(v => v.kot_table_id),
  //     "kot_occupied": this.kotDetailKot.map(v => v.kot_occupied_chairs),
  //     "kot_type": this.kotDetailKot.map(v => v.kot_type),
  //     "app_user_id": this.CurentStaff.id,
  //     "over_all_qty": this.getTotalQty(),
  //     "payent_type": "cash",
  //     "waiter_id": this.waiter_id,
  //     "sub_total": this.sub_total,
  //     "tax_total": "0.00",
  //     "discount_amount": this.discount_value,
  //     "grand_total": this.getTotalAmount(),
  //     "paid_amount": this.getTotalAmount(),
  //     "balance_to_customer": "0.00",
  //     "balance_to_res": "0.00",
  //     "is_split_bill": "0",
  //     "split_type": "",
  //     "total_no_of_split_bill": this.counter,
  //     "notes": "",
  //     "status": "2",
  //     "coupon": this.coupon,
  //     "coupon_amount": this.coupon_value,
  //     "split_by_amount": this.split_by_amount,
  //   }

  //   this.utilityService.setsplitbillvalues(this.split_bill_save);
  //   this._router.navigate(['/dinein/DineinMenuOrder']);

  // }

  checkingvalidtion(new_val, i, amt, type = "") {
    if (type == 'points') {
      if ((this.rewardData.balance_amt - this.rewardData.mini_usage) >= new_val) {
        this.splitpaid[i] = new_val;
        this.splitbalance[i] = new_val - amt;
        this.ok_button = true;
      } else {
        this.ok_button = false;
      }

    } else {
      this.splitpaid[i] = new_val;
      this.splitbalance[i] = new_val - amt;
      this.ok_button = true;
    }

    if (parseFloat(this.splitpaid[i]) >= parseFloat(this.split_by_amount[i])) {
      this.ok_button = false;
      //this.splited_data_save(i);
      // this.show_completed = 1;
      // this.total_data_entered++;
    } else {
      this.show_completed = 0;
      this.ok_button = true;
    }
  }

  splited_data_save(type, index) {
    this.split_type[index] = type;

    if (this.splitpaid[index] != '' && this.splitpaid[index] > 0) {
      // if(this.splitpaid[index] >= this.split_by_amount[index]){
      let check_bal = 0;


      // for cash and card		
      if (parseFloat(this.splitpaid[index]) < parseFloat(this.split_by_amount[index])) {
        this.counter = (this.split_by_amount.length + 1);
        this.split_by_amount[this.split_by_amount.length] = this.split_by_amount[index] - this.splitpaid[index];

        this.calulate_denominations(this.split_by_amount.length - 1);
        check_bal = 1;
      }

      this.total_data_entered++;

      // console.log(this.total_data_entered+'======'+this.split_by_amount.length);
      if (this.total_data_entered == this.split_by_amount.length) {
        this.show_completed = 1;
      }

      let grand;
      if ((this.splitpaid.length > 1)) {
        console.log(this.splitpaid.length - 1 == index);
        grand = (this.splitpaid.length - 1 == index) ? this.split_by_amount[index - 1] - this.splitpaid[index - 1] : this.splitpaid[index];
      } else {
        grand = check_bal == 1 ? this.splitpaid[index] : this.getTotalAmount();
      }

      this.split_bill_save_array[index] = {
        "kot_id": this.kotDetailKot.map(v => v.id).join(','),
        "customer_id": this.kotDetailKot.map(v => v.customer_id)[0],
        "kot_type": this.kotDetailKot.map(v => v.kot_type)[0],
        "app_user_id": this.CurentStaff.id,
        "over_all_qty": this.getTotalQty(),
        "payent_type": this.split_type[index],
        "used_rewards": (this.split_type[index] == 'points') ? this.splitpaid[index] : 0,
        "waiter_id": this.waiter_id,
        "sub_total": this.sub_total,
        "tax_total": "0.00",
        "discount_amount": this.discount_value,
        "grand_total": grand,
        "paid_amount": this.splitpaid[index],
        "balance_to_customer": (this.splitbalance[index] >= 0) ? this.splitbalance[index] : 0,
        "balance_to_res": (this.splitbalance[index] < 0) ? this.splitbalance[index] : 0,
        "is_split_bill": "0",
        "split_type": "",
        "total_no_of_split_bill": this.counter,
        "notes": "",
        "status": "2",
        "split_by_menu": '0',
        "split_by_amount": this.splitpaid[index],
        "card_name": this.card_name[index] ? this.card_name[index] : '',
        "card_number": this.card_number[index] ? this.card_number[index] : '',
        "card_type": this.card_type[index] ? this.card_type[index] : '',
        "address_no": '',
        "address_street_1": '',
        "address_street_2": '',
        "address_region": '',
        "address_landmark": '',
        "address_country": '',
        "address_zipcode": '',
        "coupon": this.coupon,
        "coupon_amount": this.coupon_value,
        "floor_id": this.kotDetailKot.map(v => v.kot_floor)[index] ? this.kotDetailKot.map(v => v.kot_floor)[index] : 0,
      }

      if (this.show_completed == 1) {
        this.splited_save();
      }

    } else {
      alert('Enter the Bill Amount');
    }

  }

  splited_save() {
    if (this.total_data_entered == this.split_by_amount.length) {

      // if (this.split_bill_save.is_split_bill == 1) {
      //   for (let index = 0; index < this.split_bill_save_array.length; index++) {
      //     this.billing.splitBillReceiptSave(this.split_bill_save_array[index]).subscribe(res => {
      //       this.invoice_details = res;
      //       this.getinvoice(res.id);
      //       this.invoice_id = this.invoice_details.id;
      //       this.sales_type = this.invoice_details.sales_type;
      //       if (this.split_bill_save_array.length == (index + 1)) {
      //         if (this.sales_type == 'dinein') {
      //           let table_data = { "floor_id": this.split_bill_save.kot_floor_id[0], "customer_id": this.split_bill_save.customer_id[0], "table_id": this.split_bill_save.kot_table_id[0], "occupied_chairs": this.split_bill_save.kot_occupied[0], "is_booked": "2", "is_cancel": "" };
      //           this.table.tableSelectionClose(table_data).subscribe(res => {
      //             // if(this.split_bill_save_array.length == (index+1)){
      //             setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
      //             // }
      //           });
      //         } else {
      //           setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
      //         }

      //       }
      //     });

      //   }
      // } else {
      this.billing.splitBillReceiptSave(this.split_bill_save_array).subscribe(res => {
        this.invoice_details = res;
        this.getinvoice(res.id);
        this.invoice_id = this.invoice_details.id;
        let table_data = {
          "floor_id": this.kotDetailKot.map(v => v.kot_floor)[0],
          "customer_id": this.kotDetailKot.map(v => v.customer_id)[0],
          "table_id": this.kotDetailKot.map(v => v.kot_table_id)[0],
          "occupied_chairs": this.kotDetailKot.map(v => v.kot_occupied_chairs)[0],
          "is_booked": "2",
          "is_cancel": ""
        };
        this.sales_type = this.invoice_details.sales_type;
        if (this.sales_type == 'dinein') {
          this.table.tableSelectionClose(table_data).subscribe(res => {
            setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);

          });
        } else {
          setTimeout(() => { this._router.navigate(['/invoicelisting/invoicelisting/']); }, 3000);
        }

      });


    }
    // this._router.navigate(['/invoicelisting/invoiceDetails/'+this.invoice_id]);

    // this._router.navigate(['/kot/KotListing']);
  }

  getinvoice(id) {

    this.invoice.invoiceDetailsList(id).subscribe(res => {
      this.invoiceDetailsData = res;
      this.kotDetailMenu = this.invoiceDetailsData.kot_data;
      this.invoiceWaiter = this.invoiceDetailsData.waiter_id;
      this.invoiceCustomer = this.invoiceDetailsData.customer_id;
      this.invoiceOrganisation = this.invoiceDetailsData.organisation;

      setTimeout(function () {
        var printContent = document.getElementsByClassName('print-ticket');
        var WinPrint = window.open('', '', 'width=900,height=650');

        WinPrint.document.write(`<style>
				*.print-ticket {
					font-size: 8px;
					font-family: 'Times New Roman';
					text-transform: uppercase;
				}
				
				td,
				th,
				tr,
				table {
				   /*border-bottom: 2px dotted black;*/
					border-collapse: collapse;
				}
				table {
				   border-bottom: 2px dotted black;
				   width:100%;
					border-collapse: collapse;
				}
				
				td.description,
				th.description {
					width: 40%;
					max-width: 40%;
					font-size: 12px;
				}							
				td.quantity,
				th.quantity {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
					text-align: right;
					font-size: 12px;
				}

				td.price,
				th.price {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
					text-align: right;
				}
				td.total,
				th.total {
					width: 25%;
					max-width: 25%;
					word-break: break-all;
					text-align: right;
					padding-right: 10px;
					font-size: 12px;
				}
				th.item {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
				}
				th.item-value {
					width: 5%;
					max-width: 5%;
					word-break: break-all;
				}
				th.paid-total {
					width: 25%;
					max-width: 25%;
					word-break: break-all;
				}
				th.paid-total-value {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
				}
				th.balance {
					width: 20%;
					max-width: 20%;
					word-break: break-all;
				}
				th.balance-value {
					width: 15%;
					max-width: 15%;
					word-break: break-all;
				}
				
				.centered {
					text-align: center;
					align-content: center;
				}
				
				.ticket {
					width: 100%;
					max-width: 100%;
				}
				
				img.logo-print {
					background-size: cover;
					width: 120px;
					height: 120px;
					border: 5px solid white;
					padding: 0.25rem;
					border-radius: 50%;
				}
				table.inv_head {
					text-align: left;
				}
				.inv_head th.quantity {
					width: 60%;
				}
				.inv_head th.description {
					width: 40%;
				}
				@page { margin: 0; }
				</style>`+ printContent[0].innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close();

      }, 1000);

    });
  }

}

