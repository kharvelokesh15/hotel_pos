import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NormalBillComponent } from './normal-bill.component';

describe('NormalBillComponent', () => {
  let component: NormalBillComponent;
  let fixture: ComponentFixture<NormalBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalBillComponent ]
    })
    .compileComponents(); 
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
