import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
//services
import { HeaderService } from './../../header.service';
import { KOTManageService } from './../../@service/kot';
import { BillingService } from './../../@service/billing/billing.service';
import { UtilityService } from '../../@service';


@Component({
  selector: 'app-split-bill',
  templateUrl: './split-bill.component.html',
  styleUrls: ['./split-bill.component.css']
})
export class SplitBillComponent implements OnInit {
  modalRef: BsModalRef;
  public amounts;
  public counter: number = 2;
  public customer_id;
  public waiter_id;
  public kotDetailsData;
  public kotDetailMenu;
  public kotDetailKot;
  grand_total:any;
  equally_splied_amt:any;
  fakeArray:any;
  split_type:any;
  public splited:number = 0;
  public spllied_data: any[];
  element_value=[];
  kot_id_col:any;
  split_by_menu:any;
  split_by_amount:any;
  kot_id:any;
  split_bill_save:any;
  angular: any;
  public onClose: false;
  oldValue:any=0;
  splitError:any;
  public CurentStaff;
  coupon:any = '';
  coupon_status:any ="";
  discount_value:any = 0.00;
  coupon_value:any = 0.00;
  coupon_flag = 0;
  sub_total = 0;
  currency;
  
  constructor(public route: ActivatedRoute,public _router: Router,private headerService: HeaderService,private modalService: BsModalService,private kot
    : KOTManageService, private billing : BillingService,private utilityService: UtilityService) {this.currency = this.utilityService.getCurrency(); }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class : 'customClass_500',
      animated: true,
      // backdrop: 'static'
    });
  }
  public onCancel(): void {
    // this.onClose.next(false);
    this.modalRef.hide();
}


  ngOnInit() {
    this.headerService.setTitle('Split Bill');
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.waiter_id = this.route.snapshot.paramMap.get('waiter_id');
    this.customer_id = this.route.snapshot.paramMap.get('customer_id');
    this.kot.kotMenuList({"waiter_id":this.waiter_id,"customer_id":this.customer_id}).subscribe(res => {
      this.kotDetailsData = res;
      this.kotDetailMenu = this.kotDetailsData.menu;
      this.kotDetailKot = this.kotDetailsData.kot;

      this.getTotalAmount();
      this.getTotalQty();
    });

    this.split_by_amount = new Array(this.counter); 
    this.splitError =  new Array(this.counter); 
      
  }

    getTotalAmount() {  
      if (this.kotDetailMenu) {  
        let grand_total = 0;
            this.kotDetailMenu.forEach(function (value) {
              if(value.status != 'cancelled' ){
                grand_total +=  value.grand_total;
              }
            });
            let total = grand_total - this.coupon_value;
            this.sub_total = grand_total;
            return total.toFixed(2);  
        }  
    }  
    getTotalDiscountAmount() {  
      if (this.kotDetailKot) {  
        let grand_discount_total = 0;
        this.kotDetailKot.forEach(function (value) {
          grand_discount_total +=   parseFloat(value.discount_total);
        });
          this.discount_value = grand_discount_total;
         
      }
    }  
    getTotalQty() {  
      if (this.kotDetailMenu) {  
          let total_qty = 0;
            this.kotDetailMenu.forEach(function (value) {
              total_qty +=  value.qty;
            });
            
            return total_qty;  
        }  
    } 

    equally(type,i) {  
    
      this.split_type = type;
      let split_value = 0;

      if(this.split_type == 'equal'){
        this.grand_total = this.getTotalAmount();
        split_value =  this.grand_total / this.counter;
        this.equally_splied_amt = split_value;
        return this.split_by_amount[i] = split_value.toFixed(2); 
      } 
      if(this.split_type == 'amount'){
        return this.split_by_amount[i] = split_value.toFixed(2); 
      }
      if(this.split_type == 'menu'){
       return this.split_by_amount[i] = split_value.toFixed(2); 
      }
         
    }

    validateCoupon(){
      console.log(this.kotDetailKot.map(v=>v.customer_id));
      this.billing.couponValidate({"coupon":this.coupon,"customer_id":this.kotDetailKot.map(v=>v.customer_id)}).subscribe(res => {
        this.coupon_status = res;
        if(this.coupon_status != null){
          if(this.coupon_status.discount_type == "percent"){
            this.discount_value = (this.discount_value + ((this.coupon_status.discount_value * 0.01)*this.sub_total)).toFixed(2);
            this.coupon_value = ((this.coupon_status.discount_value * 0.01)*this.sub_total).toFixed(2);
          }else{
            this.discount_value = (this.discount_value + this.coupon_status.discount_value).toFixed(2);
            this.coupon_value = this.coupon_status.discount_value.toFixed(2);
          }
          // this.discount_value = this.coupon_status.discount_value.toFixed(2);
          // this.coupon_value = this.coupon_status.discount_value.toFixed(2);
          // this.coupon_value = this.coupon_status.discount_value.toFixed(2);
          this.coupon_flag = 1;
        }else{
          this.coupon_flag = 0;
          this.coupon_value = this.coupon_value
          this.discount_value = 0.00
        }
      });
    }


    checkingvalidtion(new_val,i){ 
      let temp = parseFloat(this.oldValue) + parseFloat(new_val);
      let grand_total = parseFloat(this.getTotalAmount());
     
      
      if(grand_total < temp ) {
        this.splitError[i] = 'Enter the correct amount'
      } else{
        this.oldValue = temp;
      }
    }

    add_splited_menu(target_value, value){
     
      if(target_value){
        this.splited += value;
      } else{
        this.splited -= value;
      }
     
    }

    splited_data_save(){
      let values_getted;
      let temp;
      let grand_total = parseFloat(this.getTotalAmount());
      let localValue:any=0;
      values_getted = document.getElementsByClassName('splited_amt');

      for (let index = 0; index < values_getted.length; index++) {
        let inner_value;
        inner_value = values_getted[index].value;
        temp = parseFloat(localValue) + parseFloat(values_getted[index].value );
        console.log(temp);
        if(grand_total < temp ){
          this.splitError[index] = 'Enter the correct amount';
          return false;
        } else{
          this.split_by_amount[index] = inner_value;
          localValue = temp;
        }
        // this.element_value.push(values_getted[index].value);
      }

     
      delete this.split_by_amount['undefined'];
     
      this.split_bill_save = {
        "kot_id":this.kotDetailKot.map(v=>v.id).join(','),
        "customer_name":this.kotDetailKot.map(v=>v.customer_name),
        "customer_mobile":this.kotDetailKot.map(v=>v.customer_mobile),
        "customer_id":this.kotDetailKot.map(v=>v.customer_id),
        "kot_floor_id":this.kotDetailKot.map(v=>v.kot_floor),
        "kot_table_id":this.kotDetailKot.map(v=>v.kot_table_id),
        "kot_occupied":this.kotDetailKot.map(v=>v.kot_occupied_chairs),
        "kot_type":this.kotDetailKot.map(v=>v.kot_type),
        "app_user_id":this.CurentStaff.id,
        "over_all_qty":this.getTotalQty(),
        "payent_type":"cash",
        "waiter_id":this.waiter_id,
        "sub_total":this.sub_total,
        "tax_total":"0.00",
        "discount_amount":this.discount_value,
        "grand_total":this.getTotalAmount(),
        "paid_amount":this.getTotalAmount(),
        "balance_to_customer":"0.00",
        "balance_to_res":"0.00",
        "is_split_bill":"1",
        "split_type":this.split_type,
        "total_no_of_split_bill": this.counter,
        "notes":"",
        "status":"2",
        "coupon":this.coupon,
        "coupon_amount":this.coupon_value,
        "split_by_menu":this.split_by_menu,
        "split_by_amount":this.split_by_amount
      }

      console.log(this.split_bill_save)
      this.utilityService.setsplitbillvalues(this.split_bill_save);
      this.modalRef.hide();
      this._router.navigate(['/dinein/DineinMenuOrder']);
    }

    
  increment() {
    this.counter += 1;
    this.split_by_amount = new Array(this.counter); 
  }

  decrement() {
    this.counter -= 1;
    this.split_by_amount = new Array(this.counter); 
  }
 

}

 