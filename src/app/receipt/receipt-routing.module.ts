import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MergeReceiptComponent } from './merge-receipt/merge-receipt.component';
import { ReceiptListingComponent } from './receipt-listing/receipt-listing.component';
import { SplitBillComponent } from './split-bill/split-bill.component';
import { NormalBillComponent } from './normal-bill/normal-bill.component';
import { CodBillComponent } from './cod-bill/cod-bill.component';
import { SplitReceiptComponent } from './split-receipt/split-receipt.component';
import { OrderTransferComponent } from './order-transfer/order-transfer.component';

const routes: Routes = [
  { path: 'DineinMergeReceipt', component: MergeReceiptComponent },
  { path: 'DineinMergeList', component: ReceiptListingComponent },
  { path: 'DineinSplitBill/:waiter_id/:customer_id', component: SplitBillComponent },
  { path: 'DineinNormalBill/:waiter_id/:customer_id', component: NormalBillComponent },
  { path: 'DineinCodBill/:waiter_id/:customer_id', component: CodBillComponent },
  { path: 'DineinSplitReceipt', component: SplitReceiptComponent },
  { path: 'DineinOrderTransfer', component: OrderTransferComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptRoutingModule { }
