import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

//services
import { HeaderService } from '../../header.service';
import { KOTManageService } from '../../@service/kot';
import { BillingService } from '../../@service/billing/billing.service';
import { UtilityService } from '../../@service';


@Component({
  selector: 'app-cod-bill',
  templateUrl: './cod-bill.component.html',
  styleUrls: ['./cod-bill.component.css']
})
export class CodBillComponent implements OnInit {
  // modalRef: BsModalRef;
  public amounts;
  public counter: number = 1;
  public customer_id;
  public waiter_id;
  public kotDetailsData;
  public kotDetailMenu;
  public kotDetailKot;
  grand_total:any;
  equally_splied_amt:any;
  fakeArray:any;
  split_type:any;
  public splited:number = 0;
  public spllied_data: any[];
  element_value=[];
  kot_id_col:any;
  split_by_menu:any;
  split_by_amount:any;
  kot_id:any;
  split_bill_save:any;
  angular: any;
  public onClose: false;
  oldValue:any=0;
  splitError:any;
  public CurentStaff;
  coupon:any = '';
  coupon_status:any ="";
  discount_value:any = 0.00;
  coupon_value:any = 0.00;
  coupon_flag = 0;
  sub_total = 0;
  couponError='';
  currency;
  
  
  constructor(public route: ActivatedRoute,public _router: Router,private headerService: HeaderService,private kot
    : KOTManageService, private billing : BillingService,private utilityService: UtilityService) { this.currency = this.utilityService.getCurrency(); }


  ngOnInit() {
    this.headerService.setTitle('Cash On Delivery');
    this.CurentStaff = this.utilityService.getCurrentUser();
    
    this.waiter_id = this.route.snapshot.paramMap.get('waiter_id');
    this.customer_id = this.route.snapshot.paramMap.get('customer_id');
    this.kot.kotMenuList({"waiter_id":this.waiter_id,"customer_id":this.customer_id}).subscribe(res => {
      this.kotDetailsData = res;
      this.kotDetailMenu = this.kotDetailsData.menu;
      this.kotDetailKot = this.kotDetailsData.kot;

      console.log(this.kotDetailsData);
      this.getTotalDiscountAmount();
    });

    this.split_type = 'cash';
    this.split_by_amount = new Array(this.counter); 
    this.splitError =  new Array(this.counter); 
      
  }

    // getTotalAmount() {  
    //   if (this.kotDetailKot) {  
    //     let grand_total = 0;
    //     this.kotDetailKot.forEach(function (value) {
    //       // this.kotDetailMenu.forEach(function (value) {
    //       // if(value.status != 'cancelled' ){
    //         // grand_total +=  value.grand_total;
    //       // }
    //       grand_total +=   parseFloat(value.grand_total);
    //     });
    //     // if(grand_total == 0){
    //       return  grand_total.toFixed(2);
    //     // } else{
    //       // return grand_total;
    //     // }
       
    //   }
    // } 
    getTotalAmount() {  
      if (this.kotDetailKot) {  
        let grand_total = 0;
       
        this.kotDetailMenu.forEach(function (value) {
          if(value.status != 'cancelled' ){
            grand_total +=  value.grand_total;
          }
        });
          let total = grand_total - this.coupon_value;
          this.sub_total = grand_total;
          return  total.toFixed(2);
      }
    }  
    getTotalDiscountAmount() {  
      if (this.kotDetailKot) {  
        let grand_discount_total = 0;
        this.kotDetailKot.forEach(function (value) {
          grand_discount_total +=   parseFloat(value.discount_total);
        });
          this.discount_value = grand_discount_total;
         
      }
    }  

    getTotalQty() {  
      if (this.kotDetailKot) {  
          let total_qty = 0;
            this.kotDetailKot.forEach(function (value) {
              total_qty +=  parseInt(value.over_all_qty);
            });
            
            return total_qty;  
        }  
    } 

    checkingvalidtion(new_val,i){ 
      let temp = parseFloat(this.oldValue) + parseFloat(new_val);
      let grand_total = parseFloat(this.getTotalAmount());
    
      if(grand_total < temp ) {
        this.splitError[i] = 'Enter the correct amount'
      } else{
        this.oldValue = temp;
      }
    }

    validateCoupon(){
      console.log(this.kotDetailKot.map(v=>v.customer_id));
      this.billing.couponValidate({"coupon":this.coupon,"customer_id":this.kotDetailKot.map(v=>v.customer_id)}).subscribe(res => {
        this.coupon_status = res;
        if(this.coupon_status != null){
          console.log(this.coupon_status);
          if(this.coupon_status.discount_type == "percent"){
            this.discount_value = (this.discount_value + ((this.coupon_status.discount_value * 0.01)*this.sub_total)).toFixed(2);
            this.coupon_value = ((this.coupon_status.discount_value * 0.01)*this.sub_total).toFixed(2);
          }else{
            this.discount_value = (this.discount_value + this.coupon_status.discount_value).toFixed(2);
            this.coupon_value = this.coupon_status.discount_value.toFixed(2);
          }
          this.coupon_flag = 1;
        }else{
          this.coupon_flag = 0;
          this.discount_value = this.discount_value
          this.coupon_value = this.coupon_value
          this.couponError = 'Coupon is Invaild / Expired'
        }
      });
    }

    splited_data_save(){
      let values_getted;
      let temp;
      let grand_total = parseFloat(this.getTotalAmount());
      let localValue:any=0;
      let i=0;
     
      this.split_by_amount[i] = grand_total;
        
      delete this.split_by_amount['undefined'];
     
      this.split_bill_save = {
        "kot_id":this.kotDetailKot.map(v=>v.id).join(','),
        "customer_name":this.kotDetailKot.map(v=>v.customer_name),
        "customer_mobile":this.kotDetailKot.map(v=>v.customer_mobile),
        "customer_id":this.kotDetailKot.map(v=>v.customer_id),
        "kot_floor_id":this.kotDetailKot.map(v=>v.kot_floor),
        "kot_table_id":this.kotDetailKot.map(v=>v.kot_table_id),
        "kot_occupied":this.kotDetailKot.map(v=>v.kot_occupied_chairs),
        "kot_type":this.kotDetailKot.map(v=>v.kot_type),
        "app_user_id":this.CurentStaff.id,
        "over_all_qty":this.getTotalQty(),
        "payent_type":"cod",
        "waiter_id":this.waiter_id,
        "sub_total":this.sub_total,
        "tax_total":"0.00",
        "discount_amount":this.discount_value,
        "grand_total":this.getTotalAmount(),
        "paid_amount":this.getTotalAmount(),
        "balance_to_customer":"0.00",
        "balance_to_res":"0.00",
        "is_split_bill":"0",
        "split_type":"",
        "total_no_of_split_bill": this.counter,
        "notes":"",
        "status":"1",
        "coupon":this.coupon,
        "coupon_amount":this.coupon_value,
        "split_by_amount":this.split_by_amount,
      }

      console.log(this.split_bill_save);
      this.utilityService.setsplitbillvalues(this.split_bill_save);
      this._router.navigate(['/dinein/DineinMenuOrder']);
      
    }

}

 