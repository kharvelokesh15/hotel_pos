import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CodBillComponent } from './cod-bill.component';

describe('CodBillComponent', () => {
  let component: CodBillComponent;
  let fixture: ComponentFixture<CodBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodBillComponent ]
    })
    .compileComponents(); 
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
