import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeReceiptComponent } from './merge-receipt.component';

describe('MergeReceiptComponent', () => {
  let component: MergeReceiptComponent;
  let fixture: ComponentFixture<MergeReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
