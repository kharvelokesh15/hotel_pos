import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTransferComponent } from './order-transfer.component';

describe('OrderTransferComponent', () => {
  let component: OrderTransferComponent;
  let fixture: ComponentFixture<OrderTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
