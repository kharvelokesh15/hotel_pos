import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '..';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

  accountList() {
    return this.httpService.get('account-list');
  }

  accountSave(account) {
    return this.httpService.postJson('account-save', account);
  }
  

  
}
