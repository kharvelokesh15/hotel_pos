import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class CustomerManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    customerDetails(phone){
      return this.httpService.postJson('table-list', { "phone":phone});
    }
  
    customerList(){
    return this.httpService.postJson('customer-list',{});
   }

   customerSave(data){
    return this.httpService.postJson('customer-save', data);
  }
  customerUpdate(data){
    return this.httpService.postJson('customer-update', data);
  }
  
  guestSave(data){
    return this.httpService.postJson('guest-save', data);
  }
  
  customerSearch(query){
    return this.httpService.postJson('customer-search', {
      "data":query
    });
  }
  
  customerRewards(data){
    return this.httpService.postJson('customer-rewards-list', data);
  }

  customerAddressList(data){
    return this.httpService.postJson('customer-address-list', data);
  }

  customerAddressDetails(data){
    return this.httpService.postJson('customer-address-details', data);
  }
  customerAddressSave(data){
    return this.httpService.postJson('customer-address-save', data);
  }

  customerAddressRemove(data){
    return this.httpService.postJson('customer-address-remove', data);
  }
  

}
