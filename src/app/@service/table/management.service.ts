import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class TableManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

  floorList(){
    return this.httpService.get('floor-list');
  }
  
  tableSelection(){
    return this.httpService.postJson('table-selection',{});
  }

  tableList(selectedfloor){
    return this.httpService.postJson('table-list', { "floor_id":selectedfloor});
  }

  
  tableSelectionSave(selectedTable){
        return this.httpService.postJson('table-selection-save', selectedTable);
  }

  tableAvailDetails(Floor){
    return this.httpService.postJson('table-avail-details', Floor);
  }

  tableSelectionUpdate(Floor: Request){
    return this.httpService.postJson('table-selection-update', Floor);
  }

  tableSelectionClose(data){
    return this.httpService.postJson('table-selection-update',data);
  }

  tableReservationDetailsList(data){
    return this.httpService.postJson('table-reservation-details-list', data);
  }


}
