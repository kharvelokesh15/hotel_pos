import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class BillingService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

  kotSave(Kot: Request) {
    return this.httpService.postJson('kot-save', Kot).map(response => response.json());
  }

  kotUpdate(Kot: Request) {
    return this.httpService.postJson('kot-update', Kot).map(response => response.json());
  }

  kotIdList() {
    return this.httpService.get('kot-id-list');
  }

  kotDetailsList() {
    return this.httpService.get('kot-details-list');
  }

  kotList() {
    return this.httpService.get('kot-list');
  }

  InvoiceSave(Invoice: Request) {
    return this.httpService.postJson('invoice-save', Invoice).map(response => response.json());
  }

  InvoiceUpdate(Invoice: Request) {
    return this.httpService.postJson('invoice-update', Invoice).map(response => response.json());
  }

  // splitBillReceiptSave(splitBillReceipt: Request) {
  //   return this.httpService.postJson('split-bill-receipt-save', splitBillReceipt).map(response => response.json());
  // }
  splitBillReceiptSave(data) {
    return this.httpService.postJson('split-bill-receipt-save', data);
  }

  couponValidate(data){
    return this.httpService.postJson('cart-coupon-validate',data);
  }

  waiterSalesList() {
    return this.httpService.get('waiter-sales');
  }

  
}
