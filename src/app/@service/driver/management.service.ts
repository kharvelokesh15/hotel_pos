import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class DriverManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    driverList(){
      return this.httpService.get('driver-list');
    }
    driverCreate(data){
      return this.httpService.postJson('driver-save',data);
    }
  
    driverUpdate(id,data){
      return this.httpService.postJson('driver-update/'+id,data);
    }
  
    driverDelete(id){
      return this.httpService.get('driver-delete/'+id);
    }
  
}
