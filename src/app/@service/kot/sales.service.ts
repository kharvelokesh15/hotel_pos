import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class SalesManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    waiterSales(waiter_id){
      return this.httpService.postJson('waiter-sales',{	"waiter_id":waiter_id });
    }
    totalSales(data){
      return this.httpService.postJson('total-sales',data);
    }
  
  
}
