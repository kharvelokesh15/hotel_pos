import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class InvoieManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    invoicSave(data){
      return this.httpService.postJson('invoice-save',data);
    }
    invoiceUpdate(data){
      return this.httpService.postJson('invoice-update',data);
    }
    invoiceList(data){
      return this.httpService.postJson('invoice-list',data);
    }
    invoiceDetailsList(invoice_id){
      return this.httpService.postJson('invoice-details-list',{"invoice_id":invoice_id });
    }
  
  
}
