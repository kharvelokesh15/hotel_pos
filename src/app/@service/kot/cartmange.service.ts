import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class CartManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    CatCreate(data){
      return this.httpService.postJson('kot-cart-create',data);
    }
    CartAddItem(data){
      return this.httpService.postJson('kot-cart-item-add',data);
    }
  
    CartItemList(cart_id){
      return this.httpService.postJson('kot-cart-item-list',{"cart_id":cart_id });
    }
    CartItemUpdate(data){
      return this.httpService.postJson('kot-cart-item-update',data);
    }
  
    CartItemRemove(data){
      return this.httpService.postJson('kot-cart-item-remove',data);
    }
  
  
}
