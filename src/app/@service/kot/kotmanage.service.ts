import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class KOTManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    kotSave(data){
      return this.httpService.postJson('kot-save',data);
    }
    kotIdList(data){
      return this.httpService.postJson('kot-id-list',data);
    }
  
    kotDetailsList(kot_id){
      return this.httpService.postJson('kot-details-list',{"kot_id":kot_id });
    }
    kotList(data){
      return this.httpService.postJson('kot-list',data);
    }
  
    kotUpdate(data){
      return this.httpService.postJson('kot-update',data);
    }

    kotMenuList(data){
      return this.httpService.postJson('kot-menu-details-list',data);
    }

    KotMenuDetails(data){
      return this.httpService.postJson('kot-menu-details',data);
    }

    KotPosCancel(data){
      return this.httpService.postJson('kot-pos-cancel',data);
    }

    KotPosUpdate(data){
      return this.httpService.postJson('kot-pos-update',data);
    }

    KotPosMultiUpdate(data){
      return this.httpService.postJson('kot-pos-multi-update',data);
    }

    KotPosMultiComplete(data){
      return this.httpService.postJson('kot-pos-multi-completed',data);
    }
    
    KotPosHold(data){
      return this.httpService.postJson('kot-pos-hold',data);
    }  

    tableFullKotDetails(data){
      return this.httpService.postJson('kot-id-list',data);
    }
  
  
}
