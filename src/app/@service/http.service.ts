import { Injectable,Inject  } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { filter, finalize, map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import DownloadedFile from '../@models/downloaded-file';
import { ToastrService } from 'ngx-toastr';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';
import { UtilityService } from '../@service/utility.service';
import { saveAs } from 'file-saver';
import { Location } from '@angular/common';
import { WINDOW } from '../@providers/window.provider';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Injectable()
export class HttpService {
    public static DOWNLOAD_PROGRESS_TITLE: string ='Downloading Contents';
    public static DOWNLOAD_PROGRESS_MESSAGE: string ='Content download in progress, please wait';
    public static DOWNLOAD_COMPLETED_TITLE: string = 'Downloading Contents';
    public static DOWNLOAD_COMPLETED_MESSAGE: string = 'Completed';
    public static DOWNLOAD_ERROR_TITLE: string = 'Error';
    public static DOWNLOAD_ERROR_MESSAGE: string ='The content could not be downloaded, please try again';
    public static GENERIC_ERROR_TITLE = 'Error';
    private ngProgressRef: NgProgressRef;

    constructor(@Inject(WINDOW) private window: Window,
        private http: HttpClient,
        private ngProgress: NgProgress,
        private toastr: ToastrService,
        private utilityService: UtilityService,
        private router: Router,
        private loc: Location,
        private spinner:NgxSpinnerService) {
        console.log(this.loc.path());
        this.ngProgressRef = this.ngProgress.ref();
    }
    getHostname() : string {
        return (this.window.location.hostname.split(".")[0]);
    }
    envUrl(){
        return environment.https+environment.api_url;
    }
    get(url: string, needRawData=false): Observable<any> {
        
        let httpHeaders = this.getJsonHeaders();
        let basedomaine = this.getHostname();
        console.log(basedomaine);
        this.spinner.show();
        let httpObservable = this.http.get<any>(this.envUrl() + url, {
            headers: httpHeaders  
        });

        setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
        });
    
        return this.handleResponse(httpObservable, needRawData);
        
    }

    pageget(url: string): Observable<any> {
        let httpHeaders = this.getJsonHeaders();
        let basedomaine = this.getHostname();
        console.log(basedomaine);
        let httpObservable = this.http.get<any>(url, {
            headers: httpHeaders
        });

        return this.handleResponse(httpObservable);
    }

    postJson(url: string, body: any): Observable<any> {
        // console.log('fsa',body);
        let httpHeaders = this.getJsonHeaders();
        let basedomaine = this.getHostname();
        console.log(basedomaine);
        let httpObservable = this.http.post<any>(
            this.envUrl() + url, body, {
                headers: httpHeaders
            });

        return this.handleResponse(httpObservable);
    }

    postForm(url: string, formData: FormData): Observable<any> {
        let httpHeaders = this.getFormHeaders();
        let basedomaine = this.getHostname();
        let httpObservable = this.http.post<any>(
            this.envUrl() + url, formData, {
                headers: httpHeaders
            });

        return this.handleResponse(httpObservable);
    }

    downloadFile(
        url: string,
        body: any,
        progressTitle: string = HttpService.DOWNLOAD_PROGRESS_TITLE,
        progressMessage: string = HttpService.DOWNLOAD_PROGRESS_MESSAGE,
        completedTitle: string = HttpService.DOWNLOAD_COMPLETED_TITLE,
        completedMessage: string = HttpService.DOWNLOAD_COMPLETED_MESSAGE,
        errorTitle: string = HttpService.DOWNLOAD_ERROR_TITLE,
        errorMessage: string = HttpService.DOWNLOAD_ERROR_MESSAGE) {
        const toast = this.toastr.warning(progressMessage, progressTitle, {
            disableTimeOut: true,
            tapToDismiss: false
        }); 
        this.getFile(url, body)
            .pipe(finalize(() => this.toastr.remove(toast.toastId)))
            .subscribe( 
                data => {
                    console.log(data);
                    // this.downloadFilebrowser(data);
                    this.toastr.success(completedMessage, completedTitle);
                   
                    saveAs(data.blob, data.filename);
                },
                (error) => {
                    console.log(error);
                    this.toastr.error(errorMessage, errorTitle, {
                        closeButton: true,
                        disableTimeOut: true
                    });
                }
            );
    }
    downloadFilebrowser(data: Response) {
        const blob = new Blob([data.url], {'type':"application/octet-stream"});
        const url= window.URL.createObjectURL(blob);
        window.open(url);
      }

    uploadFile(url: string, formData: FormData) {
        let httpHeaders = this.getFormHeaders();
        let basedomaine = this.getHostname();
        // console.log(basedomaine);
        this.ngProgressRef.start();
        return this.http.post(this.envUrl() + url, formData, {
            observe: 'events',
            reportProgress: true,
            headers: httpHeaders
        }).pipe(
            map(data => {
                if (data.type === HttpEventType.UploadProgress) {
                    // progress
                    this.ngProgressRef.set(data.loaded / data.total);
                } else if (data.type === HttpEventType.Response) {
                    // response
                    return data.body;
                }
                return null;
            }),
            filter(data => data !== null),
            finalize(() => this.ngProgressRef.complete())
        );
    }    

    private getFile(url: string, body: any): Observable<DownloadedFile> {
        let httpHeaders = this.getJsonHeaders();
        let basedomaine = this.getHostname();
        console.log(basedomaine);
        this.ngProgressRef.start();

        return this.http.post<Blob>(this.envUrl() + url, body, {
            headers: httpHeaders,
            responseType: 'blob' as 'json',
            reportProgress: true,
            observe: 'events'
        }).pipe(
            map(data => {
                if (data.type === HttpEventType.DownloadProgress) {
                    // progress
                    this.ngProgressRef.set(data.loaded / data.total);
                } else if (data.type === HttpEventType.Response) {
                    // response
                    const content = data.headers.get('Content-Disposition');
                        if(content.toString().indexOf('=') > 0){
                            let filename = content.split('filename=');
                            return {
                                blob: data.body,
                                content_type: data.headers.get('Content-Type'),
                                filename: filename[1]
                            };
                        }
                        else {
                            return null;
                        } 
                }
                return null;
            }),
            filter(data => data !== null),
            finalize(() => this.ngProgressRef.complete())
        );
    }

    //Check Company Url Valid
    checkCompany(url: string): Observable<any> {
      let httpHeaders = this.getJsonHeaders();
      let httpObservable = this.http.get<any>(this.envUrl() + url, {
        headers: httpHeaders
      });
      return this.handleResponse(httpObservable);
    }

    private handleResponse(httpObservable: Observable<any>, needRawData?): Observable<any> {
        return httpObservable.pipe(
            map(response => {
                console.log(response);
                let message = null;
                if (response.message !== null
                    && response.message !== undefined) {
                    message = response.message;
                }

                if (response.status == 'success' || response.status == 'Success') {
                    if(needRawData){
                        return response;
                    }
                    if(response.list){
                        return response.list
                    }
                    if(response.cartDetails){
                        return response.cartDetails
                    }
                    if(response.cartItemDetail){
                        return response.cartItemDetail
                    }
                    if(response.kotDetail){
                        return response.kotDetail
                    }
                    if(response.data == ''){
                        return response;
                    }else{
                        return response.data;
                    }
                    //return response.data;
                } else if (response.status == 'error') {
                    this.spinner.hide();
                    if(response.message == 'Unauthorised')
                        this.router.navigate(['/auth/login']);
                    throw new Error(message);
                }else if(response.status == 'nocompany'){
                  window.location.href = '/'
                }else if(response.status == 'upgrade'){
                  window.location.href = response.data;
                }else if(response.status == 'warning'){
                    return response;
                }else if(response.status == 'login-error'){
                    this.toastr.error(response.message,'Login',{
                        closeButton: true,
                        disableTimeOut: false
                    });
                    return response;
                }
                else{
                    if(!response.data || response.data == ''){
                        return response;
                    }else{
                        return response.data;
                    }

                }
            }),
            catchError((error: any) => {
                let message = null;
                if (error.message !== null
                    && error.message !== undefined) {
                    message = error.message;
                } else {
                    message = error;
                }

                this.toastr.error(message, HttpService.GENERIC_ERROR_TITLE, {
                    closeButton: true,
                    disableTimeOut: false
                });

                return throwError(message);
            })
        );
    }

    getJsonHeaders() {
        let httpHeadersList = this.getDefaultHeadersList();

        Object.assign(httpHeadersList, {
            'Content-Type': 'application/json',
            'x-org-id': environment.org,
            'x-branch-id': environment.branch,
            'X-domain-company' : this.getHostname(), 
        });
      //  console.log(httpHeadersList);
        return new HttpHeaders(httpHeadersList);
    }

    private getFormHeaders() {
        let httpHeadersList = this.getDefaultHeadersList();
        Object.assign(httpHeadersList, {
            'X-domain-company' : this.getHostname(), 
        });

        return new HttpHeaders(httpHeadersList);
    }

    private getDefaultHeadersList() {
        let token = this.utilityService.getCurrentUserToken();

        let httpHeadersList = {
            'Access-Control-Allow-Origin': '*',
            'User-Timezone': this.utilityService.getCurrentTimezone()
        }

        if (token !== null) {
            Object.assign(httpHeadersList, {
                'Authorization': 'Bearer ' + token
            });
        }else{
            Object.assign(httpHeadersList, {
                'Authorization': 'Basic Ym9va2Vwcl9wcm9qZWN0OjE2MDAyMzExMjcwNzNBQjE4QzA5ODlGNjE3RUVDRDkxMkNENTYyOUU='
            });
        }

        return httpHeadersList;
    }
}
