import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class DeliveryManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    deliveryList(data){
      return this.httpService.postJson('list-order-delivery',data);
    }

    deliveryCreate(data){ 
      return this.httpService.postJson('create-order-delivery',data);
    }

    deliveryUpdate(id, data){ 
      return this.httpService.postJson('update-order-delivery/'+ id,data);
    }
    deliveryBulkUpdate(data){ 
      return this.httpService.postJson('update-bulk-order-delivery',data);
    }
    driverCreate(data){ 
      return this.httpService.postJson('driver-save',data);
    }
  
    driverUpdate(id, data){
      return this.httpService.postJson('driver-update/'+id,data);
    }
  
    driverDelete(id){
      return this.httpService.get('driver-delete/'+id);
    }

  
}
