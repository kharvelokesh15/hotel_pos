import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
//base and common apis:
@Injectable()
export class BaseService {

  constructor(public httpService: HttpService) { }
  
  Branchinfo(data = {}){
    return this.httpService.postJson('branch-details',data);
  }
}

