import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class MenuManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

  menuCategory(){
    return this.httpService.postJson('menu-category',{});
  }
  
  menuList(){
    return this.httpService.get('menu-list');
  }
  
  menuGroup(){
    return this.httpService.postJson('menu-group',{});
  }
  
  menuCategorySearch(query){
    return this.httpService.postJson('menu-category-search',{"category":query});
  }

  menuDetails(id){
    return this.httpService.postJson('menu-details',{"menu_id":id});
  }
  menuSearch(query,cat,group){
    return this.httpService.postJson('menu-group-category',{	
      "search_data":query,
      "category_id":cat,
      "group_id":group
    });
  }



}
