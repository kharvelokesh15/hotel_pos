import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class StaffManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    staffList(){
      return this.httpService.get('staff-list');
    }
  
  
}
