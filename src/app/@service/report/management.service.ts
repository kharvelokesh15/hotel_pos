import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '..';

@Injectable({
  providedIn: 'root'
})
export class ReportManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }



  getReports(data) {
    return this.httpService.postJson('reports', data);
  }
  addOpen(data) {
    return this.httpService.postJson('add-opening', data);
  }
  addClose(data) {
    return this.httpService.postJson('add-closing', data);
  }
  addDayEnd(data) {
    return this.httpService.postJson('add-day-end', data);
  }

  getReport(url) {
    return this.httpService.get(url, true);
  }

}
