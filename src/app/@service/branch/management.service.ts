import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class BranchManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

    
  
  branchList(){
    return this.httpService.get('branch-list');
   }


}
