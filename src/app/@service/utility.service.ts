import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class UtilityService {
    public static DEFAULT_IMAGE: string = '/assets/img/poster.jpg';
    public static SERVER_APP_VERSION: string = "";
    public static CLIENT_APP_VERSION: string = environment.version;
    public static DEFAULT_TIMEZONE: string = "Europe/Rome";
    public ModuleList={
        "dinein":{"Module":"Dine IN","Route":"/dinein/DineinSelection","Key":"dinein"},
        "takeaway":{"Module":"Take Away","Route":"/dinein/DineinMenuListCategory","Key":"takeaway"},///takeaway/customerinformation /dinein/DineinCustomers
        "delivery":{"Module":"Delivery","Route":"/dinein/DineinMenuListCategory","Key":"delivery"},///takeaway/customerinformation /dinein/DineinCustomers
         "reservation":{"Module":"Reservation","Route":"/tablereservation/ChargeTableReservation","Key":"reservation"}, // tablereservation/ChargeTableReservation
         "callcenter": {"Module":"Call Center","Route":"/callcenter/SelectBranch","Key":"callcenter"},
        //   "delivery":{"Module":"Delivery","Route":"/newtrip/newtriplist","Key":"delivery"},
          "report":{"Module":"Report","Route":"/report/SalesReports","Key":"report"},
          "nocharge":{"Module":"No Charge","Route":"/nocharge/ChargeStaff","Key":"nocharge"},
          "miscellanousSales":{"Module":"Miscellanous Sales","Route":"/miscellanous/MiscellanousSales","Key":"miscellanousSales"},
          "accountpayment":{"Module":"Account Payment","Route":"/account-payment/accountpaylist","Key":"accountpayment"},
        }
    constructor() {
    }

    clearDinine(){
        this.setCartDetails('');
        this.setTableSelection('','');
        this.setNumSeat('');
        this.setcartitems('');
        this.setSelectedCustomer('');
        return true;
    }

    public appendObjectToFormData(object: any, formData: FormData,
        appendNull: boolean = false, appendUndefined: boolean = false) {
        for (let key in object) {
            if (object[key] === null) {
                if (appendNull === true) {
                    formData.append(key, object[key]);
                }
            } else if (object[key] === undefined) {
                if (appendUndefined === true) {
                    formData.append(key, object[key]);
                }
            } else {
                formData.append(key, object[key]);
            }
        }
    }

    getOrg(){
        return environment.org;
    }
    
    getOrgInformation(){
        let orgInformation = JSON.parse(localStorage.getItem('orgInformation'));
        return orgInformation;
    }

    getLogoPath(){
        let logoPath = JSON.parse(localStorage.getItem('logoPath'));
        return logoPath;
    }
    getBranch(){
        return environment.branch;
    }
    setOrgInformation(orgInformation){
        localStorage.setItem('orgInformation',JSON.stringify(orgInformation));
    }
    setOrg(org){
        localStorage.setItem('org',JSON.stringify(org));
    }
    setLogoPath(path){
        localStorage.setItem('logoPath',JSON.stringify(path));
    }
    setBranch(branch){
        localStorage.setItem('branch',JSON.stringify(branch));
    }
    setCallcenterBranch(callcenterbranch){
        localStorage.setItem('callcenterbranch',JSON.stringify(callcenterbranch));
    }
    
    setCurrentUser(userData,type) {
        try {
            localStorage.setItem('currentUser',JSON.stringify(userData));
            localStorage.setItem('currentUsertype',type);
            localStorage.setItem('currency',JSON.stringify(userData.store_information.org_default_currcency));
           // sessionStorage.setItem('currentUser',JSON.stringify(userData));    
        } catch (e) { }
    }
    setSelectedCustomer(customer){
        try {
            localStorage.setItem('customer',JSON.stringify(customer)); 
        } catch (e) { }
    }
    getSelectedCustomer(){
        try {
            let selectedcustomer = JSON.parse(localStorage.getItem('customer'));
            if (selectedcustomer !== null) {
                return selectedcustomer;
            } else {
                return {};
            }
        } catch (e) {
            return {};
        }
    }

    setSelectedAddress(address){
        try {
            localStorage.setItem('address',JSON.stringify(address)); 
        } catch (e) { }
    }
    getSelectedAddress(){
        try {
            let selectedAddress = JSON.parse(localStorage.getItem('address'));
            if (selectedAddress !== null) {
                return selectedAddress;
            } else {
                return {};
            }
        } catch (e) {
            return {};
        }
    }

    setSelectedBranch(selectedBranch){
        try {
            localStorage.setItem('selectedBranch',JSON.stringify(selectedBranch)); 
        } catch (e) { }
    }
    getSelectedBranch(){
        try {
            let selectedbranch = JSON.parse(localStorage.getItem('selectedBranch'));
            if (selectedbranch !== null) {
                return selectedbranch;
            } else {
                return {};
            }
        } catch (e) {
            return {};
        }
    }

    setcartitems(cartiems){
        try {
            localStorage.setItem('cartiems',JSON.stringify(cartiems)); 
        } catch (e) { }
    }
    getcartitems(){
        try {
            let cartiems = JSON.parse(localStorage.getItem('cartiems'));
            if (cartiems !== null) {
                return cartiems;
            } else {
                return [];
            }
        } catch (e) {
            return [];
        }
    }
    setCartDetails(CartDetails){
        try {
            localStorage.setItem('CartDetails',JSON.stringify(CartDetails)); 
        } catch (e) { }
    }
    getCartDetails(){
        try {
            let CartDetails = JSON.parse(localStorage.getItem('CartDetails'));
            if (CartDetails !== null) {
                return CartDetails;
            } else {
                return [];
            }
        } catch (e) {
            return [];
        }
    }

    SetModule(module){
        try {
            localStorage.setItem('module',JSON.stringify(module)); 
        } catch (e) { }
    }
    GetModule(){
        try {
            let module = JSON.parse(localStorage.getItem('module'));
            if (module !== null) {
                return module;
            } else {
                return '';
            }
        } catch (e) {
            return '';
        }
    }
    GetModuleInfo(module = ''){
        if(module){
            return this.ModuleList[module];
        }else{
            return this.ModuleList[this.GetModule()];
        }
    }
    

    removeCurrentUser() {
        try {
            localStorage.removeItem('currentUser');
           // sessionStorage.removeItem('currentUser');
        } catch (e) { }
    }

    getCurrentUser() {
        try {
            let authObj = JSON.parse(localStorage.getItem('currentUser'));
            if (authObj !== null && authObj.waiter !== undefined
                && authObj.waiter !== null) {
                return authObj.waiter;
            } else if (authObj !== null && authObj.employee !== undefined
                && authObj.employee !== null) {
                return authObj.employee;
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    getCurrency() {
        try {
            let authObj = JSON.parse(localStorage.getItem('currency'));
            if (authObj !== null && authObj !== undefined) {
                return authObj;
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    getCurrentUserToken() {
        try {
            let authObj = JSON.parse(localStorage.getItem('currentUser'));
            if (authObj !== null && authObj.token !== undefined
                && authObj.token !== null) {
                return authObj.token;
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    async delay(ms: number) {
        await new Promise(resolve => setTimeout(() => resolve(), ms));
    }

    mergePropertyToObjectIfNotNull(object: Object, propertyName: string,
        propertyValue: any, checkEmptyString: boolean = false) {
        if (propertyValue !== null) {
            if (checkEmptyString === true) {
                try {
                    if (propertyValue.trim() !== '') {
                        object[propertyName] = propertyValue;
                    }
                } catch (e) { }
            } else {
                object[propertyName] = propertyValue;
            }
        }
    }

    getCurrentTimezone() {
        try {
            return Intl.DateTimeFormat().resolvedOptions().timeZone;
        } catch (e) { }

        return UtilityService.DEFAULT_TIMEZONE;
    }

    getDateForNgbDatepicker(date: string) {
        let dateArray = date.split('-');

        return {
            year: Number.parseInt(dateArray[0]),
            month: Number.parseInt(dateArray[1]),
            day: Number.parseInt(dateArray[2])
        };
    }

    getAuthorizedUrl(url) {
        var authorizedUrl = "";

        try {
            var queryStringPrefix = "";
            if (url.indexOf("?") === -1) {
                queryStringPrefix = "?";
            }
            authorizedUrl = environment.api_url + url + queryStringPrefix 
                + 'access_token=' + this.getCurrentUserToken();
        } catch (e) {}
               
        return authorizedUrl;
    }  
    setTableSelection(tables,tablenames){
        try {
            localStorage.setItem('selectedTable',JSON.stringify(tables));   
            localStorage.setItem('selectedtablenames',JSON.stringify(tablenames));   
        } catch (e) { }  
    } 
    getTableSelection(){
        try {
            let selectedTable = JSON.parse(localStorage.getItem('selectedTable'));
            if (selectedTable !== null) {
                return selectedTable;
            } else {
                return [];
            }
        } catch (e) {
            return [];
        }
    }
    getTablenames(){
        try {
            let selectedtablenames = JSON.parse(localStorage.getItem('selectedtablenames'));
            if (selectedtablenames !== null) {
                return selectedtablenames;
            } else {
                return [];
            }
        } catch (e) {
            return [];
        }
    }
    setNumSeat(seat){
        try {
            console.log(seat);
            localStorage.setItem('seat',JSON.stringify(seat));   
        } catch (e) { }  
    } 
    getNumSeat(){
        try {
            let selectedTable = JSON.parse(localStorage.getItem('seat'));
            if (selectedTable !== null) {
                return selectedTable;
            } else {
                return '';
            }
        } catch (e) {
            return '';
        }
    }

    setsplitbillvalues(split_bill_save){
        try {
            localStorage.setItem('split_bill_save',JSON.stringify(split_bill_save)); 
        } catch (e) { }
    }
    getsplitbillvalues(){
        try {
            let split_bill_save = JSON.parse(localStorage.getItem('split_bill_save'));
            if (split_bill_save !== null) {
                return split_bill_save;
            } else {
                return [];
            }
        } catch (e) {
            return [];
        }
    }
}