import { Injectable } from '@angular/core';
import { HttpService, UtilityService } from '../';

@Injectable({
  providedIn: 'root'
})
export class WaitinglistManageService {

  constructor(
    private utilityService: UtilityService,
    private httpService: HttpService) { }

  tableWaitingDetailsList(floor_id){
      return this.httpService.postJson('table-waiting-details-list',{'floor_id':floor_id});
  }
  
  tableWaitingDetailsCancel(data){
    return this.httpService.postJson('table-waiting-details-cancel',data);
  }

  tableWaitingDetailsSave(data){
    return this.httpService.postJson('table-waiting-details-save', data);
  }

  
}
