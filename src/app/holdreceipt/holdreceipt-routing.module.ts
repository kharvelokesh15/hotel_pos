import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HoldreceiptlistComponent } from './holdreceiptlist/holdreceiptlist.component'


const routes: Routes = [
  { path: 'holdreceiptlist', component: HoldreceiptlistComponent, pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HoldreceiptRoutingModule { }
