import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { HoldreceiptRoutingModule } from './holdreceipt-routing.module';
import { HoldreceiptlistComponent } from './holdreceiptlist/holdreceiptlist.component';


@NgModule({
  declarations: [HoldreceiptlistComponent],
  imports: [
    CommonModule,
    HeaderModule,
    HoldreceiptRoutingModule
  ]
})
export class HoldreceiptModule { }
