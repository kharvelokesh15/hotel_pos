import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
@Component({
  selector: 'app-holdreceiptlist',
  templateUrl: './holdreceiptlist.component.html',
  styleUrls: ['./holdreceiptlist.component.css']
})
export class HoldreceiptlistComponent implements OnInit {
  public holdreceiptData;
  isholdreceiptlist: boolean = false;
  checkAllholdreceiptlist: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('HOLD ITEM');
    this.holdreceiptData = [
      {
        holdreceiptNo: '120/02', id: 1, selected: true, holdreceiptfloor: '1 Floor', holdreceipttable: 'T1/David', holdreceiptitemname: 'Chicken 65', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },
      {
        holdreceiptNo: '120/25', id: 2, selected: false, holdreceiptfloor: '2 Floor', holdreceipttable: 'T3/Steve', holdreceiptitemname: 'Grill Chicken', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },

      {
        holdreceiptNo: '128/23', id: 3, selected: false, holdreceiptfloor: '3 Floor', holdreceipttable: 'T3/Albert', holdreceiptitemname: 'Chicken Tandoori', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },
      {
        holdreceiptNo: '121/24', id: 4, selected: true, holdreceiptfloor: '4 Floor', holdreceipttable: 'T5/Robert', holdreceiptitemname: 'Chicken 65', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },
      {
        holdreceiptNo: '122/27', id: 5, selected: false, holdreceiptfloor: '5 Floor', holdreceipttable: 'T6/Jerry', holdreceiptitemname: 'Chicken Fry', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },

      {
        holdreceiptNo: '147/26', id: 6, selected: false, holdreceiptfloor: '6 Floor', holdreceipttable: 'T7/Steve', holdreceiptitemname: 'Apple Juice', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },
      {
        holdreceiptNo: '152/44', id: 7, selected: false, holdreceiptfloor: '7 Floor', holdreceipttable: 'T7/Hunter', holdreceiptitemname: 'Noodles', holdreceiptordertime: '8:30 pm', holdreceiptDelivertime: '8:40 pm'
      },



    ]
  }

  changeholdreceiptlist(event) {
    if (event.target.name == 'holdreceiptData') {
      this.isholdreceiptlist = true
    }

    if (this.isholdreceiptlist && this.checkAllholdreceiptlist) {
      event.target.checked = true
    }
  }

  allholdreceiptlist(event) {
    const checked = event.target.checked;
    this.holdreceiptData.forEach(item => item.selected = checked);
  }

}
