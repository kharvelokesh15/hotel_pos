import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoldreceiptlistComponent } from './holdreceiptlist.component';

describe('HoldreceiptlistComponent', () => {
  let component: HoldreceiptlistComponent;
  let fixture: ComponentFixture<HoldreceiptlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoldreceiptlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoldreceiptlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
