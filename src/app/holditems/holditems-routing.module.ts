import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolditemlistComponent } from './holditemlist/holditemlist.component';

const routes: Routes = [
  { path: 'holdItemlist', component: HolditemlistComponent, pathMatch: 'full' },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HolditemsRoutingModule { }
