import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { HolditemsRoutingModule } from './holditems-routing.module';
import { HolditemlistComponent } from './holditemlist/holditemlist.component';


@NgModule({
  declarations: [HolditemlistComponent],
  imports: [
    CommonModule,
    HeaderModule,
    HolditemsRoutingModule
  ]
})
export class HolditemsModule { }
