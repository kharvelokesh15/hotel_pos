import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolditemlistComponent } from './holditemlist.component';

describe('HolditemlistComponent', () => {
  let component: HolditemlistComponent;
  let fixture: ComponentFixture<HolditemlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolditemlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolditemlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
