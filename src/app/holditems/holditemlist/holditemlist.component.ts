import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { KOTManageService } from './../../@service/kot';

@Component({
  selector: 'app-holditemlist',
  templateUrl: './holditemlist.component.html',
  styleUrls: ['./holditemlist.component.css']
})
export class HolditemlistComponent implements OnInit {
  public holdItemData;
  isholditemlistCol: any = [];
  selectedIds : any =[];
  checkAllholditemlist: boolean = false;
  kot_menu_status:any = 4;
  constructor(public _router: Router, private headerService: HeaderService,private kot
    : KOTManageService) { }

  ngOnInit() {
    this.headerService.setTitle('HOLD ITEM');

    this.kot.KotMenuDetails({"waiter_id":"","customer_id":"","kot_type":"","menu_status":this.kot_menu_status,"kot_date":""}).subscribe(res => {
      this.holdItemData = res;
      
    });

    this.selectedIds = new Array(this.holdItemData.length); 
    for (let index = 0; index < this.holdItemData.length; index++) {
      this.selectedIds[index] = 0;
    }

  }

  changeholdItemList(event) {
    let myNumber = Number(event.target.value);

    if(event.target.checked == true){
        this.isholditemlistCol.push(myNumber);
      
    } else{
       let removeIndex = this.isholditemlistCol.findIndex(itm => itm == myNumber);

       if(removeIndex !== -1)
         this.isholditemlistCol.splice(removeIndex,1);
    }

    if(this.isholditemlistCol.length == this.holdItemData.length){
      if(this.isholditemlistCol.length == 0){
        this.checkAllholditemlist = false
      } else{
        this.checkAllholditemlist = true
      }
    } else{
      if(this.isholditemlistCol.length == 0){
        this.checkAllholditemlist = false
      } else{
        this.checkAllholditemlist = false
      }
    }
  
  }

  allholdItemList(event_select) {

    this.isholditemlistCol =[];

    if( event_select == true){
      for (let index = 0; index < this.holdItemData.length; index++) {
        this.selectedIds[index] = 1;
        this.isholditemlistCol.push(this.holdItemData[index].id);
        this.checkAllholditemlist = true
      }

    } else{
      for (let index = 0; index < this.holdItemData.length; index++) {
        this.selectedIds[index] = 0;
          this.isholditemlistCol =[];
          this.checkAllholditemlist = false;
      }
    }
  }
  
  updateKot(){
      this.kot.KotPosMultiUpdate({"kot_id":this.isholditemlistCol,"type":'update'}).subscribe(res => {
    
        this.kot.KotMenuDetails({"waiter_id":"","customer_id":"","kot_type":"","menu_status":this.kot_menu_status,"kot_date":""}).subscribe(res => {
          this.holdItemData = res;
          this.checkAllholditemlist = false;
        });

      });
  }

  cancelKot(){
    this.kot.KotPosMultiUpdate({"kot_id":this.isholditemlistCol,"type":'cancel'}).subscribe(res => {
  
      this.kot.KotMenuDetails({"waiter_id":"","customer_id":"","kot_type":"","menu_status":this.kot_menu_status,"kot_date":""}).subscribe(res => {
        this.holdItemData = res;
        this.checkAllholditemlist = false;
      });

    });
}
  

}
