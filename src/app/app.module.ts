
// modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { NgxElectronModule } from 'ngx-electron';
import { ModalModule } from 'ngx-bootstrap/modal';

// components
import { AppComponent } from './app.component';

import {
  AuthGuard,   UsersGuard, 
  VendorGuard
} from './@gaurd';
import { AuthenticationService, HttpService, UtilityService, BaseService } from './@service';
import { TableManageService } from './@service/table';
import { CustomerManageService } from './@service/customer';
import { BillingService } from './@service/billing/billing.service';
import { DeliveryManageService } from './@service/delivery/';
import { DriverManageService } from './@service/driver';
import { MenuManageService } from './@service/menu';
import { StaffManageService } from './@service/staff';
import { WaitinglistManageService } from './@service/waitinglist';
import { CartManageService, InvoieManageService, KOTManageService,SalesManageService } from './@service/kot';
import { WINDOW_PROVIDERS } from './@providers/window.provider';
import { JwtInterceptor, ErrorInterceptor } from './@helpers';

//Thirdparty

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
// import { MatKeyboardModule } from '@ngx-material-keyboard/core';
// import { MatKeyboardModule } from 'angular-onscreen-material-keyboard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,

    // Material modules
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    ToastrModule.forRoot({
      closeButton: true,
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      maxOpened: 0,
    }),
    ModalModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    NgProgressModule,
    NgProgressHttpModule,
    NgxElectronModule
  ],
  providers: [
    AuthGuard,   UsersGuard, 
    VendorGuard,BaseService,
    AuthenticationService, HttpService, UtilityService,
    TableManageService, CustomerManageService , BillingService, DeliveryManageService,
     DriverManageService , MenuManageService ,StaffManageService,WaitinglistManageService ,
      CartManageService, InvoieManageService, KOTManageService,SalesManageService ,
    WINDOW_PROVIDERS,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
