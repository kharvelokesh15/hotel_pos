import { Component } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { BaseService, UtilityService } from './@service';
import { HeaderService } from './header.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pos';

  constructor(private _electronService: ElectronService,
    private baseService: BaseService,private headerService: HeaderService, private utilityService: UtilityService) {

      this.baseService.Branchinfo().subscribe(res=>{
        console.log(res.org_logo_path); 
        this.utilityService.setOrg(res.org);
        this.utilityService.setOrgInformation(res.org);
        this.utilityService.setLogoPath(res.org_logo_path)
        this.headerService.setRestarunt(res.org.org_name)
        this.utilityService.setBranch(res.branch);
        this.headerService.setBranch(res.branch.branch_name_eng);;
      })

     }

    public playPingPong() {
        let pong: string = this._electronService
            .ipcRenderer.sendSync('ping');
        console.log(pong);
    }

    public beep() {
        this._electronService.shell.beep();
    } 
}
