import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallcenterlistComponent } from './callcenterlist/callcenterlist.component'

const routes: Routes = [
  { path: 'callcenterorderlist', component: CallcenterlistComponent, pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallcenterorderListRoutingModule { }
