import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import {IMyOptions} from 'mydatepicker'
//services
import { HeaderService } from './../../header.service'
@Component({
  selector: 'app-callcenterlist',
  templateUrl: './callcenterlist.component.html',
  styleUrls: ['./callcenterlist.component.css']
})
export class CallcenterlistComponent implements OnInit {
  public callcenterorderData;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public CallCenterListDate; 
  iscallcenterorderlist: boolean = false;
  checkAllcallcenterorderlist: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('CALL CENTER');
    this.callcenterorderData = [
      {
        callcenterorderNo: '122', id: 1, selected: true, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Steve Smith', callcenterorderphoneno: '9856325721', callcenterordervalue: '$1250 ', callcenterorderstatus: 'Ready'
      },
      {
        callcenterorderNo: '123', id: 2, selected: false, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Albert Jeo', callcenterorderphoneno: '9632585658', callcenterordervalue: '$1250', callcenterorderstatus: 'Delivered'
      },

      {
        callcenterorderNo: '124', id: 3, selected: false, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Johny Sin', callcenterorderphoneno: '9758412587', callcenterordervalue: '$1250', callcenterorderstatus: 'Delivered'
      },

      {
        callcenterorderNo: '125', id: 4, selected: true, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Daniel Rediff', callcenterorderphoneno: '9856325721', callcenterordervalue: '$1250 ', callcenterorderstatus: 'Ready'
      },

      {
        callcenterorderNo: '126', id: 5, selected: false, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Robert Dany', callcenterorderphoneno: '9758412589', callcenterordervalue: '$1250', callcenterorderstatus: 'Taken for delivery'
      },

      {
        callcenterorderNo: '127', id: 6, selected: false, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Warner Sha ', callcenterorderphoneno: '9758412584', callcenterordervalue: '$1250', callcenterorderstatus: 'Taken for delivery'
      },

      {
        callcenterorderNo: '128', id: 7, selected: false, callcenterordertime: '08:30 pm', callcenterordertimeelapsed: '00:30:56', callcenterorderguestname: 'Delrio white', callcenterorderphoneno: '9758412581', callcenterordervalue: '$1250', callcenterorderstatus: 'Taken for delivery'
      }

    ]
  }

  changecallcenterorderlist(event) {
    if (event.target.name == 'callcenterorderData') {
      this.iscallcenterorderlist = true
    }

    if (this.iscallcenterorderlist && this.checkAllcallcenterorderlist) {
      event.target.checked = true
    }
  }

  allcallcenterorderlist(event) {
    const checked = event.target.checked;
    this.callcenterorderData.forEach(item => item.selected = checked);
  }


}
