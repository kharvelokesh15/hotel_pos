import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from '../header/header.module'
import { MyDatePickerModule } from 'mydatepicker';
import { CallcenterorderListRoutingModule } from './callcenterorder-list-routing.module';
import { CallcenterlistComponent } from './callcenterlist/callcenterlist.component';


@NgModule({
  declarations: [CallcenterlistComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    CallcenterorderListRoutingModule,
    ModalModule.forRoot()
  ]
})
export class CallcenterorderListModule { }
