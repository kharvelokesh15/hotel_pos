import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewtriplistComponent } from './newtriplist/newtriplist.component';
import { DeliverylistComponent } from './deliverylist/deliverylist.component';


const routes: Routes = [
  { path: 'newtriplist', component: NewtriplistComponent, pathMatch: 'full' },
  { path: 'deliverylist', component: DeliverylistComponent, pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewtripRoutingModule { }
