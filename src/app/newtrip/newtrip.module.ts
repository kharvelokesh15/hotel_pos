import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { NewtripRoutingModule } from './newtrip-routing.module';
import { NewtriplistComponent } from './newtriplist/newtriplist.component';
import { DeliverylistComponent } from './deliverylist/deliverylist.component';


@NgModule({
  declarations: [NewtriplistComponent,DeliverylistComponent],
  imports: [
    FormsModule,
    CommonModule,
    HeaderModule,
    NewtripRoutingModule
  ]
})
export class NewtripModule { }
