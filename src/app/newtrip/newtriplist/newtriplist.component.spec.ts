import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewtriplistComponent } from './newtriplist.component';

describe('NewtriplistComponent', () => {
  let component: NewtriplistComponent;
  let fixture: ComponentFixture<NewtriplistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewtriplistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewtriplistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
