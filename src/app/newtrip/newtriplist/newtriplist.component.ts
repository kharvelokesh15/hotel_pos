import { Component, OnInit,TemplateRef,ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
//services
import { HeaderService } from './../../header.service';
import { DriverManageService } from './../../@service/driver';
import { DeliveryManageService } from './../../@service/delivery';
import { InvoieManageService } from './../../@service/kot';

@Component({
  selector: 'app-newtriplist',
  templateUrl: './newtriplist.component.html',
  styleUrls: ['./newtriplist.component.css']
})
export class NewtriplistComponent implements OnInit {
  @ViewChild('templatetablefull', {static: true}) templatetablefull: TemplateRef<any>;

  newtripData:any=[];
  modalRef: BsModalRef;
  public tripdata;
  public tripvaluedata;
  public selectedIds = [];
  public selectedCustomerIds = [];
  public selectedCustomerAddresses = [];
  selectedDriverId:any='';
  driver_list:any;
  delivery_list:any;
  invoice_list:any =[];
  invoice_status: any = 1;
  invoice_date: any;
  isnewtriplist: boolean = false;
  checkAllnewtriplist: boolean = false;
  count = 0;
   driverSave :any = {
      "first_name": "",
      "last_name": "",
      "gender": "",
      "email": "",
      "phone": "",
      "alt_phone": "",
      "driving_license":"",
      "primary_address": "",
      'status':"1"
  };
  constructor(public _router: Router,public route: ActivatedRoute, private headerService: HeaderService,private driver
    : DriverManageService,private delivery : DeliveryManageService,private invoice : InvoieManageService,private modalService: BsModalService,) { }

  ngOnInit() {
    this.headerService.setTitle('NEW TRIP');
    // this._entityId = this.route.snapshot.paramMap.get('id');
    this.driver.driverList ().subscribe(res => {
      this.driver_list = res;
      console.log('driver_list', this.driver_list);
    });

    this.delivery.deliveryList({driver:this.selectedDriverId}).subscribe(res => {
      this.delivery_list = res;
      console.log('delivery_list', this.delivery_list);
    });

    let data = {
      date:this.invoice_date,
      status:this.invoice_status,
      sales_type: 'delivery',
      is_assign: "1"
    };
    this.invoice.invoiceList(data).subscribe(res => {
      if(res.status != 'error'){
        this.invoice_list = res;
        this.newtripData = this.invoice_list;
        this.count = 1;
      } else{
        this.count = 0;
      }
    });
    
  }

  openModalTableFullTemplate(templatetablefull: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(  
      templatetablefull,{animated: true, class:"popup-min", backdrop: 'static',keyboard:false }  
      
    );  
  }
  homeRedirect() {
    this.modalRef.hide();
    this._router.navigate(['/newtrip/newtriplist']);
  }
  saveDriver(){
    this.driver.driverCreate({
      "first_name": this.driverSave.first_name,
      "last_name": this.driverSave.last_name,
      "gender": this.driverSave.gender,
      "email": this.driverSave.email,
      "phone": this.driverSave.phone,
      "alt_phone": this.driverSave.phone,
      "driving_license":this.driverSave.driving_license,
      "primary_address": this.driverSave.primary_address,
      'status':1
    }).subscribe(res => {
      this.driver.driverList ().subscribe(res => {
        this.modalRef.hide();
        this.driver_list = res;
        console.log('driver_list', this.driver_list);
      });
  
    });
  }
  // changenewtriplist(event) {
  //   if (event.target.name == 'newtripData') {
  //     this.isnewtriplist = true
  //   }

  //   if (this.isnewtriplist && this.checkAllnewtriplist) {
  //     event.target.checked = true
  //   }
  // }

  changenewtriplist(event) {
    // selected invoice Ids
    var selArr = this.selectedIds;
    var selCustIdArr = this.selectedCustomerIds;
    var selCustAddrArr = this.selectedCustomerAddresses;
    var cdata = this.newtripData.find(x => x.id == event.target.value).customer_id;
    var idata = this.newtripData.find(x => x.id == event.target.value);
    var cust_add = idata.address_no + ' ' + idata.address_street_1 + '' + idata.address_street_2 + ' ' + idata.address_country + ' ' + idata.address_landmark + ' ' + idata.address_zipcode;
    if(event.target.checked == true){
      selArr.push(parseInt(event.target.value));
      selCustIdArr.push(cdata.id);      
      selCustAddrArr.push(cust_add);      
    }else{
        // remove id from array
        const index_id = selArr.indexOf(parseInt(event.target.value));
        if (index_id > -1) {
          selArr.splice(index_id, 1);
        }
        // remove cust id from array
        const index_cust_id = selCustIdArr.indexOf(parseInt(cdata.id));
        if (index_cust_id > -1) {
          selCustIdArr.splice(index_cust_id, 1);
        }
        // remove cust id from array
        const index_cust_addr = selCustAddrArr.indexOf(cust_add);
        if (index_cust_addr > -1) {
          selCustAddrArr.splice(index_cust_addr, 1);
        }
    }
    this.selectedIds = selArr;
    this.selectedCustomerIds = selCustIdArr;
    this.selectedCustomerAddresses = selCustAddrArr;
    
    // Seected Customer Ids
      
      
    console.log('selected Ids', this.selectedIds);
    console.log('selected Customer Ids', this.selectedCustomerIds);
    console.log('selected Customer Adresses', this.selectedCustomerAddresses);
  }

  allnewtriplist(event) {
    const checked = event.target.checked;
    if(checked == true){
      this.newtripData.forEach(item => item.selected = checked);
      this.selectedIds = this.newtripData.map(a => a.id);
      this.selectedCustomerIds = this.newtripData.map(a => a.customer_id.id);
      this.selectedCustomerAddresses = this.newtripData.map(
        a => a.address_no + ' ' + a.address_street_1 + '' + a.address_street_2 + ' ' + a.address_country + ' ' + a.address_landmark + ' ' + a.address_zipcode
        );
    }else{
      this.newtripData.forEach(item => item.selected = false);
      this.selectedIds = [];
      this.selectedCustomerIds = [];
      this.selectedCustomerAddresses = [];
    }
    console.log('All trip datas', this.newtripData);
    console.log('All selected id', this.selectedIds);
    console.log('All customers id', this.selectedCustomerIds);
    console.log('All customers adresses', this.selectedCustomerAddresses);
  }

  assignDriver(event){
     this.selectedDriverId = event.target.value;
  }

  sendOutDriver(){
    if(this.selectedIds.length > 0){
      if(this.selectedDriverId == ''){
        alert('Select driver');
        return;
      }
      this.delivery.deliveryCreate({
        "invoice_id"       : this.selectedIds,
        "driver_id"        : this.selectedDriverId,
        "customer_id"      : this.selectedCustomerIds,
        "delivery_address" : this.selectedCustomerAddresses,
        "order_status"     : "2"
      }).subscribe(res => {
        console.log('delivery create status', res);
        this.invoice.invoiceList({
          date:this.invoice_date,
          status:this.invoice_status,
          sales_type: 'delivery',
          is_assign: "1"
        }).subscribe(res => {
          // this.invoice_list = res;
          // this.newtripData = this.invoice_list;
          if(res.status != 'error'){
            this.invoice_list = res;
            this.newtripData = this.invoice_list;
            this.count =1;
            this._router.navigate(['/newtrip/deliverylist']);
          } else{
            this.count =0;
            this._router.navigate(['/newtrip/deliverylist']);
          }
        });
        this._router.navigate(['/newtrip/deliverylist']);
      });
    }else{
      alert('Select Any one Invoice to assign driver');
    }
  }


}
