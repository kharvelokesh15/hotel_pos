import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { HeaderService } from '../../header.service';
import { DriverManageService } from '../../@service/driver';
import { DeliveryManageService } from '../../@service/delivery';
import { InvoieManageService } from '../../@service/kot';

@Component({
  selector: 'app-deliverylist',
  templateUrl: './deliverylist.component.html',
  styleUrls: ['./deliverylist.component.css']
})
export class DeliverylistComponent implements OnInit {

  newtripData:any =[];
  public tripdata;
  public tripvaluedata;

  // public selectedIds = [];
  public selectedCustomerIds = [];
  public selectedCustomerAddresses = [];

  isDeliverylistCol: any = [];
  totalSelectAmount: any = 0;
  selectedIds : any =[];
  checkAllDeliverylist: boolean = false;

  selectedDriverId:any = '';
  driver_list:any;
  delivery_list:any=[];
  invoice_list:any=[];
  invoice_status: any = 1;
  invoice_date: any;
  isnewtriplist: boolean = false;
  checkAllnewtriplist: boolean = false;
  count =0;
  constructor(public route: ActivatedRoute, private headerService: HeaderService,private driver
    : DriverManageService,private delivery : DeliveryManageService,private invoice : InvoieManageService) { }

  ngOnInit() {
    this.headerService.setTitle('DELIVERED TRIP LIST');
    // this._entityId = this.route.snapshot.paramMap.get('id');
    this.driver.driverList ().subscribe(res => {
      this.driver_list = res;
      console.log('driver_list', this.driver_list);
    });

    this.delivery.deliveryList({driver:this.selectedDriverId}).subscribe(res => {
	  this.delivery_list = res;
	  if(this.delivery_list.message != 'No data found'){
		console.log(this.delivery_list);
		this.selectedIds = new Array(this.delivery_list.length); 
		for (let index = 0; index < this.delivery_list.length; index++) {
			this.selectedIds[index] = 0;
		}
	  } else{
		  this.delivery_list =[];
	  }
      
    });

  }


  UpdateDelivery(id,index) {
    console.log(this.delivery_list);
    let data = {
      invoice_id: this.delivery_list[index].invoice_detail.id,
      driver_id: this.delivery_list[index].driver_details.id,
      customer_id : this.delivery_list[index].customer_id.id,
      delivery_address :this.delivery_list[index].delivery_address,
      order_status: "3"
    }
    this.delivery.deliveryUpdate(id,data).subscribe(res => {
      this.delivery.deliveryList({driver:this.selectedDriverId}).subscribe(res => {
        this.delivery_list = res;
        this.selectedDriverId = '';
        this.isDeliverylistCol =[];
        this.totalSelectAmount = 0;
        this.checkAllDeliverylist = false
        this.selectedIds = new Array(this.delivery_list.length); 
			if(this.delivery_list.message != 'No data found'){
				console.log(this.delivery_list);
				this.selectedIds = new Array(this.delivery_list.length); 
				for (let index = 0; index < this.delivery_list.length; index++) {
					this.selectedIds[index] = 0;
				}
			} else{
				this.delivery_list =[];
			}
      });
    });
  }

  overallUpdate(){
    console.log('delivery_list over all', this.isDeliverylistCol);
	console.log('delivery_list over all', this.totalSelectAmount);
	
	this.delivery.deliveryBulkUpdate({selected_id:this.isDeliverylistCol}).subscribe(res => {
		this.delivery.deliveryList({driver:this.selectedDriverId}).subscribe(res => {
		  this.delivery_list = res;
		  this.selectedDriverId = '';
		  this.isDeliverylistCol =[];
		  this.totalSelectAmount = 0;
		  this.checkAllDeliverylist = false
			if(this.delivery_list.message != 'No data found'){
				console.log(this.delivery_list);
				this.selectedIds = new Array(this.delivery_list.length); 
				for (let index = 0; index < this.delivery_list.length; index++) {
					this.selectedIds[index] = 0;
				}
			} else{
				this.delivery_list =[];
			}
		  console.log('delivery_list', this.delivery_list);
		});
	  })
  }

  assignDriver(event){
     this.selectedDriverId = event.target.value;
     this.delivery.deliveryList({driver:this.selectedDriverId}).subscribe(res => {
      this.delivery_list = res;
	  this.isDeliverylistCol =[];
	  this.totalSelectAmount = 0;
	  this.checkAllDeliverylist = false
	  if(this.delivery_list.message != 'No data found'){
		console.log(this.delivery_list);
		this.selectedIds = new Array(this.delivery_list.length); 
		for (let index = 0; index < this.delivery_list.length; index++) {
			this.selectedIds[index] = 0;
		}
	  } else{
		  this.delivery_list =[];
	  }
      console.log('delivery_list', this.delivery_list);
    });
  }

  changeDeliveryList(event,amount) {
    let myNumber = Number(event.target.value);
	console.log(amount);
    if(event.target.checked == true){
        this.isDeliverylistCol.push(myNumber);
        this.totalSelectAmount = this.totalSelectAmount + parseFloat(amount) ;
        // this.totalSelectAmount.push(myNumber);
      
    } else{
       let removeIndex = this.isDeliverylistCol.findIndex(itm => itm == myNumber);

       if(removeIndex !== -1){
         this.isDeliverylistCol.splice(removeIndex,1);
         this.totalSelectAmount = this.totalSelectAmount - parseFloat(amount);
       }
    }

    if(this.isDeliverylistCol.length == this.delivery_list.length){
      if(this.isDeliverylistCol.length == 0){
        this.checkAllDeliverylist = false
      } else{
        this.checkAllDeliverylist = true
      }
    } else{
      if(this.isDeliverylistCol.length == 0){
        this.checkAllDeliverylist = false
      } else{
        this.checkAllDeliverylist = false
      }
    }
  
  }

  allDeliveryList(event_select) {

    this.isDeliverylistCol =[];
	this.totalSelectAmount = 0;

    if( event_select == true){
      for (let index = 0; index < this.delivery_list.length; index++) {
        this.selectedIds[index] = 1;
        this.totalSelectAmount = this.totalSelectAmount + parseFloat(this.delivery_list[index].invoice_detail.grand_total);
        this.isDeliverylistCol.push(this.delivery_list[index].id);
        this.checkAllDeliverylist = true
      }

    } else{
      for (let index = 0; index < this.delivery_list.length; index++) {
        this.selectedIds[index] = 0;
          this.isDeliverylistCol =[];
          this.totalSelectAmount = 0;
          this.checkAllDeliverylist = false;
      }
    }
  }

}
