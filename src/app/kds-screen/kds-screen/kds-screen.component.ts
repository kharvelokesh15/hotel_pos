import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import {IMyOptions} from 'mydatepicker';
import { IMyOptions, IMyDateModel, IMyDate } from 'mydatepicker';

//services
import { HeaderService } from './../../header.service';
import { KOTManageService } from './../../@service/kot';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-kds-screen',
  templateUrl: './kds-screen.component.html',
  styleUrls: ['./kds-screen.component.css']
})
export class KdsScreenComponent implements OnInit, OnDestroy {

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };

  public kotDetailsData;
  public kotDetailMenu;
  public kotDetailKot;
  public cancelResponse;
  kot_menu_status: any = 1;
  kotIdtList: any = [];
  currency;
  public selDate: IMyDate = { year: 0, month: 0, day: 0 };
  public Date: any = { date: { year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate() } };
  kdsFetchInterval;

  constructor(public _router: Router, private headerService: HeaderService, private kot
    : KOTManageService, private utilityService: UtilityService) {
    let d: Date = new Date();
    this.selDate = { year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate() };
    this.currency = this.utilityService.getCurrency();
  }

  ngOnInit() {
    this.headerService.setTitle("KDS Screen");

    this.kdsFetchInterval = setInterval(() => {
      this.fetchKdsScreenDetails();
    }, 1000 * 5)

  }

  fetchKdsScreenDetails() {
    if (this.Date) {
      this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "kot_type": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
        this.kotDetailsData = res;
      });
    }
  }

  statusChange(status) {
    console.log(this.selDate)
    this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "kot_type": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
      this.kotDetailsData = res;
    });
  }

  onDateChanged(event: IMyDateModel) {
    console.log(this.selDate)
    setTimeout(() => {
      this.fetchKdsScreenDetails();
    }, 100)
    /* this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "kot_type": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
      this.kotDetailsData = res;
    }); */
  }

  cancelKot(kot_menu_id, kot_id, kot_type, customer_cart) {
    if (confirm("Are you sure to Cancel This Item ?")) {
      this.kot.KotPosCancel({ "kot_menu_id": kot_menu_id, "kot_id": kot_id, "kot_type": kot_type, "customer_cart": customer_cart }).subscribe(res => {

        this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
          this.kotDetailsData = res;
        });

      });
    } else {
      return false;
    }

  }

  updateKot(kot_menu_id, kot_id, kot_type, customer_cart) {
    // if(confirm("Are you sure to delete ?")) {
    this.kot.KotPosUpdate({ "kot_menu_id": kot_menu_id, "kot_id": kot_id, "kot_type": kot_type, "customer_cart": customer_cart }).subscribe(res => {

      this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
        this.kotDetailsData = res;
      });

    });
    // } else{
    //   return false;
    // }

  }
  holdKot(kot_menu_id, kot_id, kot_type, customer_cart) {
    this.kot.KotPosHold({ "kot_menu_id": kot_menu_id, "kot_id": kot_id, "kot_type": kot_type, "customer_cart": customer_cart }).subscribe(res => {

      this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
        this.kotDetailsData = res;
      });

    });

  }

  updateAllKot(kot_id) {
    this.kotIdtList.push(kot_id);
    this.kot.KotPosMultiComplete({ "kot_id": this.kotIdtList, "type": 'completed' }).subscribe(res => {

      this.kot.KotMenuDetails({ "waiter_id": "", "customer_id": "", "menu_status": this.kot_menu_status, "kot_date": this.Date.formatted }).subscribe(res => {
        this.kotDetailsData = res;
      });

    });
  }

  ngOnDestroy() {
    if (this.kdsFetchInterval) {
      clearInterval(this.kdsFetchInterval);
      this.kdsFetchInterval = null;
    }
  }

}
