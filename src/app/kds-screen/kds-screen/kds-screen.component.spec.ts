import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KdsScreenComponent } from './kds-screen.component';

describe('KdsScreenComponent', () => {
  let component: KdsScreenComponent;
  let fixture: ComponentFixture<KdsScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KdsScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KdsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
