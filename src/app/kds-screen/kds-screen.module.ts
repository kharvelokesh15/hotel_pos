import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';

import { KdsScreenRoutingModule } from './kds-screen-routing.module';
import { KdsScreenComponent } from './kds-screen/kds-screen.component';


@NgModule({
  declarations: [KdsScreenComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    KdsScreenRoutingModule
  ]
})
export class KdsScreenModule { }
