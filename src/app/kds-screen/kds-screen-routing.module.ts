import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { KdsScreenComponent } from './kds-screen/kds-screen.component';
import { KdsScreenComponent } from './kds-screen/kds-screen.component';


/* const routes: Routes = [
  //  {path:'kdsScreen', component:KdsScreenComponent, pathMatch:'full'}
   { path: 'kdsScreen', component: KdsScreenComponent }
]; */

const routes: Routes = [

  { path: '', component: KdsScreenComponent, pathMatch: 'full' },
   { path: 'kdsScreen', component: KdsScreenComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KdsScreenRoutingModule { }
