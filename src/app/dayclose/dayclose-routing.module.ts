import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DayOpenComponent } from './day-open/day-open.component';
import { DayCloseComponent } from './day-close/day-close.component';
import { BillScreenCloseComponent } from './bill-screen-close/bill-screen-close.component'
import { PrintScreenComponent } from './print-screen/print-screen.component'
import { InvoicePrintScreenComponent } from './invoice-print-screen/invoice-print-screen.component';


const routes: Routes = [
  { path: 'DayOpen', component: DayOpenComponent, pathMatch: 'full' },
  { path: 'DayClose', component: DayCloseComponent, pathMatch: 'full' },
  { path: 'BillScreenClose', component: BillScreenCloseComponent },
  { path: 'PrintScreen', component: PrintScreenComponent },
  { path: 'InvoicePrintScreen/:id', component: InvoicePrintScreenComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DaycloseRoutingModule { }
