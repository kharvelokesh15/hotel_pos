import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayOpenComponent } from './day-open.component';

describe('DayOpenComponent', () => {
  let component: DayOpenComponent;
  let fixture: ComponentFixture<DayOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
