import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from '../../header.service';
import { UtilityService } from '../../@service';
import { ReportManageService } from '../../@service/report';

@Component({
  selector: 'app-day-open',
  templateUrl: './day-open.component.html',
  styleUrls: ['./day-open.component.css']
})
export class DayOpenComponent implements OnInit {

  // @ViewChildren('passwordRef') passwordRef: any;
  @ViewChildren('openingCard') openingCard: any;
  passwordOne: number | string;
  passwordTwo: number | string;
  passwordThree: number | string;
  passwordFour: number | string;
  currentlyFocused: number = 1;


  public tablesDay: any; 
  public foodList;
  public foodList_popup;
  public counter: number = 2;
  currency;
  ordermenus: string[] = ['Day Open'];
  selectedorder = this.ordermenus[0];
  denomTotal:any=[];
  qtyArray:any=[];
  denomArray:any=[];
  grand_cash:any="";
  grand_card:any = 0;
  total;
  CurentStaff;
  type;
  activeDenomIndex;

  constructor(public _router: Router, private headerService: HeaderService,private report : ReportManageService,private utilityService: UtilityService) { this.currency = this.utilityService.getCurrency();}

  ngOnInit() {
    this.headerService.setTitle('Day Open');
    this.CurentStaff = this.utilityService.getCurrentUser();
    this.qtyArray =  new Array(6);
    this.denomArray =  new Array(6);
    this.denomTotal =  new Array(6);
    this.qtyArray = [0,0,0,0,0,0];
    this.denomArray = [0,0,0,0,0,0];
    this.denomTotal = [0,0,0,0,0,0];
  }

    // changeFocusToNext(key: any, num: number) {
    //   if (key != "deleteContentBackward") {
    //     this.passwordRef.first.nativeElement.children[num].focus();
    //   }
    // }

  updateQty(i,type){

    this.denomTotal[i] = this.qtyArray[i] * this.denomArray[i];
    this.GetTotal();
    
  }
  GetTotal(){
    let sum: number = 0;
    this.denomTotal.forEach(a => sum += parseFloat(a));
    this.grand_cash = sum;
    this.total = sum;
    return sum;

  } 
  calculategrand(){
    if(this.grand_cash == ''){
      this.total = 0 + parseFloat(this.grand_card);
    } else {
      this.total = parseFloat(this.grand_cash) + parseFloat(this.grand_card);
    }
    // this.total = parseFloat(this.grand_cash) + parseFloat(this.grand_card);
  }

  saveDayOpen(){

    let data = {
      "open_by":this.CurentStaff.id,
      "open_cash":this.grand_cash,
      "open_card":this.grand_card,
      "open_box_amount":this.grand_cash,
      "open_denomination":[],
    };
    // this.denomArray.forEach(element => {
      this.denomArray.forEach((value, index) => {
        if(value != 0){
          data.open_denomination.push({
            "denom":value,
            "count":this.qtyArray[index],
          });
        }
    });

    this.report.addOpen(data).subscribe(res=>{
      this.printPageArea('print-ticket')
      this._router.navigate(['/']);
      
    })
  }

  changeFocusToNext(num: number) {
  
      if(this.type == 'openingCard'){
        if(this.grand_card != 0){
          this.grand_card = this.grand_card +''+ num;
        } else{
          this.grand_card = num;
        }
        this.calculategrand()
      }

      if(this.type == 'denomCash'){
        if(this.denomArray[this.activeDenomIndex] != 0){
          this.denomArray[this.activeDenomIndex] = this.denomArray[this.activeDenomIndex] +''+ num;
        } else{
          this.denomArray[this.activeDenomIndex] = num;
        }
        this.updateQty(this.activeDenomIndex,'')
        this.calculategrand()
      }
      if(this.type == 'denomQty'){
        if(this.qtyArray[this.activeDenomIndex] != 0){
          this.qtyArray[this.activeDenomIndex] = this.qtyArray[this.activeDenomIndex] +''+ num;
        } else{
          this.qtyArray[this.activeDenomIndex] = num;
        }
        this.updateQty(this.activeDenomIndex,'')
        this.calculategrand()
      }
  }

  createFocus(type,indexValue){
    if( indexValue > -1){
      if(type == 'denomCash'){
        this.type = type;
        this.activeDenomIndex = indexValue
      }
      if(type == 'denomQty'){
        this.type = type;
        this.activeDenomIndex = indexValue
      }
    } else{
      this.type = type;
    }
  }

  resetAll(){
    if(this.type == 'openingCard'){
      this.grand_card = 0;
      this.calculategrand()
    }
    if(this.type == 'denomCash'){
      this.denomArray[this.activeDenomIndex] = 0;
      this.updateQty(this.activeDenomIndex,'')
      this.calculategrand()
    }
    if(this.type == 'denomQty'){
      this.qtyArray[this.activeDenomIndex] = 0;
      this.updateQty(this.activeDenomIndex,'')
      this.calculategrand()
    }
  }

  pressedKey(input: any) {
    if (input == "clear") {
      if(this.type == 'openingCard'){
        this.grand_card = this.grand_card.slice(0, -1) 
        this.calculategrand()
      } 
      if(this.type == 'denomCash'){
        let denomCash = this.denomArray[this.activeDenomIndex];
        this.denomArray[this.activeDenomIndex] = denomCash.slice(0, -1) ;
        this.updateQty(this.activeDenomIndex,'')
        this.calculategrand()
      }
      if(this.type == 'denomQty'){
        let denomQty = this.qtyArray[this.activeDenomIndex];
        this.qtyArray[this.activeDenomIndex] = denomQty.slice(0, -1) ;
        this.updateQty(this.activeDenomIndex,'')
        this.calculategrand()
      } 
     
    } else {
      this.changeFocusToNext(input);
    }

  }

  printPageArea(areaID){
    var printContent = document.getElementsByClassName(areaID);
    var WinPrint = window.open('', '', 'width=900,height=650');
    
    WinPrint.document.write(`<style>
    .bill_screen_tab{
      width:100%;
        }
        *.print-ticket {
          font-size: 8px;
          font-family: 'Times New Roman';
        text-transform: uppercase;
      }
      .header_wr{
        font-size:25px;
      }
      td,
      th,
      tr,
      table {
        /*border-bottom: 2px dotted black;*/
          border-collapse: collapse;
      }
      table {
        border-bottom: 2px dotted black;
        width:100%;
          border-collapse: collapse;
      }
      
      td.description,
      th.description {
          width: 40%;
          max-width: 40%;
      }
      
      td.quantity,
      th.quantity {
          width: 15%;
          max-width: 15%;
          word-break: break-all;
          text-align: right;
      }
      
      td.price,
      th.price {
          width: 20%;
          max-width: 20%;
          word-break: break-all;
        text-align: right;
      }
      td.total,
      th.total {
          width: 25%;
          max-width: 25%;
          word-break: break-all;
        text-align: right;
      }
      th.item {
          width: 20%;
          max-width: 20%;
          word-break: break-all;
      }
      th.item-value {
          width: 5%;
          max-width: 5%;
          word-break: break-all;
      }
      th.paid-total {
          width: 25%;
          max-width: 25%;
          word-break: break-all;
      }
      th.paid-total-value {
          width: 15%;
          max-width: 15%;
          word-break: break-all;
      }
      th.balance {
          width: 20%;
          max-width: 20%;
          word-break: break-all;
      }
      th.balance-value {
          width: 15%;
          max-width: 15%;
          word-break: break-all;
      }
      
      .centered {
          text-align: center;
          align-content: center;
      }
      
      .ticket {
          width: 100%;
          max-width: 100%;
      }
      
      img.logo-print {
          background-size: cover;
          width: 120px;
          height: 120px;
          border: 5px solid white;
          padding: 0.25rem;
          border-radius: 50%;
      }
      table.inv_head {
          text-align: left;
      }
      .inv_head th.quantity {
          width: 60%;
      }
      .inv_head th.description {
          width: 40%;
      }
      .list_payment ul{
        list-style:none;
        padding-left:0;
      }
      .item_details_category.align_listWidth ul li label {
        width: 40%;
        display: inline;
        font-weight: normal;
      }
      .align_listWidth ul li span small {
        font-size: 22px;
        color: #888888;
        font-weight: normal;
        float: right;
      }
      p.divider_Point {
        font-size: 50px;
        color: #666666;
        margin-bottom:20px;
        line-height:20px;
      }
      .item_details_category.align_listWidth ul li span {
          width: 60%;
          display: inline;
          margin-right: 0;
      }
      .item_details_category .list_payment ul li span.float_right {
        text-align: right;
      }
      .item_details_category .list_payment ul li span {
        color: #444444;
        font-weight: 600;
        float: right;
        margin-right: 13px;
      }
      .item_details_category .list_payment ul li {
        border: none;
        font-size: 30px;
        color: #A7A7A7;
        line-height: 3rem;
      }
      .sub_tab_guest_profile h6 {
        color: #444444;
        font-size: 28px;
        font-weight: 600;
      }
      ul{
        list-style:none;
        padding-left:0;
      }
      .day-open-text{
        font-size:30px;
      }
      @page { margin: 0; }
    </style>`+printContent[0].innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
  }


}
