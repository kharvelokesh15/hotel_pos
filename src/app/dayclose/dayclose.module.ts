import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker'; 
import { ModalModule } from 'ngx-bootstrap/modal';
import { DaycloseRoutingModule } from './dayclose-routing.module';
import { DayOpenComponent } from './day-open/day-open.component';
import { DayCloseComponent } from './day-close/day-close.component';
import { BillScreenCloseComponent } from './bill-screen-close/bill-screen-close.component';
import { PrintScreenComponent } from './print-screen/print-screen.component';
import { InvoicePrintScreenComponent } from './invoice-print-screen/invoice-print-screen.component';


@NgModule({
  declarations: [DayOpenComponent,DayCloseComponent, BillScreenCloseComponent, PrintScreenComponent, InvoicePrintScreenComponent],
  imports: [
    CommonModule,
    DaycloseRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    ModalModule.forRoot(),
    AmazingTimePickerModule 
  ]
})
export class DaycloseModule { }
