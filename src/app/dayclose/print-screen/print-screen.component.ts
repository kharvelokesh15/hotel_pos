import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-print-screen',
  templateUrl: './print-screen.component.html',
  styleUrls: ['./print-screen.component.css']
})
export class PrintScreenComponent implements OnInit {

  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Close Shift');
  }

}
