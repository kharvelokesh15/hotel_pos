import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePrintScreenComponent } from './invoice-print-screen.component';

describe('InvoicePrintScreenComponent', () => {
  let component: InvoicePrintScreenComponent;
  let fixture: ComponentFixture<InvoicePrintScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePrintScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePrintScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
