import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { HeaderService } from '../../header.service';
import { InvoieManageService } from './../../@service/kot';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-invoice-print-screen',
  templateUrl: './invoice-print-screen.component.html',
  styleUrls: ['./invoice-print-screen.component.css']
})
export class InvoicePrintScreenComponent implements OnInit {
  public _entityId;
  public invoiceDetailsData;
  public invoiceWaiter;
  public invoiceCustomer;
  public kotDetailMenu;
  public kotTableDetails;
  public invoiceOrganisation;
  currency;
  constructor(public route: ActivatedRoute, private headerService: HeaderService,private invoice
    : InvoieManageService,private utilityService: UtilityService) { this.currency = this.utilityService.getCurrency(); }

  ngOnInit() {
    this.headerService.setTitle('Invoice Preview');
    this._entityId = this.route.snapshot.paramMap.get('id');
    this.invoice.invoiceDetailsList(this._entityId).subscribe(res => {
      this.invoiceDetailsData = res;
      this.kotDetailMenu = this.invoiceDetailsData.kot_data;
      this.invoiceWaiter = this.invoiceDetailsData.waiter_id;
      this.invoiceCustomer = this.invoiceDetailsData.customer_id;
      this.invoiceOrganisation = this.invoiceDetailsData.organisation;
      console.log(this.invoiceDetailsData);
    });
    
  }
  printPageArea(areaID){
    var printContent = document.getElementsByClassName(areaID);
    var WinPrint = window.open('', '', 'width=900,height=650');
    
    WinPrint.document.write(`<style>
    *.print-ticket {
      font-size: 8px;
      font-family: 'Times New Roman';
    text-transform: uppercase;
  }
  
  td,
  th,
  tr,
  table {
     /*border-bottom: 2px dotted black;*/
      border-collapse: collapse;
  }
  table {
     border-bottom: 2px dotted black;
     width:100%;
      border-collapse: collapse;
  }
  
  td.description,
  th.description {
      width: 40%;
      max-width: 40%;
  }
  
  td.quantity,
  th.quantity {
      width: 15%;
      max-width: 15%;
      word-break: break-all;
      text-align: right;
  }
  
  td.price,
  th.price {
      width: 20%;
      max-width: 20%;
      word-break: break-all;
    text-align: right;
  }
  td.total,
  th.total {
      width: 25%;
      max-width: 25%;
      word-break: break-all;
    text-align: right;
  }
  th.item {
      width: 20%;
      max-width: 20%;
      word-break: break-all;
  }
  th.item-value {
      width: 5%;
      max-width: 5%;
      word-break: break-all;
  }
  th.paid-total {
      width: 25%;
      max-width: 25%;
      word-break: break-all;
  }
  th.paid-total-value {
      width: 15%;
      max-width: 15%;
      word-break: break-all;
  }
  th.balance {
      width: 20%;
      max-width: 20%;
      word-break: break-all;
  }
  th.balance-value {
      width: 15%;
      max-width: 15%;
      word-break: break-all;
  }
  
  .centered {
      text-align: center;
      align-content: center;
  }
  
  .ticket {
      width: 100%;
      max-width: 100%;
  }
  
  img.logo-print {
      background-size: cover;
      width: 120px;
      height: 120px;
      border: 5px solid white;
      padding: 0.25rem;
      border-radius: 50%;
  }
  table.inv_head {
      text-align: left;
  }
  .inv_head th.quantity {
      width: 60%;
  }
  .inv_head th.description {
      width: 40%;
  }
  @page { margin: 0; }
    </style>`+printContent[0].innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}

}
