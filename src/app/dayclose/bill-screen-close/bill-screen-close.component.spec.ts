import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillScreenCloseComponent } from './bill-screen-close.component';

describe('BillScreenCloseComponent', () => {
  let component: BillScreenCloseComponent;
  let fixture: ComponentFixture<BillScreenCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillScreenCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillScreenCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
