import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { UtilityService } from '../../@service';
import { ReportManageService } from './../../@service/report';

@Component({
  selector: 'app-bill-screen-close',
  templateUrl: './bill-screen-close.component.html',
  styleUrls: ['./bill-screen-close.component.css']
})
export class BillScreenCloseComponent implements OnInit {

  @ViewChildren('passwordRef') passwordRef: any;
  passwordOne: number | string;
  passwordTwo: number | string;
  passwordThree: number | string;
  passwordFour: number | string;
  currentlyFocused: number = 1;


  public tablesDay: any; 
  public foodList;
  public foodList_popup;
  public counter: number = 2;
  currency;
  ordermenus: string[] = ['Close Shift'];
  selectedorder = this.ordermenus[0];
  denomTotal ;
  qtyArray:any=[];
  denomArray:any=[];

  constructor(public _router: Router, private headerService: HeaderService,private report : ReportManageService,private utilityService: UtilityService) { }

  ngOnInit() {
    this.headerService.setTitle('Day close');
    this.currency = this.utilityService.getCurrency();
    this.tablesDay =  new Array(8);
    this.qtyArray =  new Array(8);
    this.denomArray =  new Array(8);
    // this.tablesDay = [
    //   { type: '$100.000', quantity: '0', total: '0000' },
    //   { type: '$50.000', quantity: '0', total: '0000' },
    //   { type: '$20.000', quantity: '0', total: '0000' },
    //   { type: '$10.000', quantity: '0', total: '0000' },
    //   { type: '$5.000', quantity: '0', total: '0000' },
    //   { type: '$1.000', quantity: '0', total: '0000' }
    // ];
  }

  changeFocusToNext(key: any, num: number) {
    if (key != "deleteContentBackward") {
      this.passwordRef.first.nativeElement.children[num].focus();
    }
  }

  updateDenom(value,i){
    this.qtyArray[i] = (this.qtyArray[i] == '') ? 0 : this.qtyArray[i];
    this.denomArray[i] = (this.denomArray[i] == '') ? 0 : value;
    this.denomTotal[i] = this.qtyArray[i]*this.denomArray[i];
  }
  updateQty(value,i){
    this.denomArray[i] = (this.denomArray[i] == '') ? 0 : this.denomArray[i];
    this.qtyArray[i] = (this.qtyArray[i] == '') ? 0 : value;
    this.denomTotal[i] = this.qtyArray[i]*this.denomArray[i];
  }

  pressedKey(input: any) {
    if (input == "clear") {
      this.passwordOne = "";
      this.passwordTwo = "";
      this.passwordThree = "";
      this.passwordFour = "";
      this.currentlyFocused = 1;
    } else {
      switch (this.currentlyFocused) {
        case 1: this.passwordOne = input;
          this.changeFocusToNext("1", 1);
          break;
        case 2: this.passwordTwo = input;
          this.changeFocusToNext("2", 2);
          break;
        case 3: this.passwordThree = input;
          this.changeFocusToNext("3", 3);
          break;
        case 4: this.passwordFour = input;
          this.changeFocusToNext("3", 3);
          break;
      }
    }

  }

}
