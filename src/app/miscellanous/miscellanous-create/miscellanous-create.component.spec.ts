import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellanousCreateComponent } from './miscellanous-create.component';

describe('MiscellanousCreateComponent', () => {
  let component: MiscellanousCreateComponent;
  let fixture: ComponentFixture<MiscellanousCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellanousCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellanousCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
