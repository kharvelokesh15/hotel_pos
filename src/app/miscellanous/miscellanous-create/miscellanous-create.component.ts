import { Component, OnInit } from '@angular/core';
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-miscellanous-create',
  templateUrl: './miscellanous-create.component.html',
  styleUrls: ['./miscellanous-create.component.css']
})
export class MiscellanousCreateComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Miscellaneous SAles');
  }


  createSales=[
    {
      charge:'$100.000',remarks:'',qty:'0000',amount:'0000'
    },
    {
      charge:'$50.000',remarks:'',qty:'0000',amount:'0000'
    },
    {
      charge:'$100.000',remarks:'',qty:'0000',amount:'0000'
    }
]

}
