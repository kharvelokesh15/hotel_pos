import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';

import { MiscellanousRoutingModule } from './miscellanous-routing.module';
import { MiscellanousSalesComponent } from './miscellanous-sales/miscellanous-sales.component';
import { MiscellanousCreateComponent } from './miscellanous-create/miscellanous-create.component';


@NgModule({
  declarations: [MiscellanousSalesComponent, MiscellanousCreateComponent],
  imports: [
    CommonModule,
    MiscellanousRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
  ]
})
export class MiscellanousModule { }
