import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiscellanousSalesComponent } from './miscellanous-sales/miscellanous-sales.component';
import { MiscellanousCreateComponent } from './miscellanous-create/miscellanous-create.component';




const routes: Routes = [
  {path:'MiscellanousSales', component:MiscellanousSalesComponent, pathMatch:'full'},
  {path:'MiscellanousCreate', component:MiscellanousCreateComponent, pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiscellanousRoutingModule { }
