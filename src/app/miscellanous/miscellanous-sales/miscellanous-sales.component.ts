import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-miscellanous-sales',
  templateUrl: './miscellanous-sales.component.html',
  styleUrls: ['./miscellanous-sales.component.css']
})
export class MiscellanousSalesComponent implements OnInit {

  public misSales;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public Date; 
  isTrade: boolean = false;
  checkAllTrades: boolean = false;

  constructor(public _router: Router, private headerService: HeaderService) { }


  ngOnInit() {
    this.headerService.setTitle('Miscellaneous SAles');

    this.misSales = [
      {
        date:'29 Jul 19 ', received:'David',payment:'Cash',recno:'9876542310',amount:'1254',remark:'25 mins',
      },
      {
        date:'29 Jul 19 ', received:'Smith',payment:'Cash',recno:'9874542310',amount:'1254',remark:'25 mins',
      },
      {
        date:'29 Jul 19 ', received:'Richards',payment:'Cash',recno:'845465478',amount:'1254',remark:'30 mins',
      },
      {
        date:'29 Jul 19 ', received:'Bradman',payment:'Cash',recno:'923554528154',amount:'1254',remark:'25 mins',
      },
      {
        date:'29 Jul 19 ', received:'Midwinter',payment:'Cash',recno:'8556652310',amount:'1254',remark:'1 hour 25 mins',
      },
      {
        date:'29 Jul 19 ', received:'James ',payment:'Cash',recno:'96523265988',amount:'1254',remark:'2 hour 30 mins',
      },
      {
        date:'29 Jul 19 ', received:'Nannes',payment:'Cash',recno:'9898265672',amount:'1254',remark:'50 mins',
      }
    ]
  }
 
  }

 


 
