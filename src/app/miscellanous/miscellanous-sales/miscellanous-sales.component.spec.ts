import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellanousSalesComponent } from './miscellanous-sales.component';

describe('MiscellanousSalesComponent', () => {
  let component: MiscellanousSalesComponent;
  let fixture: ComponentFixture<MiscellanousSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellanousSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellanousSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
