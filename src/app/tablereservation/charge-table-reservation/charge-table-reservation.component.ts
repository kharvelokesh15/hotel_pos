import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions, IMyDateModel, IMyDate} from 'mydatepicker';
import { CustomerManageService } from './../../@service/customer';
import { TableManageService } from './../../@service/table';
//services
import { HeaderService } from './../../header.service';


@Component({
  selector: 'app-charge-table-reservation',
  templateUrl: './charge-table-reservation.component.html',
  styleUrls: ['./charge-table-reservation.component.css']
})
export class ChargeTableReservationComponent implements OnInit {

  constructor(public _router: Router, 
    private headerService: HeaderService,
    private tableManageService: TableManageService,
    private customerManageService: CustomerManageService
    ) { }
  
  public customer = {
    "id": "",
    "first_name": "",
    "last_name": "",
    "phone": "",
    "gender": "",
    "email": ""
  };
  public booking_data = {
    "floor_id":"",
    "table_ids":"",
    "occupied_chairs":"",
    "waiter_id":"",
    "customer_id":"",
    "is_booked":"",
    "is_reserved":"1",
    "reserving_date":"",
    "booked_from":"",
    "booked_to":"",
    "reserved_from":"",
    "reserved_to":"",
    "type":2,
    "waiting_id":"",
    "reserve_type": ""
  };
  public floorList;
  public currentUser;
  public tableList;  
  public table_seats_avail;
  public avail_seat;
  public exCustomer: false | boolean;
  public search: any;
  public selDate: IMyDate = {year: 0, month: 0, day: 0};
  public Date: any = { date: {year: (new Date()).getFullYear(), month: (new Date()).getMonth() + 1, day: (new Date()).getDate()} };

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };

  ngOnInit() {
    this.headerService.setTitle('Table Reservation');

    let d: Date = new Date(); 
    this.selDate = {year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate()};
    this.booking_data.reserving_date = this.selDate.year+'/'+this.selDate.month+'/'+this.selDate.day;

    this.tableManageService.floorList().subscribe(res => {
      this.floorList = res;
      //console.log(res);
      this.booking_data.floor_id = this.floorList[0].id;
      this.GetFloor(this.booking_data.floor_id);
      this.tableAvailDetails(this.booking_data.floor_id);
      console.log(this.booking_data.floor_id);
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    })
  }
  
  onFloorSelected(SelectedFloorId){
    this.GetFloor(SelectedFloorId);
  }

  onTableSelected(id){
    this.booking_data.table_ids = id;
  }

  onDateSelected(event: IMyDateModel){
    this.booking_data.reserving_date  = event.formatted;
    // this.booking_data.reserving_date =   this.selDate.year+'/'+this.selDate.month+'/'+this.selDate.day;
    console.log(this.selDate, this.booking_data.reserving_date);
    this.GetFloor(this.booking_data.floor_id);
  }

  GetFloor(SelectedFloorId){
    this.booking_data.floor_id = SelectedFloorId;
    this.tableManageService.tableList(this.booking_data.floor_id).subscribe(res => {
      this.tableList = res;
      console.log(this.tableList);
      this.tableAvailDetails(this.booking_data.floor_id);
    })
  }

  tableAvailDetails(floor_id){
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    this.tableManageService.tableAvailDetails({ "floor_id":floor_id,"current_time":time, "date":this.booking_data.reserving_date }).subscribe(res=>{
      console.log(res);
      this.table_seats_avail= res.table_seats_avail;
      this.avail_seat = res.avail_seat;
    })
  }

  searchCustomers(data){
    this.customerManageService.customerSearch(data.target.value).subscribe(res => {
      // console.log(res);
      if(res){  
        console.log('sgsdgd')
        this.customer = res[0]; 
        this.exCustomer = true;
        console.log('search customer =>', this.customer);
      }else{
        console.log('123456')
        this.exCustomer = false;
        this.search = data.target.value;
        this.resetCustomer();
        // this.resetLocalStorageCustomer();
      }
    })
  }

  resetCustomer(){
    this.customer = {
      "id": "",
      "first_name": "",
      "last_name": "",
      "phone": this.search,
      "gender": "",
      "email": ""
    };
  }

  reserveTable(){
    if(this.exCustomer == false){
      // Add Customer
      this.customerManageService.customerSave({
        "first_name": this.customer.first_name,
        "last_name": this.customer.last_name,
        "gender": this.customer.gender,
        "email": this.customer.email,
        "mobile_number": this.customer.phone,
        "type":1,
        "phone": this.customer.phone,
        "mobile_key": this.customer.phone,
      }).subscribe(res => {
        this.customerManageService.customerSearch(this.search).subscribe(res => {
          this.customer = res[0];
          if(res){
            // Reserve a Table
            this.booking_data.customer_id = this.customer.id;
            this.booking_data.waiter_id = this.currentUser.waiter.id;
            this.booking_data.reserve_type = localStorage.getItem('module');
            var cus = (this.customer.id != '') ? true : false ;
            if(cus == true){
              if(this.checkOccupiedChairs() == true){
                console.log('customer', this.customer);
                console.log('booking_data', this.booking_data);
                this.tableManageService.tableSelectionSave(this.booking_data).subscribe(res => {
                  console.log('booking added',res);
                  this._router.navigate(['tablereservation/ChargeTableReservationList']);
                })
              }
            }
          }
          console.log('new customer_data =>', this.customer)
        });
      });

    } else {
       // Reserve a Table
       this.booking_data.customer_id = this.customer.id;
       this.booking_data.waiter_id = this.currentUser.waiter.id;
       this.booking_data.reserve_type = localStorage.getItem('module');
       var cus = (this.customer.id != '') ? true : false ;
       if(cus == true){
         if(this.checkOccupiedChairs() == true){
           console.log('customer', this.customer);
           console.log('booking_data', this.booking_data);
           this.tableManageService.tableSelectionSave(this.booking_data).subscribe(res => {
             console.log('booking added',res);
             this._router.navigate(['tablereservation/ChargeTableReservationList']);
           })
         }
       }
      console.log('ex customer_data =>', this.customer)
    }
    
    // else{
    //   alert('Customer Details Required');
    // }
  }

  checkOccupiedChairs(){
    return (this.booking_data.occupied_chairs <= this.avail_seat || this.avail_seat == 0) ? true : alert('No Available seats'); ;
  }

}
