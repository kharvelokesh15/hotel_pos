import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeTableReservationComponent } from './charge-table-reservation.component';

describe('ChargeTableReservationComponent', () => {
  let component: ChargeTableReservationComponent;
  let fixture: ComponentFixture<ChargeTableReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeTableReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeTableReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
