import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChargeTableReservationComponent } from './charge-table-reservation/charge-table-reservation.component'
import { ChargeTableReservationListComponent } from './charge-table-reservation-list/charge-table-reservation-list.component'


const routes: Routes = [
  { path: 'ChargeTableReservation', component: ChargeTableReservationComponent },
  { path: 'ChargeTableReservationList', component: ChargeTableReservationListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TablereservationRoutingModule { }
