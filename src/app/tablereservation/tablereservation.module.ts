import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { HeaderModule } from '../header/header.module';
import { FormsModule } from '@angular/forms';
import { TablereservationRoutingModule } from './tablereservation-routing.module';

import { ChargeTableReservationComponent } from './charge-table-reservation/charge-table-reservation.component';
import { ChargeTableReservationListComponent } from './charge-table-reservation-list/charge-table-reservation-list.component';


@NgModule({
  declarations: [ChargeTableReservationComponent,ChargeTableReservationListComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    TablereservationRoutingModule
  ]
})
export class TablereservationModule { }
