import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeTableReservationListComponent } from './charge-table-reservation-list.component';

describe('ChargeTableReservationListComponent', () => {
  let component: ChargeTableReservationListComponent;
  let fixture: ComponentFixture<ChargeTableReservationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeTableReservationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeTableReservationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
