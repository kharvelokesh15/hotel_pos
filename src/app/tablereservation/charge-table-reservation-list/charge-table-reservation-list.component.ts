import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TableManageService } from './../../@service/table';
//services
import { HeaderService } from './../../header.service';


@Component({
  selector: 'app-charge-table-reservation-list',
  templateUrl: './charge-table-reservation-list.component.html',
  styleUrls: ['./charge-table-reservation-list.component.css']
})

export class ChargeTableReservationListComponent implements OnInit {

  public ReservationData;


  constructor(public _router: Router, 
    private headerService: HeaderService,
    private tableManageService: TableManageService
    ) { }

  ngOnInit() {
    this.headerService.setTitle('Table Reservation List');
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var today_date = year + '/' + month + '/' + date;
    // list function 
    this.tableManageService.tableReservationDetailsList({
      "floor_id" : "",
      "date": today_date
    }).subscribe(res => {
      console.log('reservation list => ',res);
      this.ReservationData = res;
    })


    // list function 
 
  } 
 

}
