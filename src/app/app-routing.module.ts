//modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AuthGuard, UsersGuard,
  VendorGuard
} from './@gaurd';
// components
import { environment } from './../environments/environment';

const routes: Routes = [
  {
    path: '',
    ...(environment.kds
      ? { loadChildren: './kds-screen/kds-screen.module#KdsScreenModule' }
      : { loadChildren: './homescreen/homescreen.module#HomescreenModule' }
    )
  },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'homescreen', loadChildren: './homescreen/homescreen.module#HomescreenModule', canActivate: [AuthGuard] },

  { path: 'dinein', loadChildren: './dinein/dinein.module#DineinModule', canActivate: [AuthGuard] },
  { path: 'takeaway', loadChildren: './takeaway/takeaway.module#TakeawayModule', canActivate: [AuthGuard] },
  { path: 'delivery', loadChildren: './delivery/delivery.module#DeliveryModule', canActivate: [AuthGuard] },
  { path: 'nocharge', loadChildren: './nocharge/nocharge.module#NochargeModule', canActivate: [AuthGuard] },
  { path: 'tablereservation', loadChildren: './tablereservation/tablereservation.module#TablereservationModule', canActivate: [AuthGuard] },
  { path: 'callcenter', loadChildren: './callcenter/callcenter.module#CallcenterModule', canActivate: [AuthGuard] },
  { path: 'kot', loadChildren: './kot/kot.module#KotModule', canActivate: [AuthGuard] },
  { path: 'receipt', loadChildren: './receipt/receipt.module#ReceiptModule', canActivate: [AuthGuard] },
  { path: 'holditems', loadChildren: './holditems/holditems.module#HolditemsModule', canActivate: [AuthGuard] },
  { path: 'holdreceipt', loadChildren: './holdreceipt/holdreceipt.module#HoldreceiptModule', canActivate: [AuthGuard] },
  { path: 'callcenterorderlist', loadChildren: './callcenterorder-list/callcenterorder-list.module#CallcenterorderListModule', canActivate: [AuthGuard] },
  { path: 'reservationlist', loadChildren: './reservation-list/reservation-list.module#ReservationListModule', canActivate: [AuthGuard] },
  { path: 'newtrip', loadChildren: './newtrip/newtrip.module#NewtripModule', canActivate: [AuthGuard] },
  { path: 'checkin', loadChildren: './checkin/checkin.module#CheckinModule', canActivate: [AuthGuard] },
  { path: 'status', loadChildren: './status/status.module#StatusModule', canActivate: [AuthGuard] },
  { path: 'report', loadChildren: './report/report.module#ReportModule', canActivate: [AuthGuard] },
  { path: 'dayclose', loadChildren: './dayclose/dayclose.module#DaycloseModule', canActivate: [AuthGuard] },
  { path: 'message', loadChildren: './message/message.module#MessageModule', canActivate: [AuthGuard] },
  { path: 'account-payment', loadChildren: './account-payment/account-payment.module#AccountPaymentModule', canActivate: [AuthGuard] },
  { path: 'waiter', loadChildren: './waiter/waiter.module#WaiterModule', canActivate: [AuthGuard] },
  { path: 'miscellanous', loadChildren: './miscellanous/miscellanous.module#MiscellanousModule', canActivate: [AuthGuard] },
  // { path: 'kdsScreen', loadChildren: './kds-screen/kds-screen.module#KdsScreenModule', canActivate: [AuthGuard]  },
  { path: 'kdsScreen', loadChildren: './kds-screen/kds-screen.module#KdsScreenModule' },
  { path: 'clearkot', loadChildren: './clearkot/clearkot.module#ClearkotModule', canActivate: [AuthGuard] },
  { path: 'receiptlisting', loadChildren: './receiptlisting/receiptlisting.module#ReceiptlistingModule', canActivate: [AuthGuard] },
  { path: 'invoicelisting', loadChildren: './invoicelisting/invoicelisting.module#InvoicelistingModule', canActivate: [AuthGuard] }






];

@NgModule({
  // imports: [RouterModule.forRoot(routes,{ useHash: true })],
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
