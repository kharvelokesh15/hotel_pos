import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomescreenRoutingModule } from './homescreen-routing.module';
import { HomeComponent } from './home/home.component';
//module
import { HeaderModule } from './../header/header.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HeaderModule,
    HomescreenRoutingModule
  ],

})
export class HomescreenModule { }
