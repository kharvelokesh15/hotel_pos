import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//services
import { ElectronService } from 'ngx-electron';
import { HeaderService } from './../../header.service'
import { UtilityService } from '../../@service';
import { ToastrService } from 'ngx-toastr';
import Keyboard from "simple-keyboard";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    "../../../../node_modules/simple-keyboard/build/css/index.css",    
    './home.component.css']
})
export class HomeComponent implements OnInit {
  public SelectedModule;
  public  print_options = { 
    silent: false, 
    printBackground: true, 
    color: false, 
    margin: { 
        marginType: 'printableArea'
    }, 
    landscape: false, 
    pagesPerSheet: 1, 
    collate: false, 
    copies: 1, 
    header: 'Header of the Page', 
    footer: 'Footer of the Page'
} 
  constructor(private headerService: HeaderService,
    private utilityService: UtilityService,
    private toastr: ToastrService,
    private _electronService: ElectronService,
    private _route: Router) { }

    public playPingPong() {
      let pong: string = this._electronService
          .ipcRenderer.sendSync('ping');
      console.log(pong);
  }

  public Print() {
    // let pong: string = this._electronService
        // .ipcRenderer.webContents
  }

  ngOnInit() {
   // this.playPingPong();
    // this.toastr.success('Hello world!', 'Toastr fun!');
    this.headerService.setTitle('Home Screen');
    this.utilityService.clearDinine();
    this.SelectedModule = this.utilityService.GetModule();
  }
  GotoModulePage(Module){
    if(this.SelectedModule != Module){
      this.SelectedModule = Module;
      this.utilityService.SetModule(Module);
    }
    this._route.navigate([this.utilityService.GetModuleInfo(Module).Route]);
  }

  value = "";
  keyboard: Keyboard;

  ngAfterViewInit() {
    this.keyboard = new Keyboard({
      onChange: input => this.onChange(input),
      onKeyPress: button => this.onKeyPress(button)
    });
  }

  onChange = (input: string) => {
    this.value = input;
    console.log("Input changed", input);
  };

  onKeyPress = (button: string) => {
    console.log("Button pressed", button);

    /**
     * If you want to handle the shift and caps lock buttons
     */
    if (button === "{shift}" || button === "{lock}") this.handleShift();
  };

  onInputChange = (event: any) => {
    this.keyboard.setInput(event.target.value);
  };

  handleShift = () => {
    let currentLayout = this.keyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";

    this.keyboard.setOptions({
      layoutName: shiftToggle
    });
  };

}
