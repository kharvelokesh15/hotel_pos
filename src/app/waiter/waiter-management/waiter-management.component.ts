import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-waiter-management',
  templateUrl: './waiter-management.component.html',
  styleUrls: ['./waiter-management.component.css']
})
export class WaiterManagementComponent implements OnInit {

  public holdItemData;
  isholditemlist: boolean = false;
  checkAllholditemlist: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Waiter Management');
    this.holdItemData = [
      {
        holdItemKOTNO: 'Table 1', id: 1, selected: true, holdItemordersource: 'Waiter 1', holdItemfloor: 'Chicken 65'
      },
      {
        holdItemKOTNO: 'Table 2', id: 1, selected: false, holdItemordersource: 'Waiter 2', holdItemfloor: 'Grill Chicken'
      },
      {
        holdItemKOTNO: 'Table 3', id: 1, selected: false, holdItemordersource: 'Waiter 3', holdItemfloor: 'Chicken tandoori'
      },
      {
        holdItemKOTNO: 'Table 4', id: 1, selected: true, holdItemordersource: 'Waiter 4', holdItemfloor: 'Chicken 65'
      },
      {
        holdItemKOTNO: 'Table 5', id: 1, selected: false, holdItemordersource: 'Waiter 5', holdItemfloor: 'Chicken Fry'
      }
       

    ]
  }

  changeholdItemList(event) {
    if (event.target.name == 'holdItemData') {
      this.isholditemlist = true
    }

    if (this.isholditemlist && this.checkAllholditemlist) {
      event.target.checked = true
    }
  }

  allholdItemList(event) {
    const checked = event.target.checked;
    this.holdItemData.forEach(item => item.selected = checked);
  }

}
