import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaiterManagementComponent } from './waiter-management.component';

describe('WaiterManagementComponent', () => {
  let component: WaiterManagementComponent;
  let fixture: ComponentFixture<WaiterManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaiterManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaiterManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
