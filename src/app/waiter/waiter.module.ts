import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { WaiterRoutingModule } from './waiter-routing.module';
import { WaiterManagementComponent } from './waiter-management/waiter-management.component';


@NgModule({
  declarations: [WaiterManagementComponent],
  imports: [
    CommonModule,
    HeaderModule,
    WaiterRoutingModule
  ]
})
export class WaiterModule { }
