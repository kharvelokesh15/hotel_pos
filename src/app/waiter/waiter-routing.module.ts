import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WaiterManagementComponent} from './waiter-management/waiter-management.component';



const routes: Routes = [
  {path:'WaiterManagement',component:WaiterManagementComponent,pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WaiterRoutingModule { }
