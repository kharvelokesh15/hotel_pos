import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-guest-listing-history',
  templateUrl: './guest-listing-history.component.html',
  styleUrls: ['./guest-listing-history.component.css']
})
export class GuestListingHistoryComponent implements OnInit {
  public mergereceiptdata;
  public history;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public OrderHistoryDate;
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Cell Center ');
    this.mergereceiptdata = [
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '125',   mergeRecpno: '2014589', mergeName: 'Steve Smith', mergeType: 'Dine IN', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '125',   mergeRecpno: '2014589', mergeName: 'Albert Jeo', mergeType: 'Take Away', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '163',   mergeRecpno: '2014589', mergeName: 'Johny Sin', mergeType: 'Dine IN', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '135',   mergeRecpno: '2014589', mergeName: 'Daniel Rediff', mergeType: 'Take Away', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '120',   mergeRecpno: '2014589', mergeName: 'Robert Dany', mergeType: 'Dine IN', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '128',   mergeRecpno: '2014589', mergeName: 'Warner Sha', mergeType: 'Take Away', mergeValue: '$1250'},
      {mergedate: '25-Jul-2019',mergetime:'8:55 pm',  mergekot: '147',   mergeRecpno: '2014589', mergeName: 'Delrio white', mergeType: 'Take Away', mergeValue: '$1250'},
    ];

    this.history = [
      {no:'1',descipt:'Chicken Soup',qty:'5',total:'$200.00'},
      {no:'2',descipt:'Chicken Chilly',qty:'2',total:'$00.00'},
      {no:'3',descipt:'Grill Chicken',qty:'7',total:'$400.00'},
      {no:'4',descipt:'Mutton Biryani',qty:'8',total:'$450.00'},
      {no:'5',descipt:'Chicken Soup',qty:'4',total:'$150.00'},
  ]

  document.addEventListener("click", function(){
                  
    $(".customer_list tr").click(function() {
     
       $(".customer_list tr").removeClass("highlight");
        
               $(this).addClass("highlight");
       });
  });
  }

}
