import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestListingHistoryComponent } from './guest-listing-history.component';

describe('GuestListingHistoryComponent', () => {
  let component: GuestListingHistoryComponent;
  let fixture: ComponentFixture<GuestListingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestListingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
