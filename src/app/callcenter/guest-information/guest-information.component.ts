import { Component, OnInit } from '@angular/core';
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-guest-information',
  templateUrl: './guest-information.component.html',
  styleUrls: ['./guest-information.component.css']
})
export class GuestInformationComponent implements OnInit {
  ordermenus: string[] = ['Address'];
  selectedorder = this.ordermenus[0];

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Cell Center ');
  }

}
