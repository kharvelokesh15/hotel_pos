import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from '../../header.service';
import { CustomerManageService } from './../../@service/customer';
import { BranchManageService } from './../../@service/branch';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-select-branch',
  templateUrl: './select-branch.component.html',
  styleUrls: ['./select-branch.component.css']
})
export class SelectBranchComponent implements OnInit {

  public branch: any;
  public branches: any;
  public isCallCenterReservation: false | boolean ;
  public SelectedModule;
  ordermenus: string[] = ['Order Transfer','Guest List'];
  selectedorder = this.ordermenus[1];
  selectedBranch: any;

  constructor(public _router: Router, 
    private headerService: HeaderService, 
    private BranchManageService: BranchManageService,
    private utilityService: UtilityService,
    private _route: Router
    ) { }

  ngOnInit() {
    this.BranchManageService.branchList().subscribe(res => {
        this.branches = res; 
    })
    this.utilityService.setSelectedBranch({});
    this.SelectedModule = this.utilityService.GetModule();
  }
  GotoModulePage(Module){
    if(this.SelectedModule != Module){
      this.SelectedModule = Module;
      this.utilityService.SetModule(Module);
    }
    this._route.navigate([this.utilityService.GetModuleInfo(Module).Route]);
  }

  onBranchSelected(selectedBranch){
    let res = this.branches.filter(obj => {
      return obj.id == selectedBranch;
    })
    this.selectedBranch = res[0];
    console.log(this.selectedBranch)
  }

  choosetype(type){
    if(this.branch != ''){
      this.utilityService.setSelectedBranch(this.selectedBranch);
      if(type == 'type1'){
        this._router.navigate(['/dinein/DineinSelection']);
      }
      if(type == 'type2'){
        this._router.navigate(['tablereservation/ChargeTableReservation']);
      }
    }else{
      alert('Select any branch');
    }
  }



  
  // chargesprofiles = [
  //   {chargeCode : '1',chargeName : 'John Smith',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '9876543210'},
  //   {chargeCode : '2',chargeName : 'Andrew Sam',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '8765432109'},
  //   {chargeCode : '3',chargeName : 'Robin Kellerman',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '7654321098'},
  //   {chargeCode : '4',chargeName : 'Mohammed Imran',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '6543210987'},
  //   {chargeCode : '5',chargeName : 'Robin Kellerman',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '7654321098'},

  // ];

  
  // chargesprofile = [
  //   {chargeValue : 'Staff 1'},
  //   {chargeValue : 'Staff 2'},
  //   {chargeValue : 'Staff 3'},
  //   {chargeValue : 'Staff 4'},
  //   {chargeValue : 'Staff 5'}
  // ];

}
