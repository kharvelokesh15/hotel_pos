import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GuestListingComponent} from './guest-listing/guest-listing.component';
import {GuestListingHistoryComponent} from './guest-listing-history/guest-listing-history.component';
import {GuestMenuScreenComponent} from './guest-menu-screen/guest-menu-screen.component';
import {GuesAssignKitchenComponent} from './gues-assign-kitchen/gues-assign-kitchen.component';
import {PhoneNumberComponent} from './phone-number/phone-number.component';
import {GuestInformationComponent} from './guest-information/guest-information.component';
import {SelectBranchComponent} from './select-branch/select-branch.component';


const routes: Routes = [

  { path: 'GuestListing', component: GuestListingComponent, pathMatch: 'full' },
  { path: 'GuestListingHistory', component: GuestListingHistoryComponent},
  { path: 'GuestMenuScreen', component: GuestMenuScreenComponent},
  { path: 'GuestAssignKitchen', component: GuesAssignKitchenComponent},
  { path: 'PhoneNumber', component: PhoneNumberComponent},
  { path: 'GuestInformation', component: GuestInformationComponent},
  { path: 'SelectBranch', component: SelectBranchComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallcenterRoutingModule { }
