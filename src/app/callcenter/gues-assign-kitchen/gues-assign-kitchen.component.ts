import { Component, OnInit,TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-gues-assign-kitchen',
  templateUrl: './gues-assign-kitchen.component.html',
  styleUrls: ['./gues-assign-kitchen.component.css']
})
export class GuesAssignKitchenComponent implements OnInit {
  modalRef: BsModalRef;
  public foodList;
  public foodList_popup;
  public counter: number = 2;
 
  ordermenus: string[] = ['Customer', 'Order'];
  selectedorder = this.ordermenus[1];

  fastfoodmenus: string[] = ['Fast Food', 'Drinks', 'Pizza', 'Dessert', 'Lunch Special', 'Dinner Special'];
  selectedfastfood = this.fastfoodmenus[0];

  constructor(public _router: Router, private headerService: HeaderService, private modalService: BsModalService) { }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      animated: true,
      // backdrop: 'static'
    });
  }
  ngOnInit() {
    this.headerService.setTitle('Cell Center');
    this.foodList = [
      { foodname: 'Noodles', imageUrl: 'assets/images/fastfood/fast_food_img1.png' },
      { foodname: 'Fried Rice', imageUrl: 'assets/images/fastfood/fast_food_img2.png' },
      { foodname: 'Samosa', imageUrl: 'assets/images/fastfood/fast_food_img3.png' },
      { foodname: 'Egg Kurma', imageUrl: 'assets/images/fastfood/fast_food_img4.png' },
      { foodname: 'pani puri', imageUrl: 'assets/images/fastfood/fast_food_img5.png' },
      { foodname: 'Chicken', imageUrl: 'assets/images/fastfood/fast_food_img6.png' },
      { foodname: 'Sushi', imageUrl: 'assets/images/fastfood/fast_food_img7.png' },
      { foodname: 'Fish', imageUrl: 'assets/images/fastfood/fast_food_img8.png' },
      { foodname: 'Fried Breads', imageUrl: 'assets/images/fastfood/fast_food_img9.png' },
      { foodname: 'Hotdog', imageUrl: 'assets/images/fastfood/fast_food_img10.png' },
      { foodname: 'Momo', imageUrl: 'assets/images/fastfood/fast_food_img11.png' },
      { foodname: 'Shawarma', imageUrl: 'assets/images/fastfood/fast_food_img12.png' },
      { foodname: 'Mutton', imageUrl: 'assets/images/fastfood/fast_food_img13.png' },
      { foodname: 'Pakora', imageUrl: 'assets/images/fastfood/fast_food_img13.png' },
      { foodname: 'Gravys', imageUrl: 'assets/images/fastfood/fast_food_img13.png' }
    ],
      this.foodList_popup = [
        { imageUrl: 'assets/images/fastfood/fast_food_img6.png' },
      ]
  }

  dish = [
    { label: ' Cheese', selected: true },
    { label: ' Onion', selected: true },
    { label: 'Spinach', selected: true },
    { label: 'Mayonnaise', selected: true },

  ];
  dishes = [
    { label: ' Extra Cheese', selected: true },
    { label: ' Extra Onion' },
    { label: 'Extra Spinach' },
    { label: 'Extra Mayonnaise' },

  ];

  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;
  }

}
