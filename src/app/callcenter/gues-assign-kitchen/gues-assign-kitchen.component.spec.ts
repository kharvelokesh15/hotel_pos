import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuesAssignKitchenComponent } from './gues-assign-kitchen.component';

describe('GuesAssignKitchenComponent', () => {
  let component: GuesAssignKitchenComponent;
  let fixture: ComponentFixture<GuesAssignKitchenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuesAssignKitchenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuesAssignKitchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
