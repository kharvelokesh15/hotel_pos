import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestListingComponent } from './guest-listing.component';

describe('GuestListingComponent', () => {
  let component: GuestListingComponent;
  let fixture: ComponentFixture<GuestListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
