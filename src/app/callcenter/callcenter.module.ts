import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker'; 
import { ModalModule } from 'ngx-bootstrap/modal';

import { CallcenterRoutingModule } from './callcenter-routing.module';
import { GuestListingComponent } from './guest-listing/guest-listing.component';
import { SelectBranchComponent } from './select-branch/select-branch.component';
import { GuestListingHistoryComponent } from './guest-listing-history/guest-listing-history.component';
import { GuestMenuScreenComponent } from './guest-menu-screen/guest-menu-screen.component';
import { GuesAssignKitchenComponent } from './gues-assign-kitchen/gues-assign-kitchen.component';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { GuestInformationComponent } from './guest-information/guest-information.component';
import { OnlyNumbersDirective } from './only-numbers.directive';


@NgModule({
  declarations: [GuestListingComponent, SelectBranchComponent, OnlyNumbersDirective,GuestListingHistoryComponent, GuestMenuScreenComponent, GuesAssignKitchenComponent, PhoneNumberComponent, GuestInformationComponent],
  imports: [
    CommonModule,
    CallcenterRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    ModalModule.forRoot(),
    AmazingTimePickerModule 
  ]
})
export class CallcenterModule { }
