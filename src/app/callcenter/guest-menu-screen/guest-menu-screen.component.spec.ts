import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestMenuScreenComponent } from './guest-menu-screen.component';

describe('GuestMenuScreenComponent', () => {
  let component: GuestMenuScreenComponent;
  let fixture: ComponentFixture<GuestMenuScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestMenuScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestMenuScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
