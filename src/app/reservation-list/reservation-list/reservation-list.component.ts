import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service'
@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  public ReservationData;
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public ReservationDate; 
  isReservationLists: boolean = false;
  checkAllReservationLists: boolean = false;
  constructor(public _router: Router, private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('RESERVATION');
    this.ReservationData = [
      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Steve Smith', Reservationphoneno: '9856325721', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '1 Floor', Reservationtableno: 'T1', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Albert Jeo ', Reservationphoneno: '9856325731', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '2 Floor', Reservationtableno: 'T2', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Steve Smith', Reservationphoneno: '9856325721', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '1 Floor', Reservationtableno: 'T1', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Albert Jeo ', Reservationphoneno: '9856325731', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '2 Floor', Reservationtableno: 'T2', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Steve Smith', Reservationphoneno: '9856325721', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '1 Floor', Reservationtableno: 'T1', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Albert Jeo ', Reservationphoneno: '9856325731', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '2 Floor', Reservationtableno: 'T2', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Steve Smith', Reservationphoneno: '9856325721', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '1 Floor', Reservationtableno: 'T1', Reservationnoofguest: '04'
      },

      {
        Reservationdate: '25-Jul-2019', ReservationTime: '8:55 pm', Reservationguestname: 'Albert Jeo ', Reservationphoneno: '9856325731', Reservationfromtime: '8:50 pm', Reservationtotime: '9:50 pm', Reservationfloor: '2 Floor', Reservationtableno: 'T2', Reservationnoofguest: '04'
      }


    ]
  }

  changeReservationLists(event) {
    if (event.target.name == 'ReservationData') {
      this.isReservationLists = true
    }

    if (this.isReservationLists && this.checkAllReservationLists) {
      event.target.checked = true
    }
  }

  allReservationLists(event) {
    const checked = event.target.checked;
    this.ReservationData.forEach(item => item.selected = checked);
  }

}
