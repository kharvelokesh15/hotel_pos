import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { ReservationListRoutingModule } from './reservation-list-routing.module';
import { ReservationListComponent } from './reservation-list/reservation-list.component';


@NgModule({
  declarations: [ReservationListComponent],
  imports: [
    CommonModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    ReservationListRoutingModule,
    ModalModule.forRoot()
  ]
})
export class ReservationListModule { }
