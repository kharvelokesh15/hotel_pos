import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountPaylistComponent } from './account-paylist/account-paylist.component';
import { AccountFormComponent } from './account-form/account-form.component';
const routes: Routes = [
  { path: 'accountpaylist', component: AccountPaylistComponent, pathMatch: 'full' },
  { path: 'accountform', component: AccountFormComponent, }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPaymentRoutingModule { }
