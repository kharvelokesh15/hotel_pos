import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPaylistComponent } from './account-paylist.component';

describe('AccountPaylistComponent', () => {
  let component: AccountPaylistComponent;
  let fixture: ComponentFixture<AccountPaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
