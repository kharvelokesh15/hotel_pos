import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';
import { AccountService } from './../../@service/account/index.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-account-paylist',
  templateUrl: './account-paylist.component.html',
  styleUrls: ['./account-paylist.component.css']
})
export class AccountPaylistComponent implements OnInit {
  @ViewChild('template', {static: true}) template: TemplateRef<any>;

  public accountpaymentData;
  public account = {
      "acc_cat_code" : "",
      "head_master_name" : "",
      "description" : "",
      "status" : 1  
  };
  modalRef: BsModalRef;
  constructor(
    public _router: Router, 
    private headerService: HeaderService, 
    private AccountService : AccountService,
    private modalService: BsModalService,
    ) { }

  ngOnInit() {
    this.headerService.setTitle('Account Payment');
    this.getAccountList(); 
  }

  getAccountList(){
    this.AccountService.accountList().subscribe(res => {
      this.accountpaymentData = res
      console.log('working')
    });
  }
  openModalWithClass(template: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(  
      template,{animated: true, class: 'modal-sm popup-min' }  
      
    );  
  } 

  createAccount(){
    this.openModalWithClass(this.template);
  }
  saveAccount(){
    console.log('rv', this.account)
    this.AccountService.accountSave(this.account).subscribe(res => {
      console.log('account created')
      this.modalRef.hide();
    });
  }

}
