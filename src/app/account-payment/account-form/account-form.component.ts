import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
//services
import { HeaderService } from './../../header.service';
@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {

  constructor(public _router: Router, public headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Account Payment');
  }

}
