import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { AccountPaymentRoutingModule } from './account-payment-routing.module';
import { AccountPaylistComponent } from './account-paylist/account-paylist.component';
import { AccountFormComponent } from './account-form/account-form.component';

import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AccountPaylistComponent, AccountFormComponent],
  imports: [
    CommonModule,
    HeaderModule,
    AccountPaymentRoutingModule,
    FormsModule,
  ]
})
export class AccountPaymentModule { }
