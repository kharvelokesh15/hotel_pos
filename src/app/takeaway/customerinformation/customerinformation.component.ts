import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {IMyOptions} from 'mydatepicker';
//services
import { HeaderService } from './../../header.service';
import { CustomerManageService } from './../../@service/customer';
import { UtilityService } from '../../@service';

@Component({
  selector: 'app-customerinformation',
  templateUrl: './customerinformation.component.html',
  styleUrls: ['./customerinformation.component.css']
})
export class CustomerinformationComponent implements OnInit {

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public Date; 

  public customer = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "gender": "",
    "email": ""
  };
  public exCustomer: false | boolean;
  public search: any;

  constructor(
    public _router: Router , 
    private headerService: HeaderService,
    private customerManageService: CustomerManageService,
    private utilityService: UtilityService,
    ) { }

  ngOnInit() {
    this.headerService.setTitle('TAKE AWAY');
  }

  searchCustomers(data){
    this.customerManageService.customerSearch(data.target.value).subscribe(res => {
      if(res){  
        this.customer = res[0]; 
        this.exCustomer = true;
        console.log('search customer =>', this.customer);
      }else{
        this.exCustomer = false;
        this.search = data.target.value;
        this.resetCustomer();
        this.resetLocalStorageCustomer();
      }
    })
  }

  resetCustomer(){
    this.customer = {
      "first_name": "",
      "last_name": "",
      "phone": this.search,
      "gender": "",
      "email": ""
    };
  }

  resetLocalStorageCustomer(){        
    this.utilityService.setSelectedCustomer({});
  }

  saveCustomer(){
    if(this.exCustomer == true){
      // Save Customer in local storage
      this.utilityService.setSelectedCustomer(this.customer);
      console.log('add ex customer in local storage');
      this._router.navigate(['dinein/DineinMenuListCategory']);
    }else{
      // add new customer
      this.customerManageService.customerSave({
        "first_name": this.customer.first_name,
        "last_name": this.customer.last_name,
        "gender": this.customer.gender,
        "email": this.customer.email,
        "mobile_number": this.customer.phone,
        "type":1,
        "phone": this.customer.phone,
        "mobile_key": this.customer.phone,
      }).subscribe(res => {
          this.customerManageService.customerSearch(this.search).subscribe(res => {
            this.customer = res[0];
            this.utilityService.setSelectedCustomer(this.customer);
          });
      });
    }
  }

}
