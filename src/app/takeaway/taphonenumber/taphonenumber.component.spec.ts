import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TAphonenumberComponent } from './taphonenumber.component';

describe('TAphonenumberComponent', () => {
  let component: TAphonenumberComponent;
  let fixture: ComponentFixture<TAphonenumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TAphonenumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TAphonenumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
