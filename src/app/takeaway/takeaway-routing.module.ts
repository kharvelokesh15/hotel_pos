import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TAphonenumberComponent } from './taphonenumber/taphonenumber.component';
import { CustomerinformationComponent } from './customerinformation/customerinformation.component';
import { MenuscreenComponent } from './menuscreen/menuscreen.component';

const routes: Routes = [
  { path: 'phonenumber', component: TAphonenumberComponent, pathMatch: 'full' },
  { path: 'customerinformation', component: CustomerinformationComponent },
  { path: 'menuscreen', component: MenuscreenComponent }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TakeawayRoutingModule { }
