import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module'
import { TakeawayRoutingModule } from './takeaway-routing.module';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { ModalModule } from 'ngx-bootstrap/modal';

//components
import { OnlyNumbersDirective } from '../takeaway/only-numbers.directive';
import { TAphonenumberComponent } from './taphonenumber/taphonenumber.component';
import { CustomerinformationComponent } from './customerinformation/customerinformation.component';
import { MenuscreenComponent } from './menuscreen/menuscreen.component';


@NgModule({
  declarations: [TAphonenumberComponent,OnlyNumbersDirective, CustomerinformationComponent, MenuscreenComponent],
  imports: [
    CommonModule,
    HeaderModule,
    TakeawayRoutingModule,
    FormsModule,
    MyDatePickerModule,
    ModalModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TakeawayModule { }
