import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeGuestHistoryComponent } from './charge-guest-history.component';

describe('ChargeGuestHistoryComponent', () => {
  let component: ChargeGuestHistoryComponent;
  let fixture: ComponentFixture<ChargeGuestHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeGuestHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeGuestHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
