import { Component, OnInit,TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
//services
import { HeaderService } from './../../header.service';

@Component({
  selector: 'app-charge-staff',
  templateUrl: './charge-staff.component.html',
  styleUrls: ['./charge-staff.component.css']
})
export class ChargeStaffComponent implements OnInit {

  ordermenus: string[] = ['Order Transfer ', 'Item Transfer'];
  selectedorder = this.ordermenus[0];

  constructor(public _router: Router, private headerService: HeaderService) { }

 
  ngOnInit() {
    this.headerService.setTitle('No Charge');
  }
   
  chargesprofiles = [
    {chargeCode : '1',chargeName : 'John Smith',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '9876543210'},
    {chargeCode : '2',chargeName : 'Andrew Sam',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '8765432109'},
    {chargeCode : '3',chargeName : 'Robin Kellerman',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '7654321098'},
    {chargeCode : '4',chargeName : 'Mohammed Imran',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '6543210987'},
    {chargeCode : '5',chargeName : 'Robin Kellerman',chargeAddress : '1511  Bingamon Branch Road,  Lake view, CA-25',chargeContact : '7654321098'},

  ];

  
  chargesprofile = [
    {chargeValue : 'Staff 1'},
    {chargeValue : 'Staff 2'},
    {chargeValue : 'Staff 3'},
    {chargeValue : 'Staff 4'},
    {chargeValue : 'Staff 5'}
  ];
}


