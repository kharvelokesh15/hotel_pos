import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeStaffComponent } from './charge-staff.component';

describe('ChargeStaffComponent', () => {
  let component: ChargeStaffComponent;
  let fixture: ComponentFixture<ChargeStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
