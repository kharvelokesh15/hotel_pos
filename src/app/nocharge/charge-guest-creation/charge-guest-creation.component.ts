import { Component, OnInit } from '@angular/core';
import { HeaderService } from './../../header.service';
import {IMyOptions} from 'mydatepicker';
@Component({
  selector: 'app-charge-guest-creation',
  templateUrl: './charge-guest-creation.component.html',
  styleUrls: ['./charge-guest-creation.component.css']
})
export class ChargeGuestCreationComponent implements OnInit {

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public Date; 
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('No Charge');
  }

}
