import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeGuestCreationComponent } from './charge-guest-creation.component';

describe('ChargeGuestCreationComponent', () => {
  let component: ChargeGuestCreationComponent;
  let fixture: ComponentFixture<ChargeGuestCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeGuestCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeGuestCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
