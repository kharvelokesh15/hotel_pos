import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChargeStaffComponent } from './charge-staff/charge-staff.component'
import { ChargeGuestCreationComponent } from './charge-guest-creation/charge-guest-creation.component'
import { ChargeGuestHistoryComponent } from './charge-guest-history/charge-guest-history.component'










const routes: Routes = [
  { path: 'ChargeStaff', component: ChargeStaffComponent, pathMatch: 'full' },
  { path: 'ChargeGuestCreate', component: ChargeGuestCreationComponent },
  { path: 'ChargeGuestHistory', component: ChargeGuestHistoryComponent }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NochargeRoutingModule { }
