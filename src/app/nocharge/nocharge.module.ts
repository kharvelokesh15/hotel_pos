import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MyDatePickerModule } from 'mydatepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { NochargeRoutingModule } from './nocharge-routing.module';
import { ChargeStaffComponent } from './charge-staff/charge-staff.component';
import { ChargeGuestCreationComponent } from './charge-guest-creation/charge-guest-creation.component';
import { ChargeGuestHistoryComponent } from './charge-guest-history/charge-guest-history.component';



@NgModule({
  declarations: [ChargeStaffComponent, ChargeGuestCreationComponent, ChargeGuestHistoryComponent],
  imports: [
    CommonModule,
    NochargeRoutingModule,
    HeaderModule,
    MyDatePickerModule,
    FormsModule,
    AmazingTimePickerModule

  ]
})
export class NochargeModule { }
