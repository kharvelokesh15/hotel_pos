const {
  app,
  BrowserWindow,
  ipcMain,
  IpcMessageEvent
} = require('electron')
const path = require('path')
const fs = require('fs')
const url = require('url');
const { callbackify } = require('util');

let win;
let printerWin = [];

function createWindow() {


  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: 600,
    height: 600,
    webPreferences: {
      //nodeIntegration: true,
      allowRunningInsecureContent: true,
      webSecurity: false,
      nativeWindowOpen: true, // add this
      nodeIntegration: true,
      enableRemoteModule: true
    },
  });

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/pos/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // win.webContents.openDevTools()


  // win.webContents.openDevTools();


  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  /* printerWin = new BrowserWindow({
    x: 0,
    y: 0,
    width: 900,
    height: 650,
    webPreferences: {
      //nodeIntegration: true,
      allowRunningInsecureContent: true,
      webSecurity: false,
      nativeWindowOpen: true, // add this
      nodeIntegration: true,
      enableRemoteModule: true
    },
  });
  
  printerWin.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/pos/worker.html'),
    protocol: 'file:',
    slashes: true
  }));
  
  printerWin.webContents.openDevTools()
  printerWin.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    printerWin = null;
  }); */
}

ipcMain.on('getFiles', (event, arg) => {
  const files = fs.readdirSync(__dirname)
  win.webContents.send('getFilesResponse', files)
})


ipcMain.on('print', (event, arg) => {

  win.webContents.send('print-done', 'test');
});

ipcMain.on('ping', (event, arg) => {
  event.sender.send('pong');
});


// ipcMain.on('navigateTo', (event, data) => { secondWindow.loadURL('file://' + __dirname + '/dist/index.html#/kdsScreen/kdsScreen');
//   secondWindow.show(); 
// });

function createPrinterWindow(id, callback) {
  printerWin[id] =  new BrowserWindow({
    x: 0,
    y: 0,
    width: 900,
    height: 650,
    webPreferences: {
      //nodeIntegration: true,
      allowRunningInsecureContent: true,
      webSecurity: false,
      nativeWindowOpen: true, // add this
      nodeIntegration: true,
      enableRemoteModule: true
    },
  });
      
  printerWin[id].loadURL(url.format({
    pathname: path.join(__dirname, 'dist/pos/worker.html'),
    protocol: 'file:',
    slashes: true
  })).then(success => callback(), err => callback(err));

  // printerWin[id].webContents.openDevTools()

  printerWin[id].on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    printerWin[id] = null;
  });
}


// retransmit it to printerWin
ipcMain.on("printToKitchen", (event, content) => {
  console.log(content);
  createPrinterWindow(content.id, (err)=>{
    if(!err){
      printerWin[content.id].webContents.send("printToKitchen", content);
    }
  });
});

// when worker window is ready
ipcMain.on("readyToPrintToKitchen", (event, {id, printerOptions}) => {
  console.log(printerOptions);
  // const pdfPath = path.join(os.tmpdir(), 'print.pdf');
  // Use default printing options
  printerWin[id].webContents.print(printerOptions, () => {
    console.log('printing . . . ')
    printerWin[id].destroy();
    // printerWin[id].close();
  });
  /* .then((data)=>{
      fs.writeFile(pdfPath, data, (error) => {
        if (error) {
          throw error
        }
        shell.openItem(pdfPath)
        event.sender.send('wrote-pdf', pdfPath)
      })
    }).catch((error) => {
      throw error;
    }) */
});

// Create window on electron intialization
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow()
  }
})
